<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Usuários

Route::post('cadastrarUsuarioAPI', 'UsuariosMobileController@cadastrarUsuarioAPI');

Route::post('loginUsuarioAPI', 'UsuariosMobileController@loginUsuarioAPI');

Route::get('perfilUsuarioLogadoAPI', 'UsuariosMobileController@perfilUsuarioLogadoAPI');

Route::get('empresaUsuarioLogadoAPI', 'UsuariosMobileController@empresaUsuarioLogadoAPI');

Route::post('editarUsuarioLogadoAPI/{id}', 'UsuariosMobileController@editarUsuarioLogadoAPI');

Route::get('informacoesUsuarioLogadoAPI', 'UsuariosMobileController@informacoesUsuarioLogadoAPI');




//Faturamento

 Route::post('cadastrarFaturamentoAPI', 'FaturamentosMobileController@cadastrarFaturamentoAPI');

 Route::post('editarFaturamentoAPI/{id}', 'FaturamentosMobileController@editarFaturamentoAPI');

 Route::delete('excluirFaturamentoAPI/{id}', 'FaturamentosMobileController@excluirFaturamentoAPI');

 Route::get('listarFaturamentoAPI','FaturamentosMobileController@listarFaturamentoAPI');

 Route::get('graficosFaturamentoAPI','FaturamentosMobileController@graficosFaturamentoAPI');

 //Clientes

 Route::post('cadastrarClienteAPI', 'ClientesMobileController@cadastrarClienteAPI');

 Route::post('editarClienteAPI/{id}', 'ClientesMobileController@editarClienteAPI');

 Route::delete('excluirClienteAPI/{id}', 'ClientesMobileController@excluirClienteAPI');

 Route::get('listarClienteAPI','ClientesMobileController@listarClienteAPI');

 Route::get('graficosClienteAPI','ClientesMobileController@graficosClienteAPI');


 //Verba de Marketing

 Route::post('cadastrarVerbaAPI', 'VerbasMobileController@cadastrarVerbaAPI');

 Route::post('editarVerbaAPI/{id}', 'VerbasMobileController@editarVerbaAPI');

 Route::delete('excluirVerbaAPI/{id}', 'VerbasMobileController@excluirVerbaAPI');

 Route::get('listarVerbaAPI','VerbasMobileController@listarVerbaAPI');

 Route::get('graficosVerbaAPI','VerbasMobileController@graficosVerbaAPI');


 //Empresas

 Route::post('cadastrarEmpresaAPI', 'EmpresasMobileController@cadastrarEmpresaAPI');

 Route::post('editarEmpresaAPI/{id}', 'EmpresasMobileController@editarEmpresaAPI');

 Route::delete('excluirEmpresaAPI/{id}', 'EmpresasMobileController@excluirEmpresaAPI');

 Route::get('listarEmpresaAPI','EmpresasMobileController@listarEmpresaAPI');

 Route::get('listarSegmentoEmpresaAPI','EmpresasMobileController@listarSegmentoEmpresaAPI');

 //Ações de Marketing

 Route::post('cadastrarAcaoAPI', 'AcoesMobileController@cadastrarAcaoAPI');

 Route::post('editarAcaoAPI/{id}', 'AcoesMobileController@editarAcaoAPI');

 Route::delete('excluirAcaoAPI/{id}', 'AcoesMobileController@excluirAcaoAPI');

 Route::get('listarAcaoAPI','AcoesMobileController@listarAcaoAPI');

 Route::get('graficosAcoesAPI','AcoesMobileController@graficosAcoesAPI');

 //Investimentos Totais

 Route::post('cadastrarInvestimentoTotalAPI', 'InvestimentosTotaisMobileController@cadastrarInvestimentoTotalAPI');

 Route::post('editarInvestimentoTotalAPI/{id}', 'InvestimentosTotaisMobileController@editarInvestimentoTotalAPI');

 Route::delete('excluirInvestimentoTotalAPI/{id}', 'InvestimentosTotaisMobileController@excluirInvestimentoTotalAPI');

 Route::get('listarInvestimentoTotalAPI','InvestimentosTotaisMobileController@listarInvestimentoTotalAPI');

 Route::get('graficosInvestimentosTotaisAPI','InvestimentosTotaisMobileController@graficosInvestimentosTotaisAPI');

 //Despesas

 Route::post('cadastrarDespesaAPI', 'DespesasMobileController@cadastrarDespesaAPI');

 Route::post('editarDespesaAPI/{id}', 'DespesasMobileController@editarDespesaAPI');

 Route::delete('excluirDespesaAPI/{id}', 'DespesasMobileController@excluirDespesaAPI');

 Route::get('listarDespesaAPI','DespesasMobileController@listarDespesaAPI');

 Route::get('graficosDespesasAPI', 'DespesasMobileController@graficosDespesasAPI');

 //Dados do NPS

 Route::post('cadastrarDadosNPSAPI', 'DadosNPSController@cadastrarDadosNPSAPI');

 Route::post('editarDadosNPSAPI/{id}', 'DadosNPSController@editarDadosNPSAPI');

 Route::delete('excluirDadosNPSAPI/{id}', 'DadosNPSController@excluirDadosNPSAPI');

 Route::get('listarDadosNPSAPI','DadosNPSController@listarDadosNPSAPI');

 Route::get('graficosDadosNPSAPI','DadosNPSController@graficosDadosNPSAPI');

 //Ticket Médio

 Route::get('ticketMedioAtualAPI','TicketMedioMobileController@ticketMedioAtualAPI');

 Route::get('paginaTicketMedioAPI','TicketMedioMobileController@paginaTicketMedioAPI');
 
 Route::post('pesquisaTicketMedio', 'TicketMedioMobileController@pesquisaTicketMedio')->name('pesquisaTicketMedio');


//CAC

Route::get('cacAtualAPI','CACMobileController@cacAtualAPI');

Route::get('paginaCacAPI','CACMobileController@paginaCacAPI');

Route::post('pesquisaCac', 'CACMobileController@pesquisaCac')->name('pesquisaCac');


//LTV

Route::get('ltvAtualAPI','LTVMobileController@ltvAtualAPI');

Route::get('paginaLtvAPI','LTVMobileController@paginaLtvAPI');

Route::post('pesquisaLtv', 'LTVMobileController@pesquisaLtv')->name('pesquisaLtv');


//LTV CAC

Route::get('ltvCacAtualAPI','LTVCACMobileController@ltvCacAtualAPI');

Route::get('paginaLtvCacAPI','LTVCACMobileController@paginaLtvCacAPI');

Route::post('pesquisaLtvCac', 'LTVCACMobileController@pesquisaLtvCac')->name('pesquisaLtvCac');


//ROI

Route::get('roiAtualAPI','ROIMobileController@roiAtualAPI');

Route::get('paginaRoiAPI','ROIMobileController@paginaRoiAPI');

Route::post('pesquisaRoi', 'ROIMobileController@pesquisaRoi')->name('pesquisaRoi');


//ROMI

Route::get('romiAtualAPI','ROMIMobileController@romiAtualAPI');

Route::get('paginaRomiAPI','ROMIMobileController@paginaRomiAPI');

Route::post('pesquisaRomi', 'ROMIMobileController@pesquisaRomi')->name('pesquisaRoi');

//PAYBACK

Route::get('payBackAtualAPI','PAYBACKMobileController@payBackAtualAPI');

Route::get('paginaPayBackAPI','PAYBACKMobileController@paginaPayBackAPI');

Route::post('pesquisaPayback', 'PAYBACKMobileController@pesquisaPayback')->name('pesquisaPayback');


//NPS

Route::get('npsAtualAPI','NPSMobileController@npsAtualAPI');

Route::get('paginaNpsAPI','NPSMobileController@paginaNpsAPI');

Route::post('pesquisaNps', 'NPSMobileController@pesquisaNps')->name('pesquisaNps');
