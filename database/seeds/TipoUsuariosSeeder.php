<?php

use Illuminate\Database\Seeder;

class TipoUsuariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_usuarios')->insert([
            'id' => '1',
            'nome_tipo_usuario' => 'Free',
        ]);
    

        DB::table('tipo_usuarios')->insert([
            'id' => '2',
            'nome_tipo_usuario' => 'Standard',
        ]);
    

    }
}
