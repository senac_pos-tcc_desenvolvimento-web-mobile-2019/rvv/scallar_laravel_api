<?php

use Illuminate\Database\Seeder;

class QuestionarioAlternativaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('questionario_alternativa')->insert([
            'questionario_id' => 1,
            'alternativa_id' => 1
        ]);

        DB::table('questionario_alternativa')->insert([
            'questionario_id' => 1,
            'alternativa_id' => 2
        ]);

        DB::table('questionario_alternativa')->insert([
            'questionario_id' => 1,
            'alternativa_id' => 3
        ]);

        DB::table('questionario_alternativa')->insert([
            'questionario_id' => 2,
            'alternativa_id' => 4
        ]);

        DB::table('questionario_alternativa')->insert([
            'questionario_id' => 2,
            'alternativa_id' => 5
        ]);

        DB::table('questionario_alternativa')->insert([
            'questionario_id' => 2,
            'alternativa_id' => 6
        ]);

        DB::table('questionario_alternativa')->insert([
            'questionario_id' => 3,
            'alternativa_id' => 7
        ]);

        DB::table('questionario_alternativa')->insert([
            'questionario_id' => 3,
            'alternativa_id' => 8
        ]);

        DB::table('questionario_alternativa')->insert([
            'questionario_id' => 3,
            'alternativa_id' => 9
        ]);

        DB::table('questionario_alternativa')->insert([
            'questionario_id' => 4,
            'alternativa_id' => 10
        ]);

        DB::table('questionario_alternativa')->insert([
            'questionario_id' => 4,
            'alternativa_id' => 11
        ]);

        DB::table('questionario_alternativa')->insert([
            'questionario_id' => 4,
            'alternativa_id' => 12
        ]);













    }
}
