<?php

use Illuminate\Database\Seeder;

class SegmentosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('segmentos')->insert([
            'tipo' => 'Alimentos e bebidas',
        ]);

        DB::table('segmentos')->insert([
            'tipo' => 'Animais de estimação e outros animais',
        ]);

        DB::table('segmentos')->insert([
            'tipo' => 'Artes e entretenimento',
        ]);

        DB::table('segmentos')->insert([
            'tipo' => 'Automotivo',
        ]);

        DB::table('segmentos')->insert([
            'tipo' => 'Automotivo',
        ]);

        DB::table('segmentos')->insert([
            'tipo' => 'Beleza e condicionamento físico',
        ]);

        DB::table('segmentos')->insert([
            'tipo' => 'Casa e Jardim',
        ]);

        DB::table('segmentos')->insert([
            'tipo' => 'Ciência',
        ]);

        DB::table('segmentos')->insert([
            'tipo' => 'Compras',
        ]);


        DB::table('segmentos')->insert([
            'tipo' => 'Computadores, softwares e eletrônicos',
        ]);

        DB::table('segmentos')->insert([
            'tipo' => 'Comunidades on-line',
        ]);

        DB::table('segmentos')->insert([
            'tipo' => 'Empregos e educação',
        ]);

        DB::table('segmentos')->insert([
            'tipo' => 'Esportes',
        ]);

        DB::table('segmentos')->insert([
            'tipo' => 'Finanças',
        ]);

        DB::table('segmentos')->insert([
            'tipo' => 'Hobbies e lazer',
        ]);


        DB::table('segmentos')->insert([
            'tipo' => 'Internet e telecomunicações',
        ]);

        DB::table('segmentos')->insert([
            'tipo' => 'Jogos',
        ]);

        DB::table('segmentos')->insert([
            'tipo' => 'Legislação e governo',
        ]);

        DB::table('segmentos')->insert([
            'tipo' => 'Livros e literatura',
        ]);
        
        DB::table('segmentos')->insert([
            'tipo' => 'Mercados comercial e industrial',
        ]);

        DB::table('segmentos')->insert([
            'tipo' => 'Notícias',
        ]);

        DB::table('segmentos')->insert([
            'tipo' => 'Pessoas e sociedade',
        ]);

        DB::table('segmentos')->insert([
            'tipo' => 'Referência',
        ]);

        DB::table('segmentos')->insert([
            'tipo' => 'Saúde',
        ]);
        
        DB::table('segmentos')->insert([
            'tipo' => 'Serviços imobiliários',
        ]);
        
        DB::table('segmentos')->insert([
            'tipo' => 'Viagens',
        ]);


        DB::table('segmentos')->insert([
            'tipo' => 'Outros',
        ]);











    }
}

