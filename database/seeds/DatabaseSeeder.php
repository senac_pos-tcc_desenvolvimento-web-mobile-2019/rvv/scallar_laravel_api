<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(QuestionariosTableSeeder::class);
         $this->call(AlternativasTableSeeder::class);
         $this->call(QuestionarioAlternativaSeeder::class);
         $this->call(SegmentosSeeder::class);
         $this->call(CargosSeeder::class);
         $this->call(PeriodosSeeder::class);
         $this->call(TipoInvestimentosSeeder::class);
         $this->call(AnosSeeder::class);
         $this->call(TipoUsuariosSeeder::class);


         


    }
}
