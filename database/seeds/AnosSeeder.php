<?php

use Illuminate\Database\Seeder;

class AnosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('anos')->insert([
            'id' => '2000',
            'ano' => '2000',

        ]);


        DB::table('anos')->insert([
            'id' => '2001',
            'ano' => '2001',

        ]);

        DB::table('anos')->insert([
            'id' => '2002',
            'ano' => '2002',

        ]);

        DB::table('anos')->insert([
            'id' => '2003',
            'ano' => '2003',

        ]);

        DB::table('anos')->insert([
            'id' => '2004',
            'ano' => '2004',

        ]);

        DB::table('anos')->insert([
            'id' => '2005',
            'ano' => '2005',

        ]);

        DB::table('anos')->insert([
            'id' => '2006',
            'ano' => '2006',

        ]);

        DB::table('anos')->insert([
            'id' => '2007',
            'ano' => '2007',

        ]);

        DB::table('anos')->insert([
            'id' => '2008',
            'ano' => '2008',

        ]);

        DB::table('anos')->insert([
            'id' => '2009',
            'ano' => '2009',

        ]);

        DB::table('anos')->insert([
            'id' => '2010',
            'ano' => '2010',

        ]);

        DB::table('anos')->insert([
            'id' => '2011',
            'ano' => '2011',

        ]);

        DB::table('anos')->insert([
            'id' => '2012',
            'ano' => '2012',

        ]);

        DB::table('anos')->insert([
            'id' => '2013',
            'ano' => '2013',

        ]);

        DB::table('anos')->insert([
            'id' => '2014',
            'ano' => '2014',

        ]);

        DB::table('anos')->insert([
            'id' => '2015',
            'ano' => '2015',

        ]);

        DB::table('anos')->insert([
            'id' => '2016',
            'ano' => '2016',

        ]);

        DB::table('anos')->insert([
            'id' => '2017',
            'ano' => '2017',

        ]);

        DB::table('anos')->insert([
            'id' => '2018',
            'ano' => '2018',

        ]);

        DB::table('anos')->insert([
            'id' => '2019',
            'ano' => '2019',

        ]);

        DB::table('anos')->insert([
            'id' => '2020',
            'ano' => '2020',

        ]);

        DB::table('anos')->insert([
            'id' => '2021',
            'ano' => '2021',

        ]);

        DB::table('anos')->insert([
            'id' => '2022',
            'ano' => '2022',

        ]);

        DB::table('anos')->insert([
            'id' => '2023',
            'ano' => '2023',

        ]);

        DB::table('anos')->insert([
            'id' => '2024',
            'ano' => '2024',

        ]);



    }
}
