<?php

use Illuminate\Database\Seeder;

class QuestionariosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('questionarios')->insert([
            'pergunta' => 'Como é o envolvimento de sua empresa com a comunidade ao redor?',
        ]);

        DB::table('questionarios')->insert([
            'pergunta' => 'Como você avalia a vantagem competitiva de sua empresa em comparação a suas concorrentes?',
        ]);

        DB::table('questionarios')->insert([
            'pergunta' => 'Qual das seguintes afirmações melhor condiz com o comportamento da sua empresa?',
           
        ]);
        
        DB::table('questionarios')->insert([
            'pergunta' => 'Como é a situação financeira da sua empresa?',
            
        ]);

        

    }
}
