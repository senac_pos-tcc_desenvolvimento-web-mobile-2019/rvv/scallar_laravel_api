<?php

use Illuminate\Database\Seeder;

class AlternativasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('alternativas')->insert([
            'alternativa' => 'Não costumamos nos relacionar com empresas/pessoas que não façam parte da organização.',
            'pontos' => '2' ,
        ]);

        DB::table('alternativas')->insert([
            'alternativa' => 'Nos relacionamos com pessoas de fora da organização esporadicamente.',
            'pontos' => '3' ,
        ]);

        DB::table('alternativas')->insert([
            'alternativa' => 'Fazemos ações de cunho social frequentemente. (Essa alternativa é cabível e condizente com o restante?)',    
            'pontos' => '5' ,
        ]);




            

        DB::table('alternativas')->insert([
            'alternativa' => 'Temos práticas/produtos inovadores em comparação com nossos concorrentes.',
            'pontos' => '2' ,
        ]);


        DB::table('alternativas')->insert([
            'alternativa' => 'Nossos produtos e práticas são similares aos nossos concorrentes.',
            'pontos' => '3' ,
        ]);
            
        DB::table('alternativas')->insert([
            'alternativa' => 'Nossos concorrentes possuem vantagens competitivas que ainda não conseguimos igualar.',
            'pontos' => '5' ,
        ]);



        DB::table('alternativas')->insert([
            'alternativa' => 'O cliente da nossa empresa tem sempre razão!',
            'pontos' => '2' ,
        ]);

        DB::table('alternativas')->insert([
            'alternativa' => 'Às vezes achamos que nós temos a razão, e tentamos explicar para o nosso cliente o porquê.',
            'pontos' => '3' ,
        ]);

        DB::table('alternativas')->insert([
            'alternativa' => 'Nossa empresa tem o conhecimento, e o cliente precisa aceitar isto.',    
            'pontos' => '5' ,
        ]);


        DB::table('alternativas')->insert([
            'alternativa' => 'Temos um fluxo de caixa bem estruturado e estamos preparados para quaisquer imprevistos.',
            'pontos' => '2' ,
        ]);


        DB::table('alternativas')->insert([
            'alternativa' => 'Temos altos e baixos.',
            'pontos' => '3' ,
        ]);

        DB::table('alternativas')->insert([
            'alternativa' => 'Estamos perdendo mais dinheiro do que estamos arrecadando.',    
            'pontos' => '5' ,
        ]);

       

        
            
            
            
           
           

        
    }
}
