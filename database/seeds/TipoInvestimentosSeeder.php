<?php

use Illuminate\Database\Seeder;

class TipoInvestimentosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_investimentos')->insert([
            'tipo_investimento' => 'Anúncio em Mídia Impressa',
        ]);
    
        DB::table('tipo_investimentos')->insert([
            'tipo_investimento' => 'Anúncio Radio/TV',
        ]);

        DB::table('tipo_investimentos')->insert([
            'tipo_investimento' => 'Atualização do CRM',
        ]);

        DB::table('tipo_investimentos')->insert([
            'tipo_investimento' => 'Atualização do Site',
        ]);

        DB::table('tipo_investimentos')->insert([
            'tipo_investimento' => 'Consultoria de marketing',
        ]);

        DB::table('tipo_investimentos')->insert([
            'tipo_investimento' => 'Conteudo para blog',
        ]);

        DB::table('tipo_investimentos')->insert([
            'tipo_investimento' => 'Conteúdo para Mídias Sociais',
        ]);

        DB::table('tipo_investimentos')->insert([
            'tipo_investimento' => 'Conteúdo vídeo',
        ]);

        DB::table('tipo_investimentos')->insert([
            'tipo_investimento' => 'Contratação de agência de marketing',
        ]);

        DB::table('tipo_investimentos')->insert([
            'tipo_investimento' => 'Facebook Ads',
        ]);

        DB::table('tipo_investimentos')->insert([
            'tipo_investimento' => 'Google Ads',
        ]);

        DB::table('tipo_investimentos')->insert([
            'tipo_investimento' => 'Links patrocinados',
        ]);

        DB::table('tipo_investimentos')->insert([
            'tipo_investimento' => 'Novo CRM',
        ]);

        DB::table('tipo_investimentos')->insert([
            'tipo_investimento' => 'Novo Site',
        ]);

        DB::table('tipo_investimentos')->insert([
            'tipo_investimento' => 'SEO',
        ]);

        DB::table('tipo_investimentos')->insert([
            'tipo_investimento' => 'Serviço de Email Marketing',
        ]);
    
    }
}
