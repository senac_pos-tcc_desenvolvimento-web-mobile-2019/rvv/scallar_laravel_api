<?php

use Illuminate\Database\Seeder;

class CargosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cargos')->insert([
            'nome_cargo' => 'Presidente / Diretor / Vice-Presidente',
        ]);

        DB::table('cargos')->insert([
            'nome_cargo' => 'Gerente Geral / Superintendente',
        ]);

        DB::table('cargos')->insert([
            'nome_cargo' => 'Gerente de Produto / Mercado / Marketing',
        ]);
    
        DB::table('cargos')->insert([
            'nome_cargo' => 'Gerente Financeiro / Controladoria',
        ]);

        DB::table('cargos')->insert([
            'nome_cargo' => 'Gerente de Planejamento',
        ]);

        DB::table('cargos')->insert([
            'nome_cargo' => 'Analista de Marketing',
        ]);

        DB::table('cargos')->insert([
            'nome_cargo' => 'Analista Financeiro',
        ]);

        DB::table('cargos')->insert([
            'nome_cargo' => 'Assessor / Consultor',
        ]);

        DB::table('cargos')->insert([
            'nome_cargo' => 'Outro',
        ]);
    
    }
}
