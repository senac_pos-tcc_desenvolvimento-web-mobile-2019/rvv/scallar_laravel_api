<?php

use Illuminate\Database\Seeder;

class PeriodosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('periodos')->insert([
            'mes' => 'Janeiro',
        ]);

        DB::table('periodos')->insert([
            'mes' => 'Fevereiro',
        ]);

        DB::table('periodos')->insert([
            'mes' => 'Março',
        ]);

        DB::table('periodos')->insert([
            'mes' => 'Abril',
        ]);

        DB::table('periodos')->insert([
            'mes' => 'Maio',
        ]);

        DB::table('periodos')->insert([
            'mes' => 'Junho',
        ]);

        DB::table('periodos')->insert([
            'mes' => 'Julho',
        ]);

        DB::table('periodos')->insert([
            'mes' => 'Agosto',
        ]);

        DB::table('periodos')->insert([
            'mes' => 'Setembro',
        ]);

        DB::table('periodos')->insert([
            'mes' => 'Outubro',
        ]);

        DB::table('periodos')->insert([
            'mes' => 'Novembro',
        ]);

        DB::table('periodos')->insert([
            'mes' => 'Dezembro',
        ]);
    }
}
