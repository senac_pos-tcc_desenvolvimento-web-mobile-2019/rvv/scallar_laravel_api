<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuarioEmpresaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuario_empresa', function (Blueprint $table) {
            $table->integer('usuarios_id')->unsigned();
            $table->integer('empresas_id')->unsigned();
            $table->enum('autorizado', array('0','1'))->default('0');
            
            $table->enum('ler', array('0','1'))->default('0');
            $table->enum('escrever', array('0','1'))->default('0');
            

            $table->foreign('usuarios_id')->references('id')->on('usuarios')->onUpdated('cascade')->onDelete('cascade');

            $table->foreign('empresas_id')->references('id')->on('empresas')->onUpdated('cascade')->onDelete('cascade');


        });
    }

    
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuario_empresa');
    }
}

