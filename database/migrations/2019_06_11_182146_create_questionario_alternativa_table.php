<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionarioAlternativaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questionario_alternativa', function (Blueprint $table) {
            $table->integer('questionario_id')->unsigned();
            $table->integer('alternativa_id')->unsigned();

            $table->foreign('questionario_id')->references('id')->on('questionarios')->onUpdated('cascade')->onDelete('cascade');

        $table->foreign('alternativa_id')->references('id')->on('alternativas')->onUpdated('cascade')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questionario_alternativa');
    }
}
