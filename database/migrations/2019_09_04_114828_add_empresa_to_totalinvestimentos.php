<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmpresaToTotalinvestimentos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('total_investimentos', function (Blueprint $table) {
            $table->integer('empresa_id')->nullable()->unsigned();

            $table->foreign('empresa_id')
                ->references('id')->on('empresas')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('totalinvestimentos', function (Blueprint $table) {
            //
        });
    }
}
