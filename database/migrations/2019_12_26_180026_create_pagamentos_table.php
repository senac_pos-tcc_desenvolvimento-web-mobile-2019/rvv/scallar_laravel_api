<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagamentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pagamentos', function (Blueprint $table) {
           
            $table->increments('id');
            $table->decimal('total', 20, 2)->default(0.00);
            $table->date('data');
            $table->integer('reference')->unique();
            $table->string('codigo');
            $table->integer('tipo_pagamento');
            $table->enum('status', [1,2,3,4,5,6,7]);
            $table->string('link_boleto');
            
            $table->integer('numero_dias');

            $table->timestamps();

            // Tipo de Pagamento (LOGICA)
            //1 - Essencial Mensal
            //2 - Essencial Semestral
            //3 - Essencial Anual
            //4 - Agencias e Aceleradoras Mensal
            //5 - Agencias e Aceleradoras Semestral
            //6 - Agencias e Aceleradoras Anual
            //7 - Custom

           
        });
    }

    //ALTER TABLE pagamentos ADD tipo_pagamento int;

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pagamentos');
    }
}
