<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAnoToTotalInvestimentos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('total_investimentos', function (Blueprint $table) {
            $table->integer('ano_id')->nullable()->unsigned();

            $table->foreign('ano_id')
                ->references('id')->on('anos')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('total_investimentos', function (Blueprint $table) {
            //
        });
    }
}
