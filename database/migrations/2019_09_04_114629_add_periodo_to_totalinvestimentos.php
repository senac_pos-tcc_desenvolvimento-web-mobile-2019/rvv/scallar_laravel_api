<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPeriodoToTotalinvestimentos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('total_investimentos', function (Blueprint $table) {
            $table->integer('periodo_id')->nullable()->unsigned();

            $table->foreign('periodo_id')
                ->references('id')->on('periodos')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('totalinvestimentos', function (Blueprint $table) {
            //
        });
    }
}
