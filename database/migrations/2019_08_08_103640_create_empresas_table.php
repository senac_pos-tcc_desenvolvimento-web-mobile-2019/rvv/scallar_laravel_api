<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('razao_social');
           
            $table->string('cnpj')->unique();
            $table->string('cod_empresa')->nullable();
            
            
            $table->integer('numero_funcionarios');
            $table->double('faturamento_anual', 10,2);
            $table->integer('media_retencao_clientes');
            $table->timestamps();

        });
    }

    
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dadosempresas');
    }
}

