<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAnoToNetPromoterScores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('net_promoter_scores', function (Blueprint $table) {
            $table->integer('ano_id')->nullable()->unsigned();

            $table->foreign('ano_id')
                ->references('id')->on('anos')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('net_promoter_scores', function (Blueprint $table) {
            //
        });
    }
}
