<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questionarios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('pergunta');
            // $table->string('alternativa_a');
            // $table->string('pontos_alternativa_a');
            // $table->string('alternativa_b');
            // $table->string('pontos_alternativa_b');
            // $table->string('alternativa_c');
            // $table->string('pontos_alternativa_c');
            // $table->integer('numero_quiz');
            $table->timestamps();


            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questionarios');
    }
}
