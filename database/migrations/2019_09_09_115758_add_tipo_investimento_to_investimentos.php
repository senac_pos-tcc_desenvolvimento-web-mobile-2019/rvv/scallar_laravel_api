<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTipoInvestimentoToInvestimentos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('investimentos', function (Blueprint $table) {
            $table->integer('tipo_investimento_id')->nullable()->unsigned();

            $table->foreign('tipo_investimento_id')
                ->references('id')->on('tipo_investimentos')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('investimentos', function (Blueprint $table) {
            //
        });
    }
}
