# Scallar - Análise de métricas de marketing

---

## Descrição

API Web desenvolvida utilizando o framework laravel para trabalhar em conjunto com o APP Mobile Scallar.

## Instalação

1 - Efetuar o Clone do Projeto na pasta htdocs do xampp.

```sh
git clone https://gitlab.com/senac_pos-tcc_desenvolvimento-web-mobile-2019/rvv/scallar_laravel_api
```
   
2 - Acessar o PhpMyAdmin ou similar e criar uma tabela chamada imarketing com padrão utf8_general_ci.
   
3 - Abrir o CMD e navegar até a pasta do projeto.
   
```sh
cd 'scallar_laravel_api'
```
  
4 - Executar o seguinte comando para instalar as dependências  

```sh
composer install
```
   
5 - Executar o comando de criação de tabelas

```sh
php artisan migrate
```

6 - Executar o comando de população de tabelas

```sh
php artisan db:seed
```  

7 - Executar o comando para inicializar a API

```sh
php artisan serve
```  

## API Web

```sh
https://sleepy-caverns-24482.herokuapp.com/
```

A API pode ser consumida utilizando um programa como o Postman.

## Estrutura

### Esta aplicação é dividia em dois projetos.

Scallar_Mobile_App em typescript com react native

Scallar_Laravel_API em php com laravel

### O projeto Scallar_Mobile_App pode ser acessado pelo endereço:
    
https://gitlab.com/senac_pos-tcc_desenvolvimento-web-mobile-2019/rvv/scallar-mobile-app

---
