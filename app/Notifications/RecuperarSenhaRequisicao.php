<?php
namespace App\Notifications;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class RecuperarSenhaRequisicao extends Notification implements ShouldQueue
{
    use Queueable;
    protected $token;
    /**
    * Create a new notification instance.
    *
    * @return void
    */
    public function __construct($token)
    {
        $this->token = $token;
    }
    /**
    * Get the notification's delivery channels.
    *
    * @param  mixed  $notifiable
    * @return array
    */
    public function via($notifiable)
    {
        return ['mail'];
    }
     /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
     public function toMail($notifiable)
     {

        //$urlPaginaSenha = route('pagina.nova.senha') ;
        //$url = url($urlPaginaSenha.'/api/password/encontrarToken/'.$this->token);
        $url = url('pagina.nova.senha/'.$this->token);
        return (new MailMessage)
            ->line('Você está recebendo esse e-mail devido a uma solicitação de alteração de senha para sua conta.')
            ->action('Alterar Senha', url($url))
            ->line('Se você não fez essa requisição, nenhuma ação é solicitada');
    }
    /**
    * Get the array representation of the notification.
    *
    * @param  mixed  $notifiable
    * @return array
    */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}