<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Investimento extends Model
{
    protected $fillable = ['tipo', 'valor','data_inicial', 'data_final', 'empresa_id', 'usuario_id', 'periodo_id','tipo_investimento_id','ano_id'];

    public function empresas()
    {
        return $this->hasOne('App\Empresa', 'id', 'empresa_id');
    }

    public function periodos()
    {
        return $this->hasOne('App\Periodo', 'id', 'periodo_id');
    }

    public function tipo_investimentos()
    {
        return $this->hasOne('App\TipoInvestimento', 'id', 'tipo_investimento_id');
    }

    public function anos()
    {
        return $this->hasOne('App\Ano', 'id', 'ano_id');
    }

}
