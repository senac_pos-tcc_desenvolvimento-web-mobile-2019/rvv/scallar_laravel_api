<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TotalInvestimento extends Model
{
    protected $fillable = ['nome', 'valor','data_inicial', 'data_final', 'empresa_id', 'usuario_id', 'periodo_id','ano_id'];

    public function empresas()
    {
        return $this->hasOne('App\Empresa', 'id', 'empresa_id');
    }

    public function periodos()
    {
        return $this->hasOne('App\Periodo', 'id', 'periodo_id');
    }

    public function anos()
    {
        return $this->hasOne('App\Ano', 'id', 'ano_id');
    }
}
