<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pagamento extends Model
{
    protected $fillable = ['total','data','reference','status','usuario_id','codigo','link_boleto','numero_dias','tipo_pagamento'];

    public function usuarios(){
        return $this->hasOne('App\Usuario', 'usuario_id', 'id');
    }

}
