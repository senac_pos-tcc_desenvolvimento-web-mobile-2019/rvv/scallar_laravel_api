<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Investimento;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Routing\ResponseFactory;
use Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;


class AcoesMobileController extends Controller
{



    public function cadastrarAcaoAPI(Request $request){

        $usuario_autenticado_id = $request->session()->get('usuarioId');


        $acoes = Investimento::create([

            'tipo' => $request['tipo'],
            'valor' => $request['valor'],
            'usuario_id' => $usuario_autenticado_id,
            'empresa_id' => $request['empresa_id'],
            'periodo_id' => $request['periodo_id'],
            'tipo_investimento_id' => $request['tipo_investimento_id'],
            'ano_id' => $request['ano_id'],


        ]);


        if($acoes){

            return response()->json(
                'Ação de marketing cadastrada com sucesso'
            );

        }else{

            return response()->json(
                'Erro ao salvar a Ação de marketing'
            );

        } 


    }




    public function editarAcaoAPI(Request $request, $id){

        $dados = $request->all();

        $acaoId = Investimento::find($id);

        $alt = $acaoId->update($dados);

        if ($alt)
        {

            return response()->json(
                'Ação de marketing atualizada'
            );

        }else{

            return response()->json(
                'Erro ao atualizar a Ação de marketing'
            );
            

        }



    }





    public function excluirAcaoAPI($id){

        $acao = Investimento::find($id);

        if ($acao->delete()){
                return response()->json(
                'Ação de marketing deletada'
                );

        }else{
            return response()->json(
                'Erro ao excluir a ação de marketing'
            );
        }
        
    }



    public function listarAcaoAPI(Request $request){

        $usuario_autenticado_id = $request->session()->get('usuarioId');


        $acoes = Investimento::where('usuario_id', $usuario_autenticado_id)->orderBy('ano_id', 'desc')
        ->orderBy('periodo_id', 'desc')
        ->paginate(4);


        return response()->json($acoes);


    }



    public function graficosAcoesAPI(Request $request){

        $usuario_autenticado_id = $request->session()->get('usuarioId');
        $data_atual = Carbon::now();


        $acoesJaneiro = DB::table('investimentos')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 1)
        ->where('ano_id', $data_atual->year)
        ->sum('valor');

    $acoesFevereiro = DB::table('investimentos')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 2)
        ->where('ano_id', $data_atual->year)
        ->sum('valor');

    $acoesMarco = DB::table('investimentos')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 3)
        ->where('ano_id', $data_atual->year)
        ->sum('valor');

    $acoesAbril = DB::table('investimentos')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 4)
        ->where('ano_id', $data_atual->year)
        ->sum('valor');

    $acoesMaio = DB::table('investimentos')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 5)
        ->where('ano_id', $data_atual->year)
        ->sum('valor');

    $acoesJunho = DB::table('investimentos')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 6)
        ->where('ano_id', $data_atual->year)
        ->sum('valor');

    $acoesJulho = DB::table('investimentos')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 7)
        ->where('ano_id', $data_atual->year)
        ->sum('valor');

    $acoesAgosto = DB::table('investimentos')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 8)
        ->where('ano_id', $data_atual->year)
        ->sum('valor');

    $acoesSetembro = DB::table('investimentos')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 9)
        ->where('ano_id', $data_atual->year)
        ->sum('valor');

    $acoesOutubro = DB::table('investimentos')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 10)
        ->where('ano_id', $data_atual->year)
        ->sum('valor');

    $acoesNovembro = DB::table('investimentos')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 11)
        ->where('ano_id', $data_atual->year)
        ->sum('valor');

    $acoesDezembro = DB::table('investimentos')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 12)
        ->where('ano_id', $data_atual->year)
        ->sum('valor');


        return response()->json(array(

            $acoesJaneiro, 
            $acoesFevereiro, 
            $acoesMarco, 
            $acoesAbril, 
            $acoesMaio, 
            $acoesJunho, 
            $acoesJulho,
            $acoesAgosto,
            $acoesSetembro,
            $acoesOutubro,
            $acoesNovembro,
            $acoesDezembro,

        ));


    }












    
}
