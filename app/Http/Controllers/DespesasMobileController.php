<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Despesa;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Routing\ResponseFactory;
use Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;


class DespesasMobileController extends Controller
{


     public function cadastrarDespesaAPI(Request $request){

        $usuario_autenticado_id = $request->session()->get('usuarioId');


        $despesas = Despesa::create([

            'valor' => $request['valor'],
            'periodo_id' => $request['periodo_id'],
            'ano_id' => $request['ano_id'],
            'empresa_id' => $request['empresa_id'],
            'usuario_id' => $usuario_autenticado_id,

        ]);


        if($despesas){

            return response()->json(
                'Despesa cadastrada com sucesso'
            );

        }else{

            return response()->json(
                'Erro ao salvar a despesa'
            );

        }


    }




    public function editarDespesaAPI(Request $request, $id){

        $dados = $request->all();

        $despesaId = Despesa::find($id);

        $alt = $despesaId->update($dados);

        if ($alt)
        {

            return response()->json(
                'Despesa atualizada'
            );

        }else{

            return response()->json(
                'Erro ao atualizar a despesa'
            );


        }



    }





    public function excluirDespesaAPI($id){

        $despesa = Despesa::find($id);

        if ($despesa->delete()){
                return response()->json(
                'Despesa deletada'
                );

        }else{
            return response()->json(
                'Erro ao excluir a despesa'
            );
        }

    }



    public function listarDespesaAPI(Request $request){

        $usuario_autenticado_id = $request->session()->get('usuarioId');


        $despesas = Despesa::where('usuario_id', $usuario_autenticado_id)->orderBy('ano_id', 'desc')
        ->orderBy('periodo_id', 'desc')
        ->paginate(4);


        return response()->json($despesas);


    }



    public function graficosDespesasAPI(Request $request){

        $usuario_autenticado_id = $request->session()->get('usuarioId');
        $data_atual = Carbon::now();

        $despesasJaneiro = DB::table('despesas')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 1)
            ->where('ano_id', $data_atual->year)
            ->sum('valor');

        $despesasFevereiro = DB::table('despesas')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 2)
            ->where('ano_id', $data_atual->year)
            ->sum('valor');

        $despesasMarco = DB::table('despesas')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 3)
            ->where('ano_id', $data_atual->year)
            ->sum('valor');

        $despesasAbril = DB::table('despesas')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 4)
            ->where('ano_id', $data_atual->year)
            ->sum('valor');

        $despesasMaio = DB::table('despesas')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 5)
            ->where('ano_id', $data_atual->year)
            ->sum('valor');

        $despesasJunho = DB::table('despesas')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 6)
            ->where('ano_id', $data_atual->year)
            ->sum('valor');

        $despesasJulho = DB::table('despesas')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 7)
            ->where('ano_id', $data_atual->year)
            ->sum('valor');

        $despesasAgosto = DB::table('despesas')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 8)
            ->where('ano_id', $data_atual->year)
            ->sum('valor');

        $despesasSetembro = DB::table('despesas')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 9)
            ->where('ano_id', $data_atual->year)
            ->sum('valor');

        $despesasOutubro = DB::table('despesas')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 10)
            ->where('ano_id', $data_atual->year)
            ->sum('valor');

        $despesasNovembro = DB::table('despesas')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 11)
            ->where('ano_id', $data_atual->year)
            ->sum('valor');

        $despesasDezembro = DB::table('despesas')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 12)
            ->where('ano_id', $data_atual->year)
            ->sum('valor');

            return response()->json(array(

                $despesasJaneiro,
                $despesasFevereiro,
                $despesasMarco,
                $despesasAbril,
                $despesasMaio,
                $despesasJunho,
                $despesasJulho,
                $despesasAgosto,
                $despesasSetembro,
                $despesasOutubro,
                $despesasNovembro,
                $despesasDezembro,

            ));


    }


}
