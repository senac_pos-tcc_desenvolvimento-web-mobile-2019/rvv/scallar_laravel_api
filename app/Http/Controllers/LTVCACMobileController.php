<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuario;
use App\Cargo;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\Routing\ResponseFactory;
use Auth;
use Session;
use Carbon\Carbon;

class LTVCACMobileController extends Controller
{

    public function ltvCacAtualAPI(Request $request)
    {


        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $data_atual = Carbon::now();
         

        //CAC ATUAL
        $cac = app('App\Http\Controllers\CACMobileController')->cacPaginaAPI($request);

        //LTV ATUAL
        $ltv = app('App\Http\Controllers\LTVMobileController')->ltvPaginaAPI($request);

        if ($cac != 0 && $ltv != 0)
        {
            $ltv_cac_pagina = $ltv / $cac;
        }
        else
        {
            $ltv_cac_pagina = 0;
        }


        return response()->json([
            'ltv_cac_pagina' => number_format($ltv_cac_pagina,2),
        ]);



    }




     //LTV CAC ATUAL

     public function ltvCacPaginaAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

         //CAC ATUAL
         $cac = app('App\Http\Controllers\CACMobileController')->cacPaginaAPI($request);

         //LTV ATUAL
         $ltv = app('App\Http\Controllers\LTVMobileController')->ltvPaginaAPI($request);
        

         if ($cac != 0 && $ltv != 0)
         {
             $ltv_cac_pagina = $ltv / $cac;
         }
         else
         {
             $ltv_cac_pagina = 0;
         }
 

        return $ltv_cac_pagina;


    }

      //LTV CAC DEZEMBRO ANO PASSADO

      public function ltvCacDezembroAnoPassadoAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

         //CAC ATUAL
         $cac = app('App\Http\Controllers\CACMobileController')->cacDezembroAnoPassadoAPI($request);

         //LTV ATUAL
         $ltv = app('App\Http\Controllers\LTVMobileController')->ltvDezembroAnoPassadoAPI($request);
        

         if ($cac != 0 && $ltv != 0)
         {
             $ltv_cac_dezembro_ano_passado = $ltv / $cac;
         }
         else
         {
             $ltv_cac_dezembro_ano_passado = 0;
         }
 

        return $ltv_cac_dezembro_ano_passado;


    }





     //LTV CAC JANEIRO

     public function ltvCacJaneiroAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

         //CAC ATUAL
         $cac = app('App\Http\Controllers\CACMobileController')->cacJaneiroAPI($request);

         //LTV ATUAL
         $ltv = app('App\Http\Controllers\LTVMobileController')->ltvJaneiroAPI($request);
        

         if ($cac != 0 && $ltv != 0)
         {
             $ltv_cac_janeiro = $ltv / $cac;
         }
         else
         {
             $ltv_cac_janeiro = 0;
         }
 

        return $ltv_cac_janeiro;


    }




    //LTV CAC FEVEREIRO

    public function ltvCacFevereiroAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

         //CAC ATUAL
         $cac = app('App\Http\Controllers\CACMobileController')->cacFevereiroAPI($request);

         //LTV ATUAL
         $ltv = app('App\Http\Controllers\LTVMobileController')->ltvFevereiroAPI($request);
        

         if ($cac != 0 && $ltv != 0)
         {
             $ltv_cac_fevereiro = $ltv / $cac;
         }
         else
         {
             $ltv_cac_fevereiro = 0;
         }
 

        return $ltv_cac_fevereiro;


    }



      //LTV CAC MARÇO

      public function ltvCacMarcoAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

         //CAC ATUAL
         $cac = app('App\Http\Controllers\CACMobileController')->cacMarcoAPI($request);

         //LTV ATUAL
         $ltv = app('App\Http\Controllers\LTVMobileController')->ltvMarcoAPI($request);
        

         if ($cac != 0 && $ltv != 0)
         {
             $ltv_cac_marco = $ltv / $cac;
         }
         else
         {
             $ltv_cac_marco = 0;
         }
 

        return $ltv_cac_marco;


    }



      //LTV CAC ABRIL

      public function ltvCacAbrilAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

         //CAC ATUAL
         $cac = app('App\Http\Controllers\CACMobileController')->cacAbrilAPI($request);

         //LTV ATUAL
         $ltv = app('App\Http\Controllers\LTVMobileController')->ltvAbrilAPI($request);
        

         if ($cac != 0 && $ltv != 0)
         {
             $ltv_cac_abril = $ltv / $cac;
         }
         else
         {
             $ltv_cac_abril = 0;
         }
 

        return $ltv_cac_abril;


    }



     //LTV CAC MAIO

     public function ltvCacMaioAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

         //CAC ATUAL
         $cac = app('App\Http\Controllers\CACMobileController')->cacMaioAPI($request);

         //LTV ATUAL
         $ltv = app('App\Http\Controllers\LTVMobileController')->ltvMaioAPI($request);
        

         if ($cac != 0 && $ltv != 0)
         {
             $ltv_cac_maio = $ltv / $cac;
         }
         else
         {
             $ltv_cac_maio = 0;
         }
 

        return $ltv_cac_maio;


    }




    //LTV CAC JUNHO

    public function ltvCacJunhoAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

         //CAC ATUAL
         $cac = app('App\Http\Controllers\CACMobileController')->cacJunhoAPI($request);

         //LTV ATUAL
         $ltv = app('App\Http\Controllers\LTVMobileController')->ltvJunhoAPI($request);
        

         if ($cac != 0 && $ltv != 0)
         {
             $ltv_cac_junho = $ltv / $cac;
         }
         else
         {
             $ltv_cac_junho = 0;
         }
 

        return $ltv_cac_junho;


    }



    //LTV CAC JULHO

    public function ltvCacJulhoAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

         //CAC ATUAL
         $cac = app('App\Http\Controllers\CACMobileController')->cacJulhoAPI($request);

         //LTV ATUAL
         $ltv = app('App\Http\Controllers\LTVMobileController')->ltvJulhoAPI($request);
        

         if ($cac != 0 && $ltv != 0)
         {
             $ltv_cac_julho = $ltv / $cac;
         }
         else
         {
             $ltv_cac_julho = 0;
         }
 

        return $ltv_cac_julho;


    }





     //LTV CAC AGOSTO

     public function ltvCacAgostoAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

         //CAC ATUAL
         $cac = app('App\Http\Controllers\CACMobileController')->cacAgostoAPI($request);

         //LTV ATUAL
         $ltv = app('App\Http\Controllers\LTVMobileController')->ltvAgostoAPI($request);
        

         if ($cac != 0 && $ltv != 0)
         {
             $ltv_cac_agosto = $ltv / $cac;
         }
         else
         {
             $ltv_cac_agosto = 0;
         }
 

        return $ltv_cac_agosto;


    }


      //LTV CAC SETEMBRO

      public function ltvCacSetembroAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

         //CAC ATUAL
         $cac = app('App\Http\Controllers\CACMobileController')->cacSetembroAPI($request);

         //LTV ATUAL
         $ltv = app('App\Http\Controllers\LTVMobileController')->ltvSetembroAPI($request);
        

         if ($cac != 0 && $ltv != 0)
         {
             $ltv_cac_setembro = $ltv / $cac;
         }
         else
         {
             $ltv_cac_setembro = 0;
         }
 

        return $ltv_cac_setembro;


    }



    //LTV CAC OUTUBRO

    public function ltvCacOutubroAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

         //CAC ATUAL
         $cac = app('App\Http\Controllers\CACMobileController')->cacOutubroAPI($request);

         //LTV ATUAL
         $ltv = app('App\Http\Controllers\LTVMobileController')->ltvOutubroAPI($request);
        

         if ($cac != 0 && $ltv != 0)
         {
             $ltv_cac_outubro = $ltv / $cac;
         }
         else
         {
             $ltv_cac_outubro = 0;
         }
 

        return $ltv_cac_outubro;


    }




     //LTV CAC NOVEMBRO

     public function ltvCacNovembroAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

         //CAC ATUAL
         $cac = app('App\Http\Controllers\CACMobileController')->cacNovembroAPI($request);

         //LTV ATUAL
         $ltv = app('App\Http\Controllers\LTVMobileController')->ltvNovembroAPI($request);
        

         if ($cac != 0 && $ltv != 0)
         {
             $ltv_cac_novembro = $ltv / $cac;
         }
         else
         {
             $ltv_cac_novembro = 0;
         }
 

        return $ltv_cac_novembro;


    }



      //LTV CAC NOVEMBRO

      public function ltvCacDezembroAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

         //CAC ATUAL
         $cac = app('App\Http\Controllers\CACMobileController')->cacDezembroAPI($request);

         //LTV ATUAL
         $ltv = app('App\Http\Controllers\LTVMobileController')->ltvDezembroAPI($request);
        

         if ($cac != 0 && $ltv != 0)
         {
             $ltv_cac_dezembro = $ltv / $cac;
         }
         else
         {
             $ltv_cac_dezembro = 0;
         }
 

        return $ltv_cac_dezembro;


    }






    public function paginaLtvCacAPI(Request $request)
    {

       

        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $data_atual = Carbon::now();

     
        //CAC ATUAL
        $cac =  app('App\Http\Controllers\CACMobileController')->cacPaginaAPI($request);

        //LTV ATUAL
        $ltv = app('App\Http\Controllers\LTVMobileController')->ltvPaginaAPI($request);
        
        $ltv_cac_pagina = $this->ltvCacPaginaAPI($request);
       
        $ltv_cac_dezembro_ano_passado = $this->ltvCacDezembroAnoPassadoAPI($request);
         
        $ltv_cac_janeiro = $this->ltvCacJaneiroAPI($request);
        
        $ltv_cac_fevereiro = $this->ltvCacFevereiroAPI($request);
       
        $ltv_cac_marco = $this->ltvCacMarcoAPI($request);

        $ltv_cac_abril = $this->ltvCacAbrilAPI($request);
        
        $ltv_cac_maio = $this->ltvCacMaioAPI($request);
        
        $ltv_cac_junho = $this->ltvCacJunhoAPI($request);
        
        $ltv_cac_julho = $this->ltvCacJulhoAPI($request);

        $ltv_cac_agosto = $this->ltvCacAgostoAPI($request);

        $ltv_cac_setembro = $this->ltvCacSetembroAPI($request);
       
        $ltv_cac_outubro = $this->ltvCacOutubroAPI($request);

        $ltv_cac_novembro = $this->ltvCacNovembroAPI($request);

        $ltv_cac_dezembro = $this->ltvCacDezembroAPI($request);

        //Comparação de Dados

        //Janeiro/Dezembro
        if ($ltv_cac_janeiro == 0 || $ltv_cac_dezembro_ano_passado == 0)
        {
            $ltv_cac_janeiro_dezembro = 0;
        }
        else
        {
            $ltv_cac_janeiro_dezembro = (($ltv_cac_janeiro / $ltv_cac_dezembro_ano_passado) - 1) * 100;
        }

        //Fevereiro/Janeiro
        if ($ltv_cac_fevereiro == 0 || $ltv_cac_janeiro == 0)
        {
            $ltv_cac_fevereiro_janeiro = 0;
        }
        else
        {
            $ltv_cac_fevereiro_janeiro = (($ltv_cac_fevereiro / $ltv_cac_janeiro) - 1) * 100;
        }

        //Marco/Fevereiro
        if ($ltv_cac_marco == 0 || $ltv_cac_fevereiro == 0)
        {
            $ltv_cac_marco_fevereiro = 0;
        }
        else
        {
            $ltv_cac_marco_fevereiro = (($ltv_cac_marco / $ltv_cac_fevereiro) - 1) * 100;
        }

        //Abril/Marco
        if ($ltv_cac_abril == 0 || $ltv_cac_marco == 0)
        {
            $ltv_cac_abril_marco = 0;
        }
        else
        {
            $ltv_cac_abril_marco = (($ltv_cac_abril / $ltv_cac_marco) - 1) * 100;
        }

        //Maio/Abril
        if ($ltv_cac_maio == 0 || $ltv_cac_abril == 0)
        {
            $ltv_cac_maio_abril = 0;
        }
        else
        {
            $ltv_cac_maio_abril = (($ltv_cac_maio / $ltv_cac_abril) - 1) * 100;
        }

        //Junho/Maio
        if ($ltv_cac_junho == 0 || $ltv_cac_maio == 0)
        {
            $ltv_cac_junho_maio = 0;
        }
        else
        {
            $ltv_cac_junho_maio = (($ltv_cac_junho / $ltv_cac_maio) - 1) * 100;
        }

        //Julho/Junho
        if ($ltv_cac_julho == 0 || $ltv_cac_junho == 0)
        {
            $ltv_cac_julho_junho = 0;
        }
        else
        {
            $ltv_cac_julho_junho = (($ltv_cac_julho / $ltv_cac_junho) - 1) * 100;
        }

        //Agosto/Julho
        if ($ltv_cac_agosto == 0 || $ltv_cac_julho == 0)
        {
            $ltv_cac_agosto_julho = 0;
        }
        else
        {
            $ltv_cac_agosto_julho = (($ltv_cac_agosto / $ltv_cac_julho) - 1) * 100;
        }

        //Setembro/Agosto
        if ($ltv_cac_setembro == 0 || $ltv_cac_agosto == 0)
        {
            $ltv_cac_setembro_agosto = 0;
        }
        else
        {
            $ltv_cac_setembro_agosto = (($ltv_cac_setembro / $ltv_cac_agosto) - 1) * 100;
        }

        //Outubro/Setembro
        if ($ltv_cac_outubro == 0 || $ltv_cac_setembro == 0)
        {
            $ltv_cac_outubro_setembro = 0;
        }
        else
        {
            $ltv_cac_outubro_setembro = (($ltv_cac_outubro / $ltv_cac_setembro) - 1) * 100;
        }

        //Novembro/Outubro
        if ($ltv_cac_novembro == 0 || $ltv_cac_outubro == 0)
        {
            $ltv_cac_novembro_outubro = 0;
        }
        else
        {
            $ltv_cac_novembro_outubro = (($ltv_cac_novembro / $ltv_cac_outubro) - 1) * 100;
        }

        //Dezembro/Novembro
        if ($ltv_cac_dezembro == 0 || $ltv_cac_novembro == 0)
        {
            $ltv_cac_dezembro_novembro = 0;
        }
        else
        {
            $ltv_cac_dezembro_novembro = (($ltv_cac_dezembro / $ltv_cac_novembro) - 1) * 100;
        }


       
 

        if ($cac == 0 && $ltv == 0)
        {
            $valida_cac_ltv = "Sem dados para avaliar a relação LTV:CAC.";
        }
        else if ($cac > $ltv)
        {
            $valida_cac_ltv = "A situação de sua empresa é crítica e exige uma mudança de estratégia, pois cada cliente adquirido está gerando um prejuízo monetário.";
        }
        else if ($ltv < (2 * ($cac)))
        {
            $valida_cac_ltv = "Sua empresa tem lucratividade, mas ainda não tem o LTV ideal e recomendado. É necessário otimizar seu processo de aquisição de clientes atual.";
        }
        else if ($ltv >= (2 * ($cac)) && $ltv <= (2.7 * ($cac)))
        {
            $valida_cac_ltv = "Sua empresa possui uma ótima lucratividade e está próxima de alcançar a razão ideal, ajustem alguns pontos de sua estratégia para diminuir os custos.";
        }
        else if ($ltv > (2.7 * ($cac)) && $ltv <= (3.2 * ($cac)))
        {
            $valida_cac_ltv = "Excelente! Sua empresa tem o ltv ideal e seu cliente paga o custo que foi necessário para adquiri-lo.";
        }
        else if ($ltv > (3.2 * ($cac)) && $ltv <= (3.9 * ($cac)))
        {
            $valida_cac_ltv = "Sua empresa possui uma alta lucratividade por cliente adquirido, mas vocês talvez possam gastar um pouco mais em novas estratégias de aquisição.";
        }
        else
        {
            $valida_cac_ltv = "Sua lucratividade por cliente adquirido é excessivamente alta, avalie se você não está perdendo a oportunidade de investir em novas ações que podem gerar um retorno ainda maior.";
        }

        return response()->json([
            'ltv_cac_pagina' => number_format($ltv_cac_pagina,2),
            'valida_cac_ltv' => $valida_cac_ltv,
            'ltv_cac_janeiro' => ($ltv_cac_janeiro),
            'ltv_cac_fevereiro' => ($ltv_cac_fevereiro),
            'ltv_cac_marco' => ($ltv_cac_marco),
            'ltv_cac_abril' => ($ltv_cac_abril),
            'ltv_cac_maio' => ($ltv_cac_maio),
            'ltv_cac_junho' => ($ltv_cac_junho),
            'ltv_cac_julho' => ($ltv_cac_julho),
            'ltv_cac_agosto' => ($ltv_cac_agosto),
            'ltv_cac_setembro' => ($ltv_cac_setembro),
            'ltv_cac_outubro' => ($ltv_cac_outubro),
            'ltv_cac_novembro' => ($ltv_cac_novembro),
            'ltv_cac_dezembro' => ($ltv_cac_dezembro),

            
        ]);



        
    }




      //DADOS DO LTV:CAC NO PERIODO PESQUISA

      public function ltvCacPesquisaPeriodo(Request $request){

          //CAC PERIODO
          $cac =  app('App\Http\Controllers\CACMobileController')->cacPesquisaPeriodo($request);

          //LTV PERIODO
          $ltv = app('App\Http\Controllers\LTVMobileController')->ltvPesquisaPeriodo($request);
  
          if ($cac != 0 && $ltv != 0)
          {
              $ltv_cac_periodo = $ltv / $cac;
          }
          else
          {
              $ltv_cac_periodo = 0;
          }

          return $ltv_cac_periodo;


      }




       //DADOS DO LTV:CAC NO PERIODO JANEIRO

    public function ltvCacPesquisaPeriodoJaneiro(Request $request){

        //CAC PERIODO
        $cac_janeiro =  app('App\Http\Controllers\CACMobileController')->cacPesquisaPeriodoJaneiro($request);

        //LTV PERIODO
        $ltv_janeiro = app('App\Http\Controllers\LTVMobileController')->ltvPesquisaPeriodoJaneiro($request);

        if ($cac_janeiro != 0 && $ltv_janeiro != 0)
        {
            $ltv_cac_janeiro = $ltv_janeiro / $cac_janeiro;
        }
        else
        {
            $ltv_cac_janeiro = 0;
        }

        return $ltv_cac_janeiro;


    }


    //DADOS DO LTV:CAC NO PERIODO FEVEREIRO

    public function ltvCacPesquisaPeriodoFevereiro(Request $request){

        //CAC PERIODO
        $cac_fevereiro =  app('App\Http\Controllers\CACMobileController')->cacPesquisaPeriodoFevereiro($request);

        //LTV PERIODO
        $ltv_fevereiro = app('App\Http\Controllers\LTVMobileController')->ltvPesquisaPeriodoFevereiro($request);

        if ($cac_fevereiro != 0 && $ltv_fevereiro != 0)
        {
            $ltv_cac_fevereiro = $ltv_fevereiro / $cac_fevereiro;
        }
        else
        {
            $ltv_cac_fevereiro = 0;
        }

        return $ltv_cac_fevereiro;



    }



    //DADOS DO LTV:CAC NO PERIODO MARCO

    public function ltvCacPesquisaPeriodoMarco(Request $request){

        //CAC PERIODO
        $cac_marco =  app('App\Http\Controllers\CACMobileController')->cacPesquisaPeriodoMarco($request);

        //LTV PERIODO
        $ltv_marco = app('App\Http\Controllers\LTVMobileController')->ltvPesquisaPeriodoMarco($request);

        if ($cac_marco != 0 && $ltv_marco != 0)
        {
            $ltv_cac_marco = $ltv_marco / $cac_marco;
        }
        else
        {
            $ltv_cac_marco = 0;
        }

        return $ltv_cac_marco;



    }






     //DADOS DO LTV:CAC NO PERIODO ABRIL

     public function ltvCacPesquisaPeriodoAbril(Request $request){

        //CAC PERIODO
        $cac_abril =  app('App\Http\Controllers\CACMobileController')->cacPesquisaPeriodoAbril($request);

        //LTV PERIODO
        $ltv_abril = app('App\Http\Controllers\LTVMobileController')->ltvPesquisaPeriodoAbril($request);

        if ($cac_abril != 0 && $ltv_abril != 0)
        {
            $ltv_cac_abril = $ltv_abril / $cac_abril;
        }
        else
        {
            $ltv_cac_abril = 0;
        }

        return $ltv_cac_abril;



    }






      //DADOS DO LTV:CAC NO PERIODO MAIO

      public function ltvCacPesquisaPeriodoMaio(Request $request){

        //CAC PERIODO
        $cac_maio =  app('App\Http\Controllers\CACMobileController')->cacPesquisaPeriodoMaio($request);

        //LTV PERIODO
        $ltv_maio = app('App\Http\Controllers\LTVMobileController')->ltvPesquisaPeriodoMaio($request);

        if ($cac_maio != 0 && $ltv_maio != 0)
        {
            $ltv_cac_maio = $ltv_maio / $cac_maio;
        }
        else
        {
            $ltv_cac_maio = 0;
        }

        return $ltv_cac_maio;



    }




    public function ltvCacPesquisaPeriodoJunho(Request $request){

        //CAC PERIODO
        $cac_junho =  app('App\Http\Controllers\CACMobileController')->cacPesquisaPeriodoJunho($request);

        //LTV PERIODO
        $ltv_junho = app('App\Http\Controllers\LTVMobileController')->ltvPesquisaPeriodoJunho($request);

        if ($cac_junho != 0 && $ltv_junho != 0)
        {
            $ltv_cac_junho = $ltv_junho / $cac_junho;
        }
        else
        {
            $ltv_cac_junho = 0;
        }

        return $ltv_cac_junho;


    }







    public function ltvCacPesquisaPeriodoJulho(Request $request){

        //CAC PERIODO
        $cac_julho =  app('App\Http\Controllers\CACMobileController')->cacPesquisaPeriodoJulho($request);

        //LTV PERIODO
        $ltv_julho = app('App\Http\Controllers\LTVMobileController')->ltvPesquisaPeriodoJulho($request);

        if ($cac_julho != 0 && $ltv_julho != 0)
        {
            $ltv_cac_julho = $ltv_julho / $cac_julho;
        }
        else
        {
            $ltv_cac_julho = 0;
        }

        return $ltv_cac_julho;



    }






    public function ltvCacPesquisaPeriodoAgosto(Request $request){

        //CAC PERIODO
        $cac_agosto =  app('App\Http\Controllers\CACMobileController')->cacPesquisaPeriodoAgosto($request);

        //LTV PERIODO
        $ltv_agosto = app('App\Http\Controllers\LTVMobileController')->ltvPesquisaPeriodoAgosto($request);

        if ($cac_agosto != 0 && $ltv_agosto != 0)
        {
            $ltv_cac_agosto = $ltv_agosto / $cac_agosto;
        }
        else
        {
            $ltv_cac_agosto = 0;
        }

        return $ltv_cac_agosto;



    }







    public function ltvCacPesquisaPeriodoSetembro(Request $request){

        //CAC PERIODO
        $cac_setembro =  app('App\Http\Controllers\CACMobileController')->cacPesquisaPeriodoSetembro($request);

        //LTV PERIODO
        $ltv_setembro = app('App\Http\Controllers\LTVMobileController')->ltvPesquisaPeriodoSetembro($request);

        if ($cac_setembro != 0 && $ltv_setembro != 0)
        {
            $ltv_cac_setembro = $ltv_setembro / $cac_setembro;
        }
        else
        {
            $ltv_cac_setembro = 0;
        }

        return $ltv_cac_setembro;



    }







    public function ltvCacPesquisaPeriodoOutubro(Request $request){

        //CAC PERIODO
        $cac_outubro =  app('App\Http\Controllers\CACMobileController')->cacPesquisaPeriodoOutubro($request);

        //LTV PERIODO
        $ltv_outubro = app('App\Http\Controllers\LTVMobileController')->ltvPesquisaPeriodoOutubro($request);

        if ($cac_outubro != 0 && $ltv_outubro != 0)
        {
            $ltv_cac_outubro = $ltv_outubro / $cac_outubro;
        }
        else
        {
            $ltv_cac_outubro = 0;
        }

        return $ltv_cac_outubro;



    }







    public function ltvCacPesquisaPeriodoNovembro(Request $request){

        //CAC PERIODO
        $cac_novembro =  app('App\Http\Controllers\CACMobileController')->cacPesquisaPeriodoNovembro($request);

        //LTV PERIODO
        $ltv_novembro = app('App\Http\Controllers\LTVMobileController')->ltvPesquisaPeriodoNovembro($request);

        if ($cac_novembro != 0 && $ltv_novembro != 0)
        {
            $ltv_cac_novembro = $ltv_novembro / $cac_novembro;
        }
        else
        {
            $ltv_cac_novembro = 0;
        }

        return $ltv_cac_novembro;



    }






    public function ltvCacPesquisaPeriodoDezembro(Request $request){

        //CAC PERIODO
        $cac_dezembro =  app('App\Http\Controllers\CACMobileController')->cacPesquisaPeriodoDezembro($request);

        //LTV PERIODO
        $ltv_dezembro = app('App\Http\Controllers\LTVMobileController')->ltvPesquisaPeriodoDezembro($request);

        if ($cac_dezembro != 0 && $ltv_dezembro != 0)
        {
            $ltv_cac_dezembro = $ltv_dezembro / $cac_dezembro;
        }
        else
        {
            $ltv_cac_dezembro = 0;
        }

        return $ltv_cac_dezembro;



    }









    public function pesquisaLtvCac(Request $request)
    {

        $usuarioAutenticadoId = $request->session()->get('usuarioId');
        
        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $empresa_id = $request->empresa_id;

         //DADOS DO CAC NO PERIODO
         $cac = app('App\Http\Controllers\CACMobileController')->cacPesquisaPeriodo($request);

        //DADOS DO LTV NO PERIODO
         $ltv = app('App\Http\Controllers\LTVMobileController')->ltvPesquisaPeriodo($request);


       
        $ltv_cac = $this->ltvCacPesquisaPeriodo($request);
             
        $ltv_cac_janeiro = $this->ltvCacPesquisaPeriodoJaneiro($request);
       
        $ltv_cac_fevereiro = $this->ltvCacPesquisaPeriodoFevereiro($request);
        
        $ltv_cac_marco =  $this->ltvCacPesquisaPeriodoMarco($request);
        
        $ltv_cac_abril =  $this->ltvCacPesquisaPeriodoAbril($request);
     
        $ltv_cac_maio = $this->ltvCacPesquisaPeriodoMaio($request);
      
        $ltv_cac_junho = $this->ltvCacPesquisaPeriodoJunho($request);
       
         $ltv_cac_julho = $this->ltvCacPesquisaPeriodoJulho($request);
       
        $ltv_cac_agosto = $this->ltvCacPesquisaPeriodoAgosto($request);
       
        $ltv_cac_setembro = $this->ltvCacPesquisaPeriodoSetembro($request);
       

        $ltv_cac_outubro =  $this->ltvCacPesquisaPeriodoOutubro($request);


        $ltv_cac_novembro = $this->ltvCacPesquisaPeriodoNovembro($request);

        $ltv_cac_dezembro =  $this->ltvCacPesquisaPeriodoDezembro($request);


        if ($cac == 0 && $ltv == 0)
        {
            $valida_cac_ltv = "Sem dados para avaliar a relação LTV:CAC.";
        }
        else if ($cac > $ltv)
        {
            $valida_cac_ltv = "A situação de sua empresa é crítica e exige uma mudança de estratégia, pois cada cliente adquirido está gerando um prejuízo monetário.";
        }
        else if ($ltv < (2 * ($cac)))
        {
            $valida_cac_ltv = "Sua empresa tem lucratividade, mas ainda não tem o LTV ideal e recomendado. É necessário otimizar seu processo de aquisição de clientes atual.";
        }
        else if ($ltv >= (2 * ($cac)) && $ltv <= (2.7 * ($cac)))
        {
            $valida_cac_ltv = "Sua empresa possui uma ótima lucratividade e está próxima de alcançar a razão ideal, ajustem alguns pontos de sua estratégia para diminuir os custos.";
        }
        else if ($ltv > (2.7 * ($cac)) && $ltv <= (3.2 * ($cac)))
        {
            $valida_cac_ltv = "Excelente! Sua empresa tem o ltv ideal e seu cliente paga o custo que foi necessário para adquiri-lo.";
        }
        else if ($ltv > (3.2 * ($cac)) && $ltv <= (3.9 * ($cac)))
        {
            $valida_cac_ltv = "Sua empresa possui uma alta lucratividade por cliente adquirido, mas vocês talvez possam gastar um pouco mais em novas estratégias de aquisição.";
        }
        else
        {
            $valida_cac_ltv = "Sua lucratividade por cliente adquirido é excessivamente alta, avalie se você não está perdendo a oportunidade de investir em novas ações que podem gerar um retorno ainda maior.";
        }



        return response()->json([
            'ltv_cac' => number_format($ltv_cac,2),
            'valida_cac_ltv' => $valida_cac_ltv,
            'ltv_cac_janeiro' => $ltv_cac_janeiro !== null ? $ltv_cac_janeiro : 0,
            'ltv_cac_fevereiro' => $ltv_cac_fevereiro !== null ? $ltv_cac_fevereiro : 0,
            'ltv_cac_marco' => $ltv_cac_marco !== null ? $ltv_cac_marco : 0 ,
            'ltv_cac_abril' => $ltv_cac_abril !== null ? $ltv_cac_abril : 0,
            'ltv_cac_maio' => $ltv_cac_maio !== null ? $ltv_cac_maio : 0,
            'ltv_cac_junho' => $ltv_cac_junho !== null ? $ltv_cac_junho : 0,
            'ltv_cac_julho' => $ltv_cac_julho !== null ? $ltv_cac_julho : 0,
            'ltv_cac_agosto' => $ltv_cac_agosto !== null ? $ltv_cac_agosto : 0,
            'ltv_cac_setembro' => $ltv_cac_setembro !== null ? $ltv_cac_setembro : 0,
            'ltv_cac_outubro' => $ltv_cac_outubro !== null ? $ltv_cac_outubro : 0 ,
            'ltv_cac_novembro' => $ltv_cac_novembro !== null ? $ltv_cac_novembro : 0,
            'ltv_cac_dezembro' => $ltv_cac_dezembro !== null ? $ltv_cac_dezembro : 0,


        ]);





       
        
    }












}