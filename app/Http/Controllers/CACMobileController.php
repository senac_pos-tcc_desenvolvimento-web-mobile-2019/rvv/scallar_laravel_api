<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuario;
use App\Cargo;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\Routing\ResponseFactory;
use Auth;
use Session;
use Carbon\Carbon;


class CACMobileController extends Controller
{

    public function cacAtualAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');


        $empresas = DB::table('empresas')->where('usuario_id','=',$usuarioAutenticadoId)->select('id')->first('id');


        $investimento_atual = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', $data_atual->month)
        ->where('investimentos.ano_id', '=', $data_atual->year)
        ->where('investimentos.empresa_id', '=', $empresas->id)->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

        $despesa_atual = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
        ->where('despesas.periodo_id', '=', $data_atual->month)
        ->where('despesas.ano_id', '=', $data_atual->year)
        ->where('despesas.empresa_id', '=', $empresas->id)->select(DB::raw('SUM(despesas.valor) as valor'))
        ->value('valor');

        $novos_clientes_atual = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
        ->where('clientes.periodo_id', '=', $data_atual->month)
        ->where('clientes.ano_id', '=', $data_atual->year)
        ->where('clientes.empresa_id', '=', $empresas->id)->select(DB::raw('SUM(clientes.novos_clientes) as novos_clientes'))
        ->value('novos_clientes');

        if ($novos_clientes_atual != 0)
        {
            $cac_pagina = ($investimento_atual + $despesa_atual) / $novos_clientes_atual;
        }
        else
        {
            $cac_pagina = 0;
        }



        return response()->json([
            'cac_pagina' => number_format($cac_pagina,2)
        ]);

    }



    /* CAC ATUAL */

    public function cacPaginaAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');


        $investimento_atual = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', $data_atual->month)
        ->where('investimentos.ano_id', '=', $data_atual->year)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

        $despesa_atual = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
        ->where('despesas.periodo_id', '=', $data_atual->month)
        ->where('despesas.ano_id', '=', $data_atual->year)
        ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(despesas.valor) as valor'))
        ->value('valor');

        $novos_clientes_atual = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
        ->where('clientes.periodo_id', '=', $data_atual->month)
        ->where('clientes.ano_id', '=', $data_atual->year)
        ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(clientes.novos_clientes) as novos_clientes'))
        ->value('novos_clientes');

        if ($novos_clientes_atual != 0)
        {
            $cac_pagina = ($investimento_atual + $despesa_atual) / $novos_clientes_atual;
        }
        else
        {
            $cac_pagina = 0;
        }

        return $cac_pagina;

    }



    /*CAC de DEZEMBRO DO ANO PASSADO*/

    public function cacDezembroAnoPassadoAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

      

        $investimento_dezembro_ano_passado = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 12)
        ->where('investimentos.ano_id', '=', $data_atual->year - 1)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

        $despesa_dezembro_ano_passado = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
        ->where('despesas.periodo_id', '=', 12)
        ->where('despesas.ano_id', '=', $data_atual->year - 1)
        ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(despesas.valor) as valor'))
        ->value('valor');

        $novos_clientes_dezembro_ano_passado = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
        ->where('clientes.periodo_id', '=', 12)
        ->where('clientes.ano_id', '=', $data_atual->year - 1)
        ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(clientes.novos_clientes) as novos_clientes'))
        ->value('novos_clientes');

        if ($novos_clientes_dezembro_ano_passado != 0)
        {
            $cac_dezembro_ano_passado = ($investimento_dezembro_ano_passado + $despesa_dezembro_ano_passado) / $novos_clientes_dezembro_ano_passado;
        }
        else
        {
            $cac_dezembro_ano_passado = 0;
        }

        return $cac_dezembro_ano_passado;


    }




     /*CAC de Janeiro*/

     public function cacJaneiroAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

       

        $investimento_janeiro = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 1)
        ->where('investimentos.ano_id', '=', $data_atual->year)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

        $despesa_janeiro = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
        ->where('despesas.periodo_id', '=', 1)
        ->where('despesas.ano_id', '=', $data_atual->year)
        ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(despesas.valor) as valor'))
        ->value('valor');

        $novos_clientes_janeiro = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
        ->where('clientes.periodo_id', '=', 1)
        ->where('clientes.ano_id', '=', $data_atual->year)
        ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(clientes.novos_clientes) as novos_clientes'))
        ->value('novos_clientes');

        if ($novos_clientes_janeiro != 0)
        {
            $cac_janeiro = ($investimento_janeiro + $despesa_janeiro) / $novos_clientes_janeiro;
        }
        else
        {
            $cac_janeiro = 0;
        }

        return $cac_janeiro;


    }

    /*CAC de Fevereiro*/

    public function cacFevereiroAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        

        $investimento_fevereiro = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 2)
        ->where('investimentos.ano_id', '=', $data_atual->year)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

        $despesa_fevereiro = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
        ->where('despesas.periodo_id', '=', 2)
        ->where('despesas.ano_id', '=', $data_atual->year)
        ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(despesas.valor) as valor'))
        ->value('valor');

        $novos_clientes_fevereiro = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
        ->where('clientes.periodo_id', '=', 2)
        ->where('clientes.ano_id', '=', $data_atual->year)
        ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(clientes.novos_clientes) as novos_clientes'))
        ->value('novos_clientes');

        if ($novos_clientes_fevereiro != 0)
        {
            $cac_fevereiro = ($investimento_fevereiro + $despesa_fevereiro) / $novos_clientes_fevereiro;
        }
        else
        {
            $cac_fevereiro = 0;
        }

        return $cac_fevereiro;



    }


    /*CAC de Março*/

    public function cacMarcoAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        

        $investimento_marco = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
            ->where('investimentos.periodo_id', '=', 3)
            ->where('investimentos.ano_id', '=', $data_atual->year)
            ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(investimentos.valor) as valor'))
            ->value('valor');

            $despesa_marco = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
            ->where('despesas.periodo_id', '=', 3)
            ->where('despesas.ano_id', '=', $data_atual->year)
            ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(despesas.valor) as valor'))
            ->value('valor');

            $novos_clientes_marco = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
            ->where('clientes.periodo_id', '=', 3)
            ->where('clientes.ano_id', '=', $data_atual->year)
            ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(clientes.novos_clientes) as novos_clientes'))
            ->value('novos_clientes');

        if ($novos_clientes_marco != 0)
        {
            $cac_marco = ($investimento_marco + $despesa_marco) / $novos_clientes_marco;
        }
        else
        {
            $cac_marco = 0;
        }

        return $cac_marco;


    }


    /*CAC de Abril*/

    public function cacAbrilAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

       

        $investimento_abril = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 4)
        ->where('investimentos.ano_id', '=', $data_atual->year)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

        $despesa_abril = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
        ->where('despesas.periodo_id', '=', 4)
        ->where('despesas.ano_id', '=', $data_atual->year)
        ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(despesas.valor) as valor'))
        ->value('valor');

        $novos_clientes_abril = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
            ->where('clientes.periodo_id', '=', 4)
            ->where('clientes.ano_id', '=', $data_atual->year)
            ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(clientes.novos_clientes) as novos_clientes'))
            ->value('novos_clientes');

        if ($novos_clientes_abril != 0)
        {
            $cac_abril = ($investimento_abril + $despesa_abril) / $novos_clientes_abril;
        }
        else
        {
            $cac_abril = 0;
        } 

        return $cac_abril;

    }

    /*CAC de Maio*/

    public function cacMaioAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

    
        $investimento_maio = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 5)
        ->where('investimentos.ano_id', '=', $data_atual->year)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

        $despesa_maio = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
        ->where('despesas.periodo_id', '=', 5)
        ->where('despesas.ano_id', '=', $data_atual->year)
        ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(despesas.valor) as valor'))
        ->value('valor');

        $novos_clientes_maio = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
            ->where('clientes.periodo_id', '=', 5)
            ->where('clientes.ano_id', '=', $data_atual->year)
            ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(clientes.novos_clientes) as novos_clientes'))
            ->value('novos_clientes');

        if ($novos_clientes_maio != 0)
        {
            $cac_maio = ($investimento_maio + $despesa_maio) / $novos_clientes_maio;
        }
        else
        {
            $cac_maio = 0;
        }

        return $cac_maio;

    }

    /*CAC de Junho*/

    public function cacJunhoAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        


        $investimento_junho = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 6)
        ->where('investimentos.ano_id', '=', $data_atual->year)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

        $despesa_junho = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
        ->where('despesas.periodo_id', '=', 6)
        ->where('despesas.ano_id', '=', $data_atual->year)
        ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(despesas.valor) as valor'))
        ->value('valor');

            $novos_clientes_junho = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
            ->where('clientes.periodo_id', '=', 6)
            ->where('clientes.ano_id', '=', $data_atual->year)
            ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(clientes.novos_clientes) as novos_clientes'))
            ->value('novos_clientes');

            if ($novos_clientes_junho != 0)
        {
            $cac_junho = ($investimento_junho + $despesa_junho) / $novos_clientes_junho;
        }
        else
        {
            $cac_junho = 0;
        }

        return $cac_junho;

    }


    /*CAC de Julho*/

    public function cacJulhoAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

          

        $investimento_julho = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 7)
        ->where('investimentos.ano_id', '=', $data_atual->year)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

        $despesa_julho = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
        ->where('despesas.periodo_id', '=', 7)
        ->where('despesas.ano_id', '=', $data_atual->year)
        ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(despesas.valor) as valor'))
        ->value('valor');

        $novos_clientes_julho = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
            ->where('clientes.periodo_id', '=', 7)
            ->where('clientes.ano_id', '=', $data_atual->year)
            ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(clientes.novos_clientes) as novos_clientes'))
            ->value('novos_clientes');

            if ($novos_clientes_julho != 0)
        {
            $cac_julho = ($investimento_julho + $despesa_julho) / $novos_clientes_julho;
        }
        else
        {
            $cac_julho = 0;
        }

        return $cac_julho;

    }

    /*CAC de Agosto*/

    public function cacAgostoAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        

        $investimento_agosto = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 8)
        ->where('investimentos.ano_id', '=', $data_atual->year)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

        $despesa_agosto = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
        ->where('despesas.periodo_id', '=', 8)
        ->where('despesas.ano_id', '=', $data_atual->year)
        ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(despesas.valor) as valor'))
        ->value('valor');

        $novos_clientes_agosto = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
        ->where('clientes.periodo_id', '=', 8)
        ->where('clientes.ano_id', '=', $data_atual->year)
        ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(clientes.novos_clientes) as novos_clientes'))
        ->value('novos_clientes');

        if ($novos_clientes_agosto != 0)
        {
            $cac_agosto = ($investimento_agosto + $despesa_agosto) / $novos_clientes_agosto;
        }
        else
        {
            $cac_agosto = 0;
        }

        return $cac_agosto;

    }

    /*CAC de Setembro*/

    public function cacSetembroAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

       
        
        $investimento_setembro = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
            ->where('investimentos.periodo_id', '=', 9)
            ->where('investimentos.ano_id', '=', $data_atual->year)
            ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(investimentos.valor) as valor'))
            ->value('valor');

            $despesa_setembro = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
            ->where('despesas.periodo_id', '=', 9)
            ->where('despesas.ano_id', '=', $data_atual->year)
            ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(despesas.valor) as valor'))
            ->value('valor');

            $novos_clientes_setembro = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
            ->where('clientes.periodo_id', '=', 9)
            ->where('clientes.ano_id', '=', $data_atual->year)
            ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(clientes.novos_clientes) as novos_clientes'))
            ->value('novos_clientes');


            if ($novos_clientes_setembro != 0)
            {
                $cac_setembro = ($investimento_setembro + $despesa_setembro) / $novos_clientes_setembro;
            }
            else
            {
                $cac_setembro = 0;
            }

            return $cac_setembro;
    


    }

    /*CAC de Outubro*/

    public function cacOutubroAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

       

        $investimento_outubro = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
            ->where('investimentos.periodo_id', '=', 10)
            ->where('investimentos.ano_id', '=', $data_atual->year)
            ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(investimentos.valor) as valor'))
            ->value('valor');

            $despesa_outubro = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
            ->where('despesas.periodo_id', '=', 10)
            ->where('despesas.ano_id', '=', $data_atual->year)
            ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(despesas.valor) as valor'))
            ->value('valor');
            
            $novos_clientes_outubro = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
            ->where('clientes.periodo_id', '=', 10)
            ->where('clientes.ano_id', '=', $data_atual->year)
            ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(clientes.novos_clientes) as novos_clientes'))
            ->value('novos_clientes');

            if ($novos_clientes_outubro != 0)
            {
                $cac_outubro = ($investimento_outubro + $despesa_outubro) / $novos_clientes_outubro;
            }
            else
            {
                $cac_outubro = 0;
            }

            return $cac_outubro;


    }

    /*CAC de Novembro*/

    public function cacNovembroAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        
        

        $investimento_novembro = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
            ->where('investimentos.periodo_id', '=', 11)
            ->where('investimentos.ano_id', '=', $data_atual->year)
            ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(investimentos.valor) as valor'))
            ->value('valor');

            $despesa_novembro = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
            ->where('despesas.periodo_id', '=', 11)
            ->where('despesas.ano_id', '=', $data_atual->year)
            ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(despesas.valor) as valor'))
            ->value('valor');

            $novos_clientes_novembro = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
            ->where('clientes.periodo_id', '=', 11)
            ->where('clientes.ano_id', '=', $data_atual->year)
            ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(clientes.novos_clientes) as novos_clientes'))
            ->value('novos_clientes');

            if ($novos_clientes_novembro != 0)
            {
                $cac_novembro = ($investimento_novembro + $despesa_novembro) / $novos_clientes_novembro;
            }
            else
            {
                $cac_novembro = 0;
            }

            return $cac_novembro;


    }

    /*CAC de Dezembro*/

    public function cacDezembroAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

     
        

        $investimento_dezembro = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 12)
        ->where('investimentos.ano_id', '=', $data_atual->year)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

        $despesa_dezembro = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
        ->where('despesas.periodo_id', '=', 12)
        ->where('despesas.ano_id', '=', $data_atual->year)
        ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(despesas.valor) as valor'))
        ->value('valor');

        $novos_clientes_dezembro = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
            ->where('clientes.periodo_id', '=', 12)
            ->where('clientes.ano_id', '=', $data_atual->year)
            ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(clientes.novos_clientes) as novos_clientes'))
            ->value('novos_clientes');

            if ($novos_clientes_dezembro != 0)
            {
                $cac_dezembro = ($investimento_dezembro + $despesa_dezembro) / $novos_clientes_dezembro;
            }
            else
            {
                $cac_dezembro = 0;
            }

            return $cac_dezembro;


    }







    public function paginaCacAPI(Request $request)
    {

      

        $usuarioAutenticadoId = $request->session()->get('usuarioId');
       
        $data_atual = Carbon::now();



       //DADOS DO CAC ATUAL
       $cac_pagina = $this->cacPaginaAPI($request);

       //TICKET MEDIO ATUAL
       $ticket_medio_comparacao = app('App\Http\Controllers\TicketMedioMobileController')->ticketMedioPaginaAPI($request);

       //CAC DEZEMBRO DO ANO PASSADO
       $cac_dezembro_ano_passado = $this->cacDezembroAnoPassadoAPI($request);
        
       //CAC JANEIRO
        $cac_janeiro = $this->cacJaneiroAPI($request);

        //CAC FEVEREIRO
        $cac_fevereiro = $this->cacFevereiroAPI($request);

        //CAC MARÇO
        $cac_marco = $this->cacMarcoAPI($request);
      
        //CAC ABRIL
        $cac_abril = $this->cacAbrilAPI($request);

        //CAC MAIO
        $cac_maio = $this->cacMaioAPI($request);

        //CAC JUNHO
        $cac_junho = $this->cacJunhoAPI($request);

        //CAC JULHO
        $cac_julho = $this->cacJulhoAPI($request);

        //CAC AGOSTO
        $cac_agosto = $this->cacAgostoAPI($request);
       
        //CAC SETEMBRO
        $cac_setembro = $this->cacSetembroAPI($request);

        //CAC OUTUBRO
        $cac_outubro = $this->cacOutubroAPI($request);

        //CAC NOVEMBRO
        $cac_novembro = $this->cacNovembroAPI($request);

        //CAC DEZEMBRO
        $cac_dezembro = $this->cacDezembroAPI($request);


        //Comparação de Dados

        //Janeiro/Dezembro

        if ($cac_janeiro == 0 || $cac_dezembro_ano_passado == 0)
        {
            $cac_janeiro_dezembro = 0;
        }
        else
        {
            $cac_janeiro_dezembro = (($cac_janeiro / $cac_dezembro_ano_passado) - 1) * 100;
        }

        //Fevereiro/Janeiro
        if ($cac_fevereiro == 0 || $cac_janeiro == 0)
        {
            $cac_fevereiro_janeiro = 0;
        }
        else
        {
            $cac_fevereiro_janeiro = (($cac_fevereiro / $cac_janeiro) - 1) * 100;
        }

        //Marco/Fevereiro
        if ($cac_marco == 0 || $cac_fevereiro == 0)
        {
            $cac_marco_fevereiro = 0;
        }
        else
        {
            $cac_marco_fevereiro = (($cac_marco / $cac_fevereiro) - 1) * 100;
        }

        //Abril/Marco
        if ($cac_abril == 0 || $cac_marco == 0)
        {
            $cac_abril_marco = 0;
        }
        else
        {
            $cac_abril_marco = (($cac_abril / $cac_marco) - 1) * 100;
        }

        //Maio/Abril
        if ($cac_maio == 0 || $cac_abril == 0)
        {
            $cac_maio_abril = 0;
        }
        else
        {
            $cac_maio_abril = (($cac_maio / $cac_abril) - 1) * 100;
        }

        //Junho/Maio
        if ($cac_junho == 0 || $cac_maio == 0)
        {
            $cac_junho_maio = 0;
        }
        else
        {
            $cac_junho_maio = (($cac_junho / $cac_maio) - 1) * 100;
        }

        //Julho/Junho
        if ($cac_julho == 0 || $cac_junho == 0)
        {
            $cac_julho_junho = 0;
        }
        else
        {
            $cac_julho_junho = (($cac_julho / $cac_junho) - 1) * 100;
        }

        //Agosto/Julho
        if ($cac_agosto == 0 || $cac_julho == 0)
        {
            $cac_agosto_julho = 0;
        }
        else
        {
            $cac_agosto_julho = (($cac_agosto / $cac_julho) - 1) * 100;
        }

        //Setembro/Agosto
        if ($cac_setembro == 0 || $cac_agosto == 0)
        {
            $cac_setembro_agosto = 0;
        }
        else
        {
            $cac_setembro_agosto = (($cac_setembro / $cac_agosto) - 1) * 100;
        }

        //Outubro/Setembro
        if ($cac_outubro == 0 || $cac_setembro == 0)
        {
            $cac_outubro_setembro = 0;
        }
        else
        {
            $cac_outubro_setembro = (($cac_outubro / $cac_setembro) - 1) * 100;
        }

        //Novembro/Outubro
        if ($cac_novembro == 0 || $cac_outubro == 0)
        {
            $cac_novembro_outubro = 0;
        }
        else
        {
            $cac_novembro_outubro = (($cac_novembro / $cac_outubro) - 1) * 100;
        }

        //Dezembro/Novembro
        if ($cac_dezembro == 0 || $cac_novembro == 0)
        {
            $cac_dezembro_novembro = 0;
        }
        else
        {
            $cac_dezembro_novembro = (($cac_dezembro / $cac_novembro) - 1) * 100;
        }

        if ($cac_pagina < $ticket_medio_comparacao)
        {
            $valida_cac = "Seu custo de aquisição está bom.";
        }
        else if ($cac_pagina > $ticket_medio_comparacao)
        {
            $valida_cac = "Seu custo de aquisição está alto.";
        }
        else if ($cac_pagina == 0 && $ticket_medio_comparacao == 0)
        {
            $valida_cac = "Sem dados para avaliar o custo de aquisição.";
        } else {
            
            $valida_cac = "Sem dados para avaliar o custo de aquisição.";

        }

        
        return response()->json([
            'cac_pagina' => number_format($cac_pagina,2),
            'valida_cac' => $valida_cac,
            'cac_janeiro' => ($cac_janeiro),
            'cac_fevereiro' => ($cac_fevereiro),
            'cac_marco' => ($cac_marco),
            'cac_abril' => ($cac_abril),
            'cac_maio' => ($cac_maio),
            'cac_junho' => ($cac_junho),
            'cac_julho' => ($cac_julho),
            'cac_agosto' => ($cac_agosto),
            'cac_setembro' => ($cac_setembro),
            'cac_outubro' => ($cac_outubro),
            'cac_novembro' => ($cac_novembro),
            'cac_dezembro' => ($cac_dezembro),
        ]);

      
    }


    /*CAC PESQUISA PERIODO*/

    public function cacPesquisaPeriodo(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $investimento_atual = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', $periodo_id)
        ->where('investimentos.ano_id', '=', $ano_id)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)
        ->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

        $despesa_atual = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
        ->where('despesas.periodo_id', '=', $periodo_id)
        ->where('despesas.ano_id', '=', $ano_id)
        ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)
        ->select(DB::raw('SUM(despesas.valor) as valor'))
        ->value('valor');

        $novos_clientes_atual = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
        ->where('clientes.periodo_id', '=', $periodo_id)
        ->where('clientes.ano_id', '=', $ano_id)
        ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)
        ->select(DB::raw('SUM(clientes.novos_clientes) as novos_clientes'))
        ->value('novos_clientes');

        if ($novos_clientes_atual != 0)
        {
            $cac_comparacao = ($investimento_atual + $despesa_atual) / $novos_clientes_atual;
        }
        else
        {
            $cac_comparacao = 0;
        }

        return $cac_comparacao;


    }





     /*CAC PESQUISA PERIODO DE JANEIRO*/

     public function cacPesquisaPeriodoJaneiro(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $empresa_id = $request->empresa_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $investimento_janeiro = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 1)
        ->where('investimentos.ano_id', '=', $ano_id)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)
        
        ->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

        $despesa_janeiro = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
        ->where('despesas.periodo_id', '=', 1)->where('despesas.ano_id', '=', $ano_id)
        ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)

        
        ->select(DB::raw('SUM(despesas.valor) as valor'))
        ->value('valor');

        $novos_clientes_janeiro = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
        ->where('clientes.periodo_id', '=', 1)
        ->where('clientes.ano_id', '=', $ano_id)
        ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)
        
        
        ->select(DB::raw('SUM(clientes.novos_clientes) as novos_clientes'))
        ->value('novos_clientes');

        if ($novos_clientes_janeiro != 0)
        {
            $cac_janeiro = ($investimento_janeiro + $despesa_janeiro) / $novos_clientes_janeiro;
        }
        else
        {
            $cac_janeiro = 0;
        }

        return $cac_janeiro;


    }

    //CAC PESQUISA PERIODO DE FEVEREIRO

    public function cacPesquisaPeriodoFevereiro(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $investimento_fevereiro = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 2)
        ->where('investimentos.ano_id', '=', $ano_id)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)
        
        ->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

        $despesa_fevereiro = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
        ->where('despesas.periodo_id', '=', 2)->where('despesas.ano_id', '=', $ano_id)
        ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)

        
        ->select(DB::raw('SUM(despesas.valor) as valor'))
        ->value('valor');

        $novos_clientes_fevereiro = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
        ->where('clientes.periodo_id', '=', 2)
        ->where('clientes.ano_id', '=', $ano_id)
        ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)
        
        
        ->select(DB::raw('SUM(clientes.novos_clientes) as novos_clientes'))
        ->value('novos_clientes');

        if ($novos_clientes_fevereiro != 0)
        {
            $cac_fevereiro = ($investimento_fevereiro + $despesa_fevereiro) / $novos_clientes_fevereiro;
        }
        else
        {
            $cac_fevereiro = 0;
        }

        return $cac_fevereiro;

    }

    //CAC PESQUISA PERIODO DE MARÇO

    public function cacPesquisaPeriodoMarco(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $investimento_marco = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 3)
        ->where('investimentos.ano_id', '=', $ano_id)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)
        
        ->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

        $despesa_marco = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
        ->where('despesas.periodo_id', '=', 3)->where('despesas.ano_id', '=', $ano_id)
        ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)

        
        
        ->select(DB::raw('SUM(despesas.valor) as valor'))
        ->value('valor');

        $novos_clientes_marco = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
        ->where('clientes.periodo_id', '=', 3)
        ->where('clientes.ano_id', '=', $ano_id)
        ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)
        
        
        ->select(DB::raw('SUM(clientes.novos_clientes) as novos_clientes'))
        ->value('novos_clientes');

        if ($novos_clientes_marco != 0)
        {
            $cac_marco = ($investimento_marco + $despesa_marco) / $novos_clientes_marco;
        }
        else
        {
            $cac_marco = 0;
        }

        return $cac_marco;

    }



     //CAC PESQUISA PERIODO DE ABRIL

     public function cacPesquisaPeriodoAbril(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $investimento_abril = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 4)
        ->where('investimentos.ano_id', '=', $ano_id)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)
        

        ->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

        $despesa_abril = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
        ->where('despesas.periodo_id', '=', 4)->where('despesas.ano_id', '=', $ano_id)
        ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)

        
        ->select(DB::raw('SUM(despesas.valor) as valor'))
        ->value('valor');

        $novos_clientes_abril = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
        ->where('clientes.periodo_id', '=', 4)
        ->where('clientes.ano_id', '=', $ano_id)
        ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)
        
        
        ->select(DB::raw('SUM(clientes.novos_clientes) as novos_clientes'))
        ->value('novos_clientes');

        if ($novos_clientes_abril != 0)
        {
            $cac_abril = ($investimento_abril + $despesa_abril) / $novos_clientes_abril;
        }
        else
        {
            $cac_abril = 0;
        }

        return $cac_abril;

    }






    //CAC PESQUISA PERIODO DE MAIO

    public function cacPesquisaPeriodoMaio(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $investimento_maio = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 5)
        ->where('investimentos.ano_id', '=', $ano_id)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)
        
        ->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

        $despesa_maio = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
        ->where('despesas.periodo_id', '=', 5)->where('despesas.ano_id', '=', $ano_id)
        ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)

        
        ->select(DB::raw('SUM(despesas.valor) as valor'))
        ->value('valor');

        $novos_clientes_maio = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
        ->where('clientes.periodo_id', '=', 5)
        ->where('clientes.ano_id', '=', $ano_id)
        ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)
        
        
        ->select(DB::raw('SUM(clientes.novos_clientes) as novos_clientes'))
        ->value('novos_clientes');

        if ($novos_clientes_maio != 0)
        {
            $cac_maio = ($investimento_maio + $despesa_maio) / $novos_clientes_maio;
        }
        else
        {
            $cac_maio = 0;
        }

        return $cac_maio;

    }



    //CAC PESQUISA PERIODO DE JUNHO

    public function cacPesquisaPeriodoJunho(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $investimento_junho = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 6)
        ->where('investimentos.ano_id', '=', $ano_id)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)
        
        ->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

        $despesa_junho = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
        ->where('despesas.periodo_id', '=', 6)->where('despesas.ano_id', '=', $ano_id)
        ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)

        
        ->select(DB::raw('SUM(despesas.valor) as valor'))
        ->value('valor');

        $novos_clientes_junho = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
        ->where('clientes.periodo_id', '=', 6)
        ->where('clientes.ano_id', '=', $ano_id)
        ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)
        
        
        ->select(DB::raw('SUM(clientes.novos_clientes) as novos_clientes'))
        ->value('novos_clientes');

        if ($novos_clientes_junho != 0)
        {
            $cac_junho = ($investimento_junho + $despesa_junho) / $novos_clientes_junho;
        }
        else
        {
            $cac_junho = 0;
        }

        return $cac_junho;

    }







     //CAC PESQUISA PERIODO DE JULHO

     public function cacPesquisaPeriodoJulho(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $investimento_julho = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 7)
        ->where('investimentos.ano_id', '=', $ano_id)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)
        
        ->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

        $despesa_julho = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
        ->where('despesas.periodo_id', '=', 7)->where('despesas.ano_id', '=', $ano_id)
        ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)

        
        
        ->select(DB::raw('SUM(despesas.valor) as valor'))
        ->value('valor');

        $novos_clientes_julho = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
        ->where('clientes.periodo_id', '=', 7)
        ->where('clientes.ano_id', '=', $ano_id)
        ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)
        
        
        ->select(DB::raw('SUM(clientes.novos_clientes) as novos_clientes'))
        ->value('novos_clientes');

        if ($novos_clientes_julho != 0)
        {
            $cac_julho = ($investimento_julho + $despesa_julho) / $novos_clientes_julho;
        }
        else
        {
            $cac_julho = 0;
        }

        return $cac_julho;

    }







     //CAC PESQUISA PERIODO DE AGOSTO

     public function cacPesquisaPeriodoAgosto(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $investimento_agosto = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 8)
        ->where('investimentos.ano_id', '=', $ano_id)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)
        
        ->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

        $despesa_agosto = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
        ->where('despesas.periodo_id', '=', 8)->where('despesas.ano_id', '=', $ano_id)
        ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)

        
        ->select(DB::raw('SUM(despesas.valor) as valor'))
        ->value('valor');

        $novos_clientes_agosto = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
        ->where('clientes.periodo_id', '=', 8)
        ->where('clientes.ano_id', '=', $ano_id)
        ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)
        
        
        ->select(DB::raw('SUM(clientes.novos_clientes) as novos_clientes'))
        ->value('novos_clientes');

        if ($novos_clientes_agosto != 0)
        {
            $cac_agosto = ($investimento_agosto + $despesa_agosto) / $novos_clientes_agosto;
        }
        else
        {
            $cac_agosto = 0;
        }

        return $cac_agosto;

    }




     //CAC PESQUISA PERIODO DE SETEMBRO

     public function cacPesquisaPeriodoSetembro(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $investimento_setembro = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 9)
        ->where('investimentos.ano_id', '=', $ano_id)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)
        
        ->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

        $despesa_setembro = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
        ->where('despesas.periodo_id', '=', 9)->where('despesas.ano_id', '=', $ano_id)
        ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)

        
        ->select(DB::raw('SUM(despesas.valor) as valor'))
        ->value('valor');

        $novos_clientes_setembro = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
        ->where('clientes.periodo_id', '=', 9)
        ->where('clientes.ano_id', '=', $ano_id)
        ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)
        
        
        ->select(DB::raw('SUM(clientes.novos_clientes) as novos_clientes'))
        ->value('novos_clientes');

        if ($novos_clientes_setembro != 0)
        {
            $cac_setembro = ($investimento_setembro + $despesa_setembro) / $novos_clientes_setembro;
        }
        else
        {
            $cac_setembro = 0;
        }

        return $cac_setembro;

    }






    //CAC PESQUISA PERIODO DE OUTUBRO

    public function cacPesquisaPeriodoOutubro(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $investimento_outubro = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 10)
        ->where('investimentos.ano_id', '=', $ano_id)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)
        
        ->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

        $despesa_outubro = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
        ->where('despesas.periodo_id', '=', 10)->where('despesas.ano_id', '=', $ano_id)
        ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)

        
        ->select(DB::raw('SUM(despesas.valor) as valor'))
        ->value('valor');

        $novos_clientes_outubro = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
        ->where('clientes.periodo_id', '=', 10)
        ->where('clientes.ano_id', '=', $ano_id)
        ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)
        
        
        ->select(DB::raw('SUM(clientes.novos_clientes) as novos_clientes'))
        ->value('novos_clientes');

        if ($novos_clientes_outubro != 0)
        {
            $cac_outubro = ($investimento_outubro + $despesa_outubro) / $novos_clientes_outubro;
        }
        else
        {
            $cac_outubro = 0;
        }

        return $cac_outubro;

    }







     //CAC PESQUISA PERIODO DE NOVEMBRO

     public function cacPesquisaPeriodoNovembro(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $investimento_novembro = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 11)
        ->where('investimentos.ano_id', '=', $ano_id)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)
        
        ->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

        $despesa_novembro = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
        ->where('despesas.periodo_id', '=', 11)->where('despesas.ano_id', '=', $ano_id)
        ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)

        
        ->select(DB::raw('SUM(despesas.valor) as valor'))
        ->value('valor');

        $novos_clientes_novembro = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
        ->where('clientes.periodo_id', '=', 11)
        ->where('clientes.ano_id', '=', $ano_id)
        ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)
        
        
        ->select(DB::raw('SUM(clientes.novos_clientes) as novos_clientes'))
        ->value('novos_clientes');

        if ($novos_clientes_novembro != 0)
        {
            $cac_novembro = ($investimento_novembro + $despesa_novembro) / $novos_clientes_novembro;
        }
        else
        {
            $cac_novembro = 0;
        }

        return $cac_novembro;

    }






     //CAC PESQUISA PERIODO DE DEZEMBRO

     public function cacPesquisaPeriodoDezembro(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $investimento_dezembro = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 12)
        ->where('investimentos.ano_id', '=', $ano_id)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)

        ->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

        $despesa_dezembro = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
        ->where('despesas.periodo_id', '=', 12)->where('despesas.ano_id', '=', $ano_id)
        ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)

        
        ->select(DB::raw('SUM(despesas.valor) as valor'))
        ->value('valor');

        $novos_clientes_dezembro = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
        ->where('clientes.periodo_id', '=', 12)
        ->where('clientes.ano_id', '=', $ano_id)
        ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)
        
        
        ->select(DB::raw('SUM(clientes.novos_clientes) as novos_clientes'))
        ->value('novos_clientes');

        if ($novos_clientes_dezembro != 0)
        {
            $cac_dezembro = ($investimento_dezembro + $despesa_dezembro) / $novos_clientes_dezembro;
        }
        else
        {
            $cac_dezembro = 0;
        }

        return $cac_dezembro;

    }







    public function pesquisaCac(Request $request)
    {

       

        $usuarioAutenticadoId = $request->session()->get('usuarioId');
    

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $empresa_id = $request->empresa_id;


        //CAC ATUAL NO PERIODO
        $cac = $this->cacPesquisaPeriodo($request);

        //TICKET MEDIO NO PERIODO
        $ticket_medio_comparacao = app('App\Http\Controllers\TicketMedioMobileController')->ticketMedioPesquisaPeriodo($request);

        
        //CAC JANEIRO
        $cac_janeiro = $this->cacPesquisaPeriodoJaneiro($request);


        //CAC FEVEREIRO
        $cac_fevereiro = $this->cacPesquisaPeriodoFevereiro($request);


        //CAC MARÇO
        $cac_marco = $this->cacPesquisaPeriodoMarco($request);

        //CAC ABRIL
        $cac_abril = $this->cacPesquisaPeriodoAbril($request);

        //CAC MAIO
        $cac_maio = $this->cacPesquisaPeriodoMaio($request);


        //CAC JUNHO
        $cac_junho = $this->cacPesquisaPeriodoJunho($request);


        //CAC JULHO
        $cac_julho = $this->cacPesquisaPeriodoJulho($request);


        //CAC AGOSTO
        $cac_agosto = $this->cacPesquisaPeriodoAgosto($request);


        //CAC SETEMBRO
        $cac_setembro = $this->cacPesquisaPeriodoSetembro($request);


        //CAC OUTUBRO
        $cac_outubro = $this->cacPesquisaPeriodoOutubro($request);


        //CAC NOVEMBRO
        $cac_novembro = $this->cacPesquisaPeriodoNovembro($request);


        //CAC DEZEMBRO
        $cac_dezembro = $this->cacPesquisaPeriodoDezembro($request);


        if ($cac < $ticket_medio_comparacao)
        {
            $valida_cac = "Seu custo de aquisição está bom.";
        }
        else if ($cac > $ticket_medio_comparacao)
        {
            $valida_cac = "Seu custo de aquisição está alto.";
        }
        else if ($cac == 0 && $ticket_medio_comparacao == 0)
        {
            $valida_cac = "Sem dados para avaliar o custo de aquisição.";
        }

        return response()->json([
            'cac' => number_format($cac,2),
            'valida_cac' => $valida_cac,
            'cac_janeiro' => $cac_janeiro,
            'cac_fevereiro' => $cac_fevereiro,
            'cac_marco' => $cac_marco,
            'cac_abril' => $cac_abril,
            'cac_maio' => $cac_maio,
            'cac_junho' => $cac_junho,
            'cac_julho' => $cac_julho,
            'cac_agosto' => $cac_agosto,
            'cac_setembro' => $cac_setembro,
            'cac_outubro' => $cac_outubro,
            'cac_novembro' => $cac_novembro,
            'cac_dezembro' => $cac_dezembro,


        ]);



    }




}