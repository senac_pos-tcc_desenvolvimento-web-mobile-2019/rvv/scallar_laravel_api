<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuario;
use App\Cargo;
use App\Empresa;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\Routing\ResponseFactory;
use Auth;
use Session;
use Carbon\Carbon;



class PAYBACKMobileController extends Controller
{

    public function payBackAtualAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');




        //DADOS DO CAC ATUAL
        $cac_comparacao = app('App\Http\Controllers\CACMobileController')->cacPaginaAPI($request);

        //TICKET MEDIO ATUAL
        $ticket_medio_comparacao = app('App\Http\Controllers\TicketMedioMobileController')->ticketMedioPaginaAPI($request);
        

        //PAYBACK ATUAL        

        if ($ticket_medio_comparacao != 0)
        {
            $payback_pagina = $cac_comparacao / $ticket_medio_comparacao;
        }
        else
        {
            $payback_pagina = 0;
        }

        return $payback_pagina;



    }






    public function payBackJaneiroAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        //DADOS DO CAC ATUAL
        $cac_janeiro = app('App\Http\Controllers\CACMobileController')->cacJaneiroAPI($request);

        //TICKET MEDIO ATUAL
        $ticket_medio_janeiro = app('App\Http\Controllers\TicketMedioMobileController')->ticketMedioJaneiroAPI($request);
        

        //PAYBACK ATUAL        

        if ($ticket_medio_janeiro != 0)
        {
            $payback_janeiro = $cac_janeiro / $ticket_medio_janeiro;
        }
        else
        {
            $payback_janeiro = 0;
        }

        return $payback_janeiro;



    }






    public function payBackFevereiroAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        //DADOS DO CAC ATUAL
        $cac_fevereiro = app('App\Http\Controllers\CACMobileController')->cacFevereiroAPI($request);

        //TICKET MEDIO ATUAL
        $ticket_medio_fevereiro = app('App\Http\Controllers\TicketMedioMobileController')->ticketMedioFevereiroAPI($request);
        

        //PAYBACK ATUAL        

        if ($ticket_medio_fevereiro != 0)
        {
            $payback_fevereiro = $cac_fevereiro / $ticket_medio_fevereiro;
        }
        else
        {
            $payback_fevereiro = 0;
        }

        return $payback_fevereiro;



    }






    public function payBackMarcoAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        //DADOS DO CAC ATUAL
        $cac_marco = app('App\Http\Controllers\CACMobileController')->cacMarcoAPI($request);

        //TICKET MEDIO ATUAL
        $ticket_medio_marco = app('App\Http\Controllers\TicketMedioMobileController')->ticketMedioMarcoAPI($request);
        

        //PAYBACK ATUAL        

        if ($ticket_medio_marco != 0)
        {
            $payback_marco = $cac_marco / $ticket_medio_marco;
        }
        else
        {
            $payback_marco = 0;
        }

        return $payback_marco;



    }







    public function payBackAbrilAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        //DADOS DO CAC ATUAL
        $cac_abril = app('App\Http\Controllers\CACMobileController')->cacAbrilAPI($request);

        //TICKET MEDIO ATUAL
        $ticket_medio_abril = app('App\Http\Controllers\TicketMedioMobileController')->ticketMedioAbrilAPI($request);
        

        //PAYBACK ATUAL        

        if ($ticket_medio_abril != 0)
        {
            $payback_abril = $cac_abril / $ticket_medio_abril;
        }
        else
        {
            $payback_abril = 0;
        }

        return $payback_abril;



    }







    public function payBackMaioAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        //DADOS DO CAC ATUAL
        $cac_maio = app('App\Http\Controllers\CACMobileController')->cacMaioAPI($request);

        //TICKET MEDIO ATUAL
        $ticket_medio_maio = app('App\Http\Controllers\TicketMedioMobileController')->ticketMedioMaioAPI($request);
        

        //PAYBACK ATUAL        

        if ($ticket_medio_maio != 0)
        {
            $payback_maio = $cac_maio / $ticket_medio_maio;
        }
        else
        {
            $payback_maio = 0;
        }

        return $payback_maio;



    }











    
    public function payBackJunhoAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        //DADOS DO CAC ATUAL
        $cac_junho = app('App\Http\Controllers\CACMobileController')->cacJunhoAPI($request);

        //TICKET MEDIO ATUAL
        $ticket_medio_junho = app('App\Http\Controllers\TicketMedioMobileController')->ticketMedioJunhoAPI($request);
        

        //PAYBACK ATUAL        

        if ($ticket_medio_junho != 0)
        {
            $payback_junho = $cac_maio / $ticket_medio_maio;
        }
        else
        {
            $payback_junho = 0;
        }

        return $payback_junho;



    }










    public function payBackJulhoAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        //DADOS DO CAC ATUAL
        $cac_julho = app('App\Http\Controllers\CACMobileController')->cacJulhoAPI($request);

        //TICKET MEDIO ATUAL
        $ticket_medio_julho = app('App\Http\Controllers\TicketMedioMobileController')->ticketMedioJulhoAPI($request);
        

        //PAYBACK ATUAL        

        if ($ticket_medio_julho != 0)
        {
            $payback_julho = $cac_julho / $ticket_medio_julho;
        }
        else
        {
            $payback_julho = 0;
        }

        return $payback_julho;



    }







    public function payBackAgostoAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        //DADOS DO CAC ATUAL
        $cac_agosto = app('App\Http\Controllers\CACMobileController')->cacAgostoAPI($request);

        //TICKET MEDIO ATUAL
        $ticket_medio_agosto = app('App\Http\Controllers\TicketMedioMobileController')->ticketMedioAgostoAPI($request);
        

        //PAYBACK ATUAL        

        if ($ticket_medio_agosto != 0)
        {
            $payback_agosto = $cac_agosto / $ticket_medio_agosto;
        }
        else
        {
            $payback_agosto = 0;
        }

        return $payback_agosto;



    }








    
    public function payBackSetembroAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        //DADOS DO CAC ATUAL
        $cac_setembro = app('App\Http\Controllers\CACMobileController')->cacSetembroAPI($request);

        //TICKET MEDIO ATUAL
        $ticket_medio_setembro = app('App\Http\Controllers\TicketMedioMobileController')->ticketMedioSetembroAPI($request);
        

        //PAYBACK ATUAL        

        if ($ticket_medio_setembro != 0)
        {
            $payback_setembro = $cac_setembro / $ticket_medio_setembro;
        }
        else
        {
            $payback_setembro = 0;
        }

        return $payback_setembro;



    }







    public function payBackOutubroAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        //DADOS DO CAC ATUAL
        $cac_outubro = app('App\Http\Controllers\CACMobileController')->cacOutubroAPI($request);

        //TICKET MEDIO ATUAL
        $ticket_medio_outubro = app('App\Http\Controllers\TicketMedioMobileController')->ticketMedioOutubroAPI($request);
        

        //PAYBACK ATUAL        

        if ($ticket_medio_outubro != 0)
        {
            $payback_outubro = $cac_outubro / $ticket_medio_outubro;
        }
        else
        {
            $payback_outubro = 0;
        }

        return $payback_outubro;



    }










    public function payBackNovembroAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        //DADOS DO CAC ATUAL
        $cac_novembro = app('App\Http\Controllers\CACMobileController')->cacNovembroAPI($request);

        //TICKET MEDIO ATUAL
        $ticket_medio_novembro = app('App\Http\Controllers\TicketMedioMobileController')->ticketMedioNovembroAPI($request);
        

        //PAYBACK ATUAL        

        if ($ticket_medio_novembro != 0)
        {
            $payback_novembro = $cac_novembro / $ticket_medio_novembro;
        }
        else
        {
            $payback_novembro = 0;
        }

        return $payback_novembro;



    }










    public function payBackDezembroAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        //DADOS DO CAC ATUAL
        $cac_dezembro = app('App\Http\Controllers\CACMobileController')->cacDezembroAPI($request);

        //TICKET MEDIO ATUAL
        $ticket_medio_dezembro = app('App\Http\Controllers\TicketMedioMobileController')->ticketMedioDezembroAPI($request);
        

        //PAYBACK ATUAL        

        if ($ticket_medio_dezembro != 0)
        {
            $payback_dezembro = $cac_dezembro / $ticket_medio_dezembro;
        }
        else
        {
            $payback_dezembro = 0;
        }

        return $payback_dezembro;



    }






    public function paginaPayBackAPI(Request $request)
    {

         //CAC ATUAL
         $cac_pagina = app('App\Http\Controllers\CACMobileController')->cacPaginaAPI($request);

          //TICKET MEDIO ATUAL
          $ticket_medio_pagina = app('App\Http\Controllers\TicketMedioMobileController')->ticketMedioPaginaAPI($request);

        //PAYBACK ATUAL
        $payback_pagina = $this->payBackAtualAPI($request);

        //PAYBACK JANEIRO
        $payback_janeiro = $this->payBackJaneiroAPI($request);

         //PAYBACK FEVEREIRO
         $payback_fevereiro = $this->payBackFevereiroAPI($request);

         //PAYBACK MARÇO
         $payback_marco = $this->payBackMarcoAPI($request);

          //PAYBACK ABRIL
          $payback_abril = $this->payBackAbrilAPI($request);

            //PAYBACK MAIO
            $payback_maio = $this->payBackMaioAPI($request);

                //PAYBACK JUNHO
                $payback_junho = $this->payBackJunhoAPI($request);

                  //PAYBACK JULHO
                  $payback_julho = $this->payBackJulhoAPI($request);

                    //PAYBACK AGOSTO
                    $payback_agosto = $this->payBackAgostoAPI($request);

                    //PAYBACK SETEMBRO
                    $payback_setembro = $this->payBackSetembroAPI($request);

                      //PAYBACK OUTUBRO
                      $payback_outubro = $this->payBackOutubroAPI($request);

                        //PAYBACK NOVEMBRO
                        $payback_novembro = $this->payBackNovembroAPI($request);

                             //PAYBACK DEZEMBRO
                             $payback_dezembro = $this->payBackDezembroAPI($request);


                             if ($cac_pagina == 0 && $ticket_medio_pagina == 0)
                             {
                                 $valida_payback = "Sem dados para avaliar o PayBack.";
                             }
                             else if ($payback_pagina > 16 && $payback_pagina <= 24)
                             {
                                 $valida_payback = "O cliente de sua empresa está levando um tempo muito alto para dar retorno. Sua empresa passa muito tempo sem fluxo de caixa, ações urgentes são recomendadas.";
                             }
                             else if ($payback_pagina > 12 && $payback_pagina <= 16)
                             {
                                 $valida_payback = "O cliente de sua empresa dará retorno após um período de tempo muito elevado, é necessário reavaliar as estratégias atuais.";
                             }
                             else if ($payback_pagina > 7 && $payback_pagina <= 12)
                             {
                                 $valida_payback = "O cliente de sua empresa está dando retorno no período ideal, continuem assim.";
                             }
                             else if ($payback_pagina > 5 && $payback_pagina <= 7)
                             {
                                 $valida_payback = "O cliente de sua empresa está dando o retorno em um período abaixo da média, vocês estão no caminho certo.";
                             }
                             else if ($payback_pagina <= 5)
                             {
                                 $valida_payback = "O cliente da sua empresa está dando retorno em um período mínimo, excelente!";
                             }
                             else
                             {
                                 $valida_payback = "Sem dados para avaliar o PayBack.";
                             }




                             return response()->json([
                                'payback_pagina' => ($payback_pagina),
                                'valida_payback' => $valida_payback,
                                'payback_janeiro' => ($payback_pagina),
                                'payback_fevereiro' => ($payback_fevereiro),
                                'payback_marco' => ($payback_marco),
                                'payback_abril' => ($payback_abril),
                                'payback_maio' => ($payback_maio),
                                'payback_junho' => ($payback_junho),
                                'payback_julho' => ($payback_julho),
                                'payback_agosto' => ($payback_agosto),
                                'payback_setembro' => ($payback_setembro),
                                'payback_outubro' => ($payback_outubro),
                                'payback_novembro' => ($payback_novembro),
                                'payback_dezembro' => ($payback_dezembro)
                            ]);


    }



    public function payBackPesquisaPeriodo(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

       //DADOS DO CAC NO PERIODO DE JANEIRO
       $cac = app('App\Http\Controllers\CACMobileController')->cacPesquisaPeriodo($request);

       //TICKET MEDIO NO PERIODO DE JANEIRO
       $ticket_medio =  app('App\Http\Controllers\TicketMedioMobileController')->ticketMedioPesquisaPeriodo($request);
        

        //PAYBACK JANEIRO
        if ($ticket_medio != 0)
        {
            $payback_pagina = $cac / $ticket_medio;
        }
        else
        {
            $payback_pagina = 0;
        }


    return $payback_pagina;
    
    }




    
    public function payBackPesquisaPeriodoJaneiro(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

       //DADOS DO CAC NO PERIODO DE JANEIRO
       $cac_janeiro = app('App\Http\Controllers\CACMobileController')->cacPesquisaPeriodoJaneiro($request);

       //TICKET MEDIO NO PERIODO DE JANEIRO
       $ticket_medio_janeiro =  app('App\Http\Controllers\TicketMedioMobileController')->ticketMedioPesquisaPeriodoJaneiro($request);
        

        //PAYBACK JANEIRO
        if ($ticket_medio_janeiro != 0)
        {
            $payback_janeiro = $cac_janeiro / $ticket_medio_janeiro;
        }
        else
        {
            $payback_janeiro = 0;
        }


    return $payback_janeiro;
    
    }





    public function payBackPesquisaPeriodoFevereiro(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

       //DADOS DO CAC NO PERIODO DE FEVEREIRO
       $cac_fevereiro = app('App\Http\Controllers\CACMobileController')->cacPesquisaPeriodoFevereiro($request);

       //TICKET MEDIO NO PERIODO DE JANEIRO
       $ticket_medio_fevereiro =  app('App\Http\Controllers\TicketMedioMobileController')->ticketMedioPesquisaPeriodoFevereiro($request);
        

        //PAYBACK fevereiro
        if ($ticket_medio_fevereiro != 0)
        {
            $payback_fevereiro = $cac_fevereiro / $ticket_medio_fevereiro;
        }
        else
        {
            $payback_fevereiro = 0;
        }


    return $payback_fevereiro;
    
    }






    public function payBackPesquisaPeriodoMarco(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

       //DADOS DO CAC NO PERIODO DE FEVEREIRO
       $cac_marco = app('App\Http\Controllers\CACMobileController')->cacPesquisaPeriodoMarco($request);

       //TICKET MEDIO NO PERIODO DE JANEIRO
       $ticket_medio_marco =  app('App\Http\Controllers\TicketMedioMobileController')->ticketMedioPesquisaPeriodoMarco($request);
        

        //PAYBACK fevereiro
        if ($ticket_medio_marco != 0)
        {
            $payback_marco = $cac_marco / $ticket_medio_marco;
        }
        else
        {
            $payback_marco = 0;
        }


    return $payback_marco;
    
    }



    public function payBackPesquisaPeriodoAbril(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

       //DADOS DO CAC NO PERIODO DE FEVEREIRO
       $cac_abril = app('App\Http\Controllers\CACMobileController')->cacPesquisaPeriodoAbril($request);

       //TICKET MEDIO NO PERIODO DE JANEIRO
       $ticket_medio_abril =  app('App\Http\Controllers\TicketMedioMobileController')->ticketMedioPesquisaPeriodoAbril($request);
        

        //PAYBACK fevereiro
        if ($ticket_medio_abril != 0)
        {
            $payback_abril = $cac_abril / $ticket_medio_abril;
        }
        else
        {
            $payback_abril = 0;
        }


    return $payback_abril;
    
    }






    
    public function payBackPesquisaPeriodoMaio(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

       //DADOS DO CAC NO PERIODO DE FEVEREIRO
       $cac_maio = app('App\Http\Controllers\CACMobileController')->cacPesquisaPeriodoMaio($request);

       //TICKET MEDIO NO PERIODO DE JANEIRO
       $ticket_medio_maio =  app('App\Http\Controllers\TicketMedioMobileController')->ticketMedioPesquisaPeriodoMaio($request);
        

        //PAYBACK fevereiro
        if ($ticket_medio_maio != 0)
        {
            $payback_maio = $cac_maio / $ticket_medio_maio;
        }
        else
        {
            $payback_maio = 0;
        }


    return $payback_maio;
    
    }







    public function payBackPesquisaPeriodoJunho(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

       //DADOS DO CAC NO PERIODO DE FEVEREIRO
       $cac_junho = app('App\Http\Controllers\CACMobileController')->cacPesquisaPeriodoJunho($request);

       //TICKET MEDIO NO PERIODO DE JANEIRO
       $ticket_medio_junho =  app('App\Http\Controllers\TicketMedioMobileController')->ticketMedioPesquisaPeriodoJunho($request);
        

        //PAYBACK fevereiro
        if ($ticket_medio_junho != 0)
        {
            $payback_junho = $cac_junho / $ticket_medio_junho;
        }
        else
        {
            $payback_junho = 0;
        }


    return $payback_junho;
    
    }






    public function payBackPesquisaPeriodoJulho(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

       //DADOS DO CAC NO PERIODO DE FEVEREIRO
       $cac_julho = app('App\Http\Controllers\CACMobileController')->cacPesquisaPeriodoJulho($request);

       //TICKET MEDIO NO PERIODO DE JANEIRO
       $ticket_medio_julho =  app('App\Http\Controllers\TicketMedioMobileController')->ticketMedioPesquisaPeriodoJulho($request);
        

        //PAYBACK fevereiro
        if ($ticket_medio_julho != 0)
        {
            $payback_julho = $cac_julho / $ticket_medio_julho;
        }
        else
        {
            $payback_julho = 0;
        }


    return $payback_julho;
    
    }





    public function payBackPesquisaPeriodoAgosto(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

       //DADOS DO CAC NO PERIODO DE FEVEREIRO
       $cac_agosto = app('App\Http\Controllers\CACMobileController')->cacPesquisaPeriodoAgosto($request);

       //TICKET MEDIO NO PERIODO DE JANEIRO
       $ticket_medio_agosto =  app('App\Http\Controllers\TicketMedioMobileController')->ticketMedioPesquisaPeriodoAgosto($request);
        

        //PAYBACK fevereiro
        if ($ticket_medio_agosto != 0)
        {
            $payback_agosto = $cac_agosto / $ticket_medio_agosto;
        }
        else
        {
            $payback_agosto = 0;
        }


    return $payback_agosto;
    
    }






    public function payBackPesquisaPeriodoSetembro(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

       //DADOS DO CAC NO PERIODO DE FEVEREIRO
       $cac_setembro = app('App\Http\Controllers\CACMobileController')->cacPesquisaPeriodoSetembro($request);

       //TICKET MEDIO NO PERIODO DE JANEIRO
       $ticket_medio_setembro =  app('App\Http\Controllers\TicketMedioMobileController')->ticketMedioPesquisaPeriodoSetembro($request);
        

        //PAYBACK fevereiro
        if ($ticket_medio_setembro != 0)
        {
            $payback_setembro = $cac_setembro / $ticket_medio_setembro;
        }
        else
        {
            $payback_setembro = 0;
        }


    return $payback_setembro;
    
    }







    public function payBackPesquisaPeriodoOutubro(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

       //DADOS DO CAC NO PERIODO DE FEVEREIRO
       $cac_outubro = app('App\Http\Controllers\CACMobileController')->cacPesquisaPeriodoOutubro($request);

       //TICKET MEDIO NO PERIODO DE JANEIRO
       $ticket_medio_outubro =  app('App\Http\Controllers\TicketMedioMobileController')->ticketMedioPesquisaPeriodoOutubro($request);
        

        //PAYBACK fevereiro
        if ($ticket_medio_outubro != 0)
        {
            $payback_outubro = $cac_outubro / $ticket_medio_outubro;
        }
        else
        {
            $payback_outubro = 0;
        }


    return $payback_outubro;
    
    }







    public function payBackPesquisaPeriodoNovembro(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

       //DADOS DO CAC NO PERIODO DE FEVEREIRO
       $cac_novembro = app('App\Http\Controllers\CACMobileController')->cacPesquisaPeriodoNovembro($request);

       //TICKET MEDIO NO PERIODO DE JANEIRO
       $ticket_medio_novembro =  app('App\Http\Controllers\TicketMedioMobileController')->ticketMedioPesquisaPeriodoNovembro($request);
        

        //PAYBACK fevereiro
        if ($ticket_medio_novembro != 0)
        {
            $payback_novembro = $cac_novembro / $ticket_medio_novembro;
        }
        else
        {
            $payback_novembro = 0;
        }


    return $payback_novembro;
    
    }







    public function payBackPesquisaPeriodoDezembro(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

       //DADOS DO CAC NO PERIODO DE FEVEREIRO
       $cac_dezembro = app('App\Http\Controllers\CACMobileController')->cacPesquisaPeriodoNovembro($request);

       //TICKET MEDIO NO PERIODO DE JANEIRO
       $ticket_medio_dezembro =  app('App\Http\Controllers\TicketMedioMobileController')->ticketMedioPesquisaPeriodoNovembro($request);
        

        //PAYBACK fevereiro
        if ($ticket_medio_dezembro != 0)
        {
            $payback_dezembro = $cac_dezembro / $ticket_medio_dezembro;
        }
        else
        {
            $payback_dezembro = 0;
        }


    return $payback_dezembro;
    
    }








    public function pesquisaPayback(Request $request)
    {

     

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $nomeEmpresa = Empresa::where('usuario_id', $usuarioAutenticadoId)->select('razao_social')->value('razao_social');

        //DADOS DO CAC NO PERIODO
        $cac_comparacao = app('App\Http\Controllers\CACMobileController')->cacPesquisaPeriodo($request);

        //TICKET MEDIO NO PERIODO
        $ticket_medio_comparacao = app('App\Http\Controllers\TicketMedioMobileController')->ticketMedioPesquisaPeriodo($request);
        
        
          //TICKET MEDIO NO PERIODO
          $payback_pagina = $this->payBackPesquisaPeriodo($request);

          $payback_janeiro = $this->payBackPesquisaPeriodoJaneiro($request);

          $payback_fevereiro = $this->payBackPesquisaPeriodoFevereiro($request);

          $payback_marco = $this->payBackPesquisaPeriodoMarco($request);

          $payback_abril = $this->payBackPesquisaPeriodoAbril($request);

          $payback_maio = $this->payBackPesquisaPeriodoMaio($request);

          $payback_junho = $this->payBackPesquisaPeriodoJunho($request);

          $payback_julho = $this->payBackPesquisaPeriodoJulho($request);

          $payback_agosto = $this->payBackPesquisaPeriodoAgosto($request);

          $payback_setembro = $this->payBackPesquisaPeriodoSetembro($request);

          $payback_outubro = $this->payBackPesquisaPeriodoOutubro($request);

          $payback_novembro = $this->payBackPesquisaPeriodoNovembro($request);

          $payback_dezembro = $this->payBackPesquisaPeriodoDezembro($request);


        if ($cac_comparacao == 0 && $ticket_medio_comparacao == 0)
        {
            $valida_payback = "Sem dados para avaliar o PayBack.";
        }
        else if ($payback_pagina > 16 && $payback_pagina <= 24)
        {
            $valida_payback = "O cliente de sua empresa está levando um tempo muito alto para dar retorno. Sua empresa passa muito tempo sem fluxo de caixa, ações urgentes são recomendadas.";
        }
        else if ($payback_pagina > 12 && $payback_pagina <= 16)
        {
            $valida_payback = "O cliente de sua empresa dará retorno após um período de tempo muito elevado, é necessário reavaliar as estratégias atuais.";
        }
        else if ($payback_pagina > 7 && $payback_pagina <= 12)
        {
            $valida_payback = "O cliente de sua empresa está dando retorno no período ideal, continuem assim.";
        }
        else if ($payback_pagina > 5 && $payback_pagina <= 7)
        {
            $valida_payback = "O cliente de sua empresa está dando o retorno em um período abaixo da média, vocês estão no caminho certo.";
        }
        else if ($payback_pagina <= 5)
        {
            $valida_payback = "O cliente da sua empresa está dando retorno em um período mínimo, excelente!";
        }
        else
        {
            $valida_payback = "Sem dados para avaliar o PayBack.";
        }

        
        return response()->json([
            'payback_pagina' => ($payback_pagina),
            'valida_payback' => $valida_payback,
            'payback_janeiro' => $payback_janeiro,
            'payback_fevereiro' => $payback_fevereiro,
            'payback_marco' => $payback_marco,
            'payback_abril' => $payback_abril,
            'payback_maio' => $payback_maio,
            'payback_junho' => $payback_junho,
            'payback_julho' => $payback_julho,
            'payback_agosto' => $payback_agosto,
            'payback_setembro' => $payback_setembro,
            'payback_outubro' => $payback_outubro,
            'payback_novembro' => $payback_novembro,
            'payback_dezembro' => $payback_dezembro,


        ]);






    }







}