<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuario;
use App\Cargo;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\Routing\ResponseFactory;
use Auth;
use Session;
use Carbon\Carbon;

class ROIMobileController extends Controller
{

    public function roiAtualAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

    $faturamento_atual = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', $data_atual->month)
        ->where('faturamentos.ano_id', '=', $data_atual->year)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');

    $total_investimento_atual = DB::table('total_investimentos')->join('periodos', 'periodos.id', '=', 'total_investimentos.periodo_id')
        ->where('total_investimentos.periodo_id', '=', $data_atual->month)
        ->where('total_investimentos.ano_id', '=', $data_atual->year)
        ->where('total_investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(total_investimentos.valor) as valor'))
        ->value('valor');

    $investimento_atual = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', $data_atual->month)
        ->where('investimentos.ano_id', '=', $data_atual->year)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

    $despesa_atual = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
        ->where('despesas.periodo_id', '=', $data_atual->month)
        ->where('despesas.ano_id', '=', $data_atual->year)
        ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(despesas.valor) as valor'))
        ->value('valor');;

    if (($investimento_atual + $total_investimento_atual + $despesa_atual) != 0)
    {
        $roi_pagina = number_format(($faturamento_atual - ($investimento_atual + $total_investimento_atual + $despesa_atual)) / ($investimento_atual + $total_investimento_atual + $despesa_atual) , 2);
        //ROI = (faturamento - (ações de marketing+investimentos+despesas com vendas)) / (ações de marketing+investimento+despesas com vendas)
        
    }
    else
    {
        $roi_pagina = 0;
    }

    return response()->json([
        'roi_pagina' => number_format($roi_pagina,2)
    ]);


    }




    public function roiPaginaAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

           

        $faturamento_atual = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', $data_atual->month)
        ->where('faturamentos.ano_id', '=', $data_atual->year)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');

    $total_investimento_atual = DB::table('total_investimentos')->join('periodos', 'periodos.id', '=', 'total_investimentos.periodo_id')
        ->where('total_investimentos.periodo_id', '=', $data_atual->month)
        ->where('total_investimentos.ano_id', '=', $data_atual->year)
        ->where('total_investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(total_investimentos.valor) as valor'))
        ->value('valor');

    $investimento_atual = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', $data_atual->month)
        ->where('investimentos.ano_id', '=', $data_atual->year)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

    $despesa_atual = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
        ->where('despesas.periodo_id', '=', $data_atual->month)
        ->where('despesas.ano_id', '=', $data_atual->year)
        ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(despesas.valor) as valor'))
        ->value('valor');;

    if (($investimento_atual + $total_investimento_atual + $despesa_atual) != 0)
    {
        $roi_pagina = number_format(($faturamento_atual - ($investimento_atual + $total_investimento_atual + $despesa_atual)) / ($investimento_atual + $total_investimento_atual + $despesa_atual) , 2);
        //ROI = (faturamento - (ações de marketing+investimentos+despesas com vendas)) / (ações de marketing+investimento+despesas com vendas)
        
    }
    else
    {
        $roi_pagina = 0;
    }

    return $roi_pagina;


    }



    public function roiDezembroAnoPassadoAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        



        $faturamento_dezembro_ano_passado = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
            ->where('faturamentos.periodo_id', '=', 12)
            ->where('faturamentos.ano_id', '=', $data_atual->year - 1)
            ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
            ->value('valor_faturamento');

        $total_investimento_dezembro_ano_passado = DB::table('total_investimentos')->join('periodos', 'periodos.id', '=', 'total_investimentos.periodo_id')
            ->where('total_investimentos.periodo_id', '=', 12)
            ->where('total_investimentos.ano_id', '=', $data_atual->year - 1)
            ->where('total_investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(total_investimentos.valor) as valor'))
            ->value('valor');

        $investimento_dezembro_ano_passado = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
            ->where('investimentos.periodo_id', '=', 12)
            ->where('investimentos.ano_id', '=', $data_atual->year - 1)
            ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(investimentos.valor) as valor'))
            ->value('valor');

        $despesa_dezembro_ano_passado = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
            ->where('despesas.periodo_id', '=', 12)
            ->where('despesas.ano_id', '=', $data_atual->year - 1)
            ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(despesas.valor) as valor'))
            ->value('valor');;

        if (($investimento_dezembro_ano_passado + $total_investimento_dezembro_ano_passado + $despesa_dezembro_ano_passado) != 0)
        {
            $roi_dezembro_ano_passado = ($faturamento_dezembro_ano_passado - ($investimento_dezembro_ano_passado + $total_investimento_dezembro_ano_passado + $despesa_dezembro_ano_passado)) / ($investimento_dezembro_ano_passado + $total_investimento_dezembro_ano_passado + $despesa_dezembro_ano_passado);
            //ROI = (faturamento - (ações de marketing+investimentos+despesas com vendas)) / (ações de marketing+investimento+despesas com vendas)
            
        }
        else
        {
            $roi_dezembro_ano_passado = 0;
        }

        return $roi_dezembro_ano_passado;    





    }







    public function roiJaneiroAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');




        $faturamento_janeiro = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
            ->where('faturamentos.periodo_id', '=', 1)
            ->where('faturamentos.ano_id', '=', $data_atual->year)
            ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
            ->value('valor_faturamento');

        $total_investimento_janeiro = DB::table('total_investimentos')->join('periodos', 'periodos.id', '=', 'total_investimentos.periodo_id')
            ->where('total_investimentos.periodo_id', '=', 1)
            ->where('total_investimentos.ano_id', '=', $data_atual->year)
            ->where('total_investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(total_investimentos.valor) as valor'))
            ->value('valor');

        $investimento_janeiro = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
            ->where('investimentos.periodo_id', '=', 1)
            ->where('investimentos.ano_id', '=', $data_atual->year)
            ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(investimentos.valor) as valor'))
            ->value('valor');

        $despesa_janeiro = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
            ->where('despesas.periodo_id', '=', 1)
            ->where('despesas.ano_id', '=', $data_atual->year)
            ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(despesas.valor) as valor'))
            ->value('valor');;

        if (($investimento_janeiro + $total_investimento_janeiro + $despesa_janeiro) != 0)
        {
            $roi_janeiro = ($faturamento_janeiro - ($investimento_janeiro + $total_investimento_janeiro + $despesa_janeiro)) / ($investimento_janeiro + $total_investimento_janeiro + $despesa_janeiro);
            //ROI = (faturamento - (ações de marketing+investimentos+despesas com vendas)) / (ações de marketing+investimento+despesas com vendas)
            
        }
        else
        {
            $roi_janeiro = 0;
        }

        return $roi_janeiro;    





    }







    public function roiFevereiroAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        

        $faturamento_fevereiro = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', 2)
        ->where('faturamentos.ano_id', '=', $data_atual->year)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');

    $total_investimento_fevereiro = DB::table('total_investimentos')->join('periodos', 'periodos.id', '=', 'total_investimentos.periodo_id')
        ->where('total_investimentos.periodo_id', '=', 2)
        ->where('total_investimentos.ano_id', '=', $data_atual->year)
        ->where('total_investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(total_investimentos.valor) as valor'))
        ->value('valor');

    $investimento_fevereiro = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 2)
        ->where('investimentos.ano_id', '=', $data_atual->year)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

    $despesa_fevereiro = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
        ->where('despesas.periodo_id', '=', 2)
        ->where('despesas.ano_id', '=', $data_atual->year)
        ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(despesas.valor) as valor'))
        ->value('valor');;

    if (($investimento_fevereiro + $total_investimento_fevereiro + $despesa_fevereiro) != 0)
    {
        $roi_fevereiro = ($faturamento_fevereiro - ($investimento_fevereiro + $total_investimento_fevereiro + $despesa_fevereiro)) / ($investimento_fevereiro + $total_investimento_fevereiro + $despesa_fevereiro);
        //ROI = (faturamento - (ações de marketing+investimentos+despesas com vendas)) / (ações de marketing+investimento+despesas com vendas)
        
    }
    else
    {
        $roi_fevereiro = 0;
    }

    return $roi_fevereiro;  





    }





    public function roiMarcoAPI(Request $request){


        
        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

    

        $faturamento_marco = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', 3)
        ->where('faturamentos.ano_id', '=', $data_atual->year)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');

    $total_investimento_marco = DB::table('total_investimentos')->join('periodos', 'periodos.id', '=', 'total_investimentos.periodo_id')
        ->where('total_investimentos.periodo_id', '=', 3)
        ->where('total_investimentos.ano_id', '=', $data_atual->year)
        ->where('total_investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(total_investimentos.valor) as valor'))
        ->value('valor');

    $investimento_marco = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 3)
        ->where('investimentos.ano_id', '=', $data_atual->year)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

    $despesa_marco = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
        ->where('despesas.periodo_id', '=', 3)
        ->where('despesas.ano_id', '=', $data_atual->year)
        ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(despesas.valor) as valor'))
        ->value('valor');;

    if (($investimento_marco + $total_investimento_marco + $despesa_marco) != 0)
    {
        $roi_marco = ($faturamento_marco - ($investimento_marco + $total_investimento_marco + $despesa_marco)) / ($investimento_marco + $total_investimento_marco + $despesa_marco);
        //ROI = (faturamento - (ações de marketing+investimentos+despesas com vendas)) / (ações de marketing+investimento+despesas com vendas)
        
    }
    else
    {
        $roi_marco = 0;
    }

    return $roi_marco;



    }







    public function roiAbrilAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        

        $faturamento_abril = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', 4)
        ->where('faturamentos.ano_id', '=', $data_atual->year)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');

    $total_investimento_abril = DB::table('total_investimentos')->join('periodos', 'periodos.id', '=', 'total_investimentos.periodo_id')
        ->where('total_investimentos.periodo_id', '=', 4)
        ->where('total_investimentos.ano_id', '=', $data_atual->year)
        ->where('total_investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(total_investimentos.valor) as valor'))
        ->value('valor');

    $investimento_abril = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 4)
        ->where('investimentos.ano_id', '=', $data_atual->year)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

    $despesa_abril = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
        ->where('despesas.periodo_id', '=', 4)
        ->where('despesas.ano_id', '=', $data_atual->year)
        ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(despesas.valor) as valor'))
        ->value('valor');;

    if (($investimento_abril + $total_investimento_abril + $despesa_abril) != 0)
    {
        $roi_abril = ($faturamento_abril - ($investimento_abril + $total_investimento_abril + $despesa_abril)) / ($investimento_abril + $total_investimento_abril + $despesa_abril);
        //ROI = (faturamento - (ações de marketing+investimentos+despesas com vendas)) / (ações de marketing+investimento+despesas com vendas)
        
    }
    else
    {
        $roi_abril = 0;
    }

    return $roi_abril;

    }




    public function roiMaioAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

       

        $faturamento_maio = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', 5)
        ->where('faturamentos.ano_id', '=', $data_atual->year)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');

    $total_investimento_maio = DB::table('total_investimentos')->join('periodos', 'periodos.id', '=', 'total_investimentos.periodo_id')
        ->where('total_investimentos.periodo_id', '=', 5)
        ->where('total_investimentos.ano_id', '=', $data_atual->year)
        ->where('total_investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(total_investimentos.valor) as valor'))
        ->value('valor');

    $investimento_maio = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 5)
        ->where('investimentos.ano_id', '=', $data_atual->year)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

    $despesa_maio = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
        ->where('despesas.periodo_id', '=', 5)
        ->where('despesas.ano_id', '=', $data_atual->year)
        ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(despesas.valor) as valor'))
        ->value('valor');;

    if (($investimento_maio + $total_investimento_maio + $despesa_maio) != 0)
    {
        $roi_maio = ($faturamento_maio - ($investimento_maio + $total_investimento_maio + $despesa_maio)) / ($investimento_maio + $total_investimento_maio + $despesa_maio);
        //ROI = (faturamento - (ações de marketing+investimentos+despesas com vendas)) / (ações de marketing+investimento+despesas com vendas)
        
    }
    else
    {
        $roi_maio = 0;
    }

    return $roi_maio;

    }




    public function roiJunhoAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        
 

        $faturamento_junho = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', 6)
        ->where('faturamentos.ano_id', '=', $data_atual->year)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');

    $total_investimento_junho = DB::table('total_investimentos')->join('periodos', 'periodos.id', '=', 'total_investimentos.periodo_id')
        ->where('total_investimentos.periodo_id', '=', 6)
        ->where('total_investimentos.ano_id', '=', $data_atual->year)
        ->where('total_investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(total_investimentos.valor) as valor'))
        ->value('valor');

    $investimento_junho = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 6)
        ->where('investimentos.ano_id', '=', $data_atual->year)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

    $despesa_junho = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
        ->where('despesas.periodo_id', '=', 6)
        ->where('despesas.ano_id', '=', $data_atual->year)
        ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(despesas.valor) as valor'))
        ->value('valor');;

    if (($investimento_junho + $total_investimento_junho + $despesa_junho) != 0)
    {
        $roi_junho = ($faturamento_junho - ($investimento_junho + $total_investimento_junho + $despesa_junho)) / ($investimento_junho + $total_investimento_junho + $despesa_junho);
        //ROI = (faturamento - (ações de marketing+investimentos+despesas com vendas)) / (ações de marketing+investimento+despesas com vendas)
        
    }
    else
    {
        $roi_junho = 0;
    }

    return $roi_junho;  

    }




    public function roiJulhoAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        

        $faturamento_julho = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', 7)
        ->where('faturamentos.ano_id', '=', $data_atual->year)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');

    $total_investimento_julho = DB::table('total_investimentos')->join('periodos', 'periodos.id', '=', 'total_investimentos.periodo_id')
        ->where('total_investimentos.periodo_id', '=', 7)
        ->where('total_investimentos.ano_id', '=', $data_atual->year)
        ->where('total_investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(total_investimentos.valor) as valor'))
        ->value('valor');

    $investimento_julho = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 7)
        ->where('investimentos.ano_id', '=', $data_atual->year)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

    $despesa_julho = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
        ->where('despesas.periodo_id', '=', 7)
        ->where('despesas.ano_id', '=', $data_atual->year)
        ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(despesas.valor) as valor'))
        ->value('valor');;

    if (($investimento_julho + $total_investimento_julho + $despesa_julho) != 0)
    {
        $roi_julho = ($faturamento_julho - ($investimento_julho + $total_investimento_julho + $despesa_julho)) / ($investimento_julho + $total_investimento_julho + $despesa_julho);
        //ROI = (faturamento - (ações de marketing+investimentos+despesas com vendas)) / (ações de marketing+investimento+despesas com vendas)
        
    }
    else
    {
        $roi_julho = 0;
    }

    }





    public function roiAgostoAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

 

        $faturamento_agosto = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', 8)
        ->where('faturamentos.ano_id', '=', $data_atual->year)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');

    $total_investimento_agosto = DB::table('total_investimentos')->join('periodos', 'periodos.id', '=', 'total_investimentos.periodo_id')
        ->where('total_investimentos.periodo_id', '=', 8)
        ->where('total_investimentos.ano_id', '=', $data_atual->year)
        ->where('total_investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(total_investimentos.valor) as valor'))
        ->value('valor');

    $investimento_agosto = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 8)
        ->where('investimentos.ano_id', '=', $data_atual->year)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

    $despesa_agosto = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
        ->where('despesas.periodo_id', '=', 8)
        ->where('despesas.ano_id', '=', $data_atual->year)
        ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(despesas.valor) as valor'))
        ->value('valor');;

    if (($investimento_agosto + $total_investimento_agosto + $despesa_agosto) != 0)
    {
        $roi_agosto = ($faturamento_agosto - ($investimento_agosto + $total_investimento_agosto + $despesa_agosto)) / ($investimento_agosto + $total_investimento_agosto + $despesa_agosto);
        //ROI = (faturamento - (ações de marketing+investimentos+despesas com vendas)) / (ações de marketing+investimento+despesas com vendas)
        
    }
    else
    {
        $roi_agosto = 0;
    }

    }




    public function roiSetembroAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        
        

        $faturamento_setembro = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', 9)
        ->where('faturamentos.ano_id', '=', $data_atual->year)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');

    $total_investimento_setembro = DB::table('total_investimentos')->join('periodos', 'periodos.id', '=', 'total_investimentos.periodo_id')
        ->where('total_investimentos.periodo_id', '=', 9)
        ->where('total_investimentos.ano_id', '=', $data_atual->year)
        ->where('total_investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(total_investimentos.valor) as valor'))
        ->value('valor');

    $investimento_setembro = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 9)
        ->where('investimentos.ano_id', '=', $data_atual->year)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

    $despesa_setembro = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
        ->where('despesas.periodo_id', '=', 9)
        ->where('despesas.ano_id', '=', $data_atual->year)
        ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(despesas.valor) as valor'))
        ->value('valor');;

    if (($investimento_setembro + $total_investimento_setembro + $despesa_setembro) != 0)
    {
        $roi_setembro = ($faturamento_setembro - ($investimento_setembro + $total_investimento_setembro + $despesa_setembro)) / ($investimento_setembro + $total_investimento_setembro + $despesa_setembro);
        //ROI = (faturamento - (ações de marketing+investimentos+despesas com vendas)) / (ações de marketing+investimento+despesas com vendas)
        
    }
    else
    {
        $roi_setembro = 0;
    }

    }




    public function roiOutubroAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

       

        $faturamento_outubro = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', 10)
        ->where('faturamentos.ano_id', '=', $data_atual->year)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');

    $total_investimento_outubro = DB::table('total_investimentos')->join('periodos', 'periodos.id', '=', 'total_investimentos.periodo_id')
        ->where('total_investimentos.periodo_id', '=', 10)
        ->where('total_investimentos.ano_id', '=', $data_atual->year)
        ->where('total_investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(total_investimentos.valor) as valor'))
        ->value('valor');

    $investimento_outubro = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 10)
        ->where('investimentos.ano_id', '=', $data_atual->year)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

    $despesa_outubro = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
        ->where('despesas.periodo_id', '=', 10)
        ->where('despesas.ano_id', '=', $data_atual->year)
        ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(despesas.valor) as valor'))
        ->value('valor');;

    if (($investimento_outubro + $total_investimento_outubro + $despesa_outubro) != 0)
    {
        $roi_outubro = ($faturamento_outubro - ($investimento_outubro + $total_investimento_outubro + $despesa_outubro)) / ($investimento_outubro + $total_investimento_outubro + $despesa_outubro);
        //ROI = (faturamento - (ações de marketing+investimentos+despesas com vendas)) / (ações de marketing+investimento+despesas com vendas)
        
    }
    else
    {
        $roi_outubro = 0;
    }

    }





    public function roiNovembroAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

         

        $faturamento_novembro = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', 11)
        ->where('faturamentos.ano_id', '=', $data_atual->year)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');

    $total_investimento_novembro = DB::table('total_investimentos')->join('periodos', 'periodos.id', '=', 'total_investimentos.periodo_id')
        ->where('total_investimentos.periodo_id', '=', 11)
        ->where('total_investimentos.ano_id', '=', $data_atual->year)
        ->where('total_investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(total_investimentos.valor) as valor'))
        ->value('valor');

    $investimento_novembro = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 11)
        ->where('investimentos.ano_id', '=', $data_atual->year)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

    $despesa_novembro = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
        ->where('despesas.periodo_id', '=', 11)
        ->where('despesas.ano_id', '=', $data_atual->year)
        ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(despesas.valor) as valor'))
        ->value('valor');;

    if (($investimento_novembro + $total_investimento_novembro + $despesa_novembro) != 0)
    {
        $roi_novembro = ($faturamento_novembro - ($investimento_novembro + $total_investimento_novembro + $despesa_novembro)) / ($investimento_novembro + $total_investimento_novembro + $despesa_novembro);
        //ROI = (faturamento - (ações de marketing+investimentos+despesas com vendas)) / (ações de marketing+investimento+despesas com vendas)
        
    }
    else
    {
        $roi_novembro = 0;
    }

    }





    public function roiDezembroAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        

        $faturamento_dezembro = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', 12)
        ->where('faturamentos.ano_id', '=', $data_atual->year)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');

    $total_investimento_dezembro = DB::table('total_investimentos')->join('periodos', 'periodos.id', '=', 'total_investimentos.periodo_id')
        ->where('total_investimentos.periodo_id', '=', 12)
        ->where('total_investimentos.ano_id', '=', $data_atual->year)
        ->where('total_investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(total_investimentos.valor) as valor'))
        ->value('valor');

    $investimento_dezembro = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 12)
        ->where('investimentos.ano_id', '=', $data_atual->year)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

    $despesa_dezembro = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
        ->where('despesas.periodo_id', '=', 12)
        ->where('despesas.ano_id', '=', $data_atual->year)
        ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(despesas.valor) as valor'))
        ->value('valor');;

    if (($investimento_dezembro + $total_investimento_dezembro + $despesa_dezembro) != 0)
    {
        $roi_dezembro = ($faturamento_dezembro - ($investimento_dezembro + $total_investimento_dezembro + $despesa_dezembro)) / ($investimento_dezembro + $total_investimento_dezembro + $despesa_dezembro);
        //ROI = (faturamento - (ações de marketing+investimentos+despesas com vendas)) / (ações de marketing+investimento+despesas com vendas)
        
    }
    else
    {
        $roi_dezembro = 0;
    }

    }








    public function paginaRoiAPI(Request $request)
    {

        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $data_atual = Carbon::now();

      
        //ROI ATUAL
        $roi_pagina = $this->roiPaginaAPI($request);
        
        //ROI DEZEMBRO DO ANO PASSADO
        $roi_dezembro_ano_passado = $this->roiDezembroAnoPassadoAPI($request);

        //ROI JANEIRO
        $roi_janeiro = $this->roiJaneiroAPI($request);

        //ROI FEVEREIRO
        $roi_fevereiro = $this->roiFevereiroAPI($request);

        //ROI MARÇO
        $roi_marco = $this->roiMarcoAPI($request);

        //ROI ABRIL
        $roi_abril = $this->roiAbrilAPI($request);

        //ROI MAIO
        $roi_maio = $this->roiMaioAPI($request);

        //ROI JUNHO
        $roi_junho = $this->roiJunhoAPI($request);

        //ROI JULHO
        $roi_julho = $this->roiJulhoAPI($request);

        //ROI AGOSTO
        $roi_agosto = $this->roiAgostoAPI($request);

        //ROI SETEMBRO
       $roi_setembro = $this->roiSetembroAPI($request);

        //ROI OUTUBRO
       $roi_outubro = $this->roiOutubroAPI($request);

        //ROI NOVEMBRO
        $roi_novembro = $this->roiNovembroAPI($request);

        //ROI DEZEMBRO
        $roi_dezembro = $this->roiDezembroAPI($request);


        //Comparação de Dados

        //Janeiro/Dezembro
        if ($roi_janeiro == 0 || $roi_dezembro_ano_passado == 0)
        {
            $roi_janeiro_dezembro = 0;
        }
        else
        {
            $roi_janeiro_dezembro = (($roi_janeiro / $roi_dezembro_ano_passado) - 1) * 100;
        }

        //Fevereiro/Janeiro
        if ($roi_fevereiro == 0 || $roi_janeiro == 0)
        {
            $roi_fevereiro_janeiro = 0;
        }
        else
        {
            $roi_fevereiro_janeiro = (($roi_fevereiro / $roi_janeiro) - 1) * 100;
        }

        //Marco/Fevereiro
        if ($roi_marco == 0 || $roi_fevereiro == 0)
        {
            $roi_marco_fevereiro = 0;
        }
        else
        {
            $roi_marco_fevereiro = (($roi_marco / $roi_fevereiro) - 1) * 100;
        }

        //Abril/Marco
        if ($roi_abril == 0 || $roi_marco == 0)
        {
            $roi_abril_marco = 0;
        }
        else
        {
            $roi_abril_marco = (($roi_abril / $roi_marco) - 1) * 100;
        }

        //Maio/Abril
        if ($roi_maio == 0 || $roi_abril == 0)
        {
            $roi_maio_abril = 0;
        }
        else
        {
            $roi_maio_abril = (($roi_maio / $roi_abril) - 1) * 100;
        }

        //Junho/Maio
        if ($roi_junho == 0 || $roi_maio == 0)
        {
            $roi_junho_maio = 0;
        }
        else
        {
            $roi_junho_maio = (($roi_junho / $roi_maio) - 1) * 100;
        }

        //Julho/Junho
        if ($roi_julho == 0 || $roi_junho == 0)
        {
            $roi_julho_junho = 0;
        }
        else
        {
            $roi_julho_junho = (($roi_julho / $roi_junho) - 1) * 100;
        }

        //Agosto/Julho
        if ($roi_agosto == 0 || $roi_julho == 0)
        {
            $roi_agosto_julho = 0;
        }
        else
        {
            $roi_agosto_julho = (($roi_agosto / $roi_julho) - 1) * 100;
        }

        //Setembro/Agosto
        if ($roi_setembro == 0 || $roi_agosto == 0)
        {
            $roi_setembro_agosto = 0;
        }
        else
        {
            $roi_setembro_agosto = (($roi_setembro / $roi_agosto) - 1) * 100;
        }

        //Outubro/Setembro
        if ($roi_outubro == 0 || $roi_setembro == 0)
        {
            $roi_outubro_setembro = 0;
        }
        else
        {
            $roi_outubro_setembro = (($roi_outubro / $roi_setembro) - 1) * 100;
        }

        //Novembro/Outubro
        if ($roi_novembro == 0 || $roi_outubro == 0)
        {
            $roi_novembro_outubro = 0;
        }
        else
        {
            $roi_novembro_outubro = (($roi_novembro / $roi_outubro) - 1) * 100;
        }

        //Dezembro/Novembro
        if ($roi_dezembro == 0 || $roi_novembro == 0)
        {
            $roi_dezembro_novembro = 0;
        }
        else
        {
            $roi_dezembro_novembro = (($roi_dezembro / $roi_novembro) - 1) * 100;
        }

        if ($roi_pagina == 0)
        {
            $valida_roi = "Investimento nulo.";
        }
        else if ($roi_pagina < 0)
        {
            $valida_roi = "Seus investimentos não estão dando retorno.";
        }
        else
        {
            $valida_roi = "Seu retorno de investimento foi de $roi_pagina x";
        }


        return response()->json([
            'roi_pagina' => number_format($roi_pagina,2),
            'valida_roi' => $valida_roi,
            'roi_janeiro'=> ($roi_janeiro),
            'roi_fevereiro' => ($roi_fevereiro),
            'roi_marco' => ($roi_marco),
            'roi_abril' => ($roi_abril),
            'roi_maio' => ($roi_maio),
            'roi_junho' => ($roi_junho),
            'roi_julho' => ($roi_julho),
            'roi_agosto' => ($roi_agosto),
            'roi_setembro' => ($roi_setembro),
            'roi_outubro'  =>($roi_outubro),
            'roi_novembro' => ($roi_novembro),
            'roi_dezembro'  => ($roi_dezembro),
        ]);

       
        
    }







    public function roiPesquisaPeriodo(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');


        $faturamento_atual = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', $periodo_id)->where('faturamentos.ano_id', '=', $ano_id)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)
        ->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');

    $total_investimento_atual = DB::table('total_investimentos')->join('periodos', 'periodos.id', '=', 'total_investimentos.periodo_id')
        ->where('total_investimentos.periodo_id', '=', $periodo_id)->where('total_investimentos.ano_id', '=', $ano_id)
        ->where('total_investimentos.usuario_id', '=', $usuarioAutenticadoId)
        ->select(DB::raw('SUM(total_investimentos.valor) as valor'))
        ->value('valor');

    $investimento_atual = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', $periodo_id)->where('investimentos.ano_id', '=', $ano_id)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)
        ->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

    $despesa_atual = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
        ->where('despesas.periodo_id', '=', $periodo_id)->where('despesas.ano_id', '=', $ano_id)
        ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)
        ->select(DB::raw('SUM(despesas.valor) as valor'))
        ->value('valor');;

    if (($investimento_atual + $total_investimento_atual + $despesa_atual) != 0)
    {
        $roi_pagina = number_format(($faturamento_atual - ($investimento_atual + $total_investimento_atual + $despesa_atual)) / ($investimento_atual + $total_investimento_atual + $despesa_atual) , 2);
        //ROI = (faturamento - (ações de marketing+investimentos+despesas com vendas)) / (ações de marketing+investimento+despesas com vendas)
        
    }
    else    
    {
        $roi_pagina = 0;
    }

    return $roi_pagina;


    }






    public function roiPesquisaPeriodoJaneiro(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');


       //ROI JANEIRO
       $faturamento_janeiro = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
       ->where('faturamentos.periodo_id', '=', 1)
       ->where('faturamentos.ano_id', '=', $ano_id)
       ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)
       ->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
       ->value('valor_faturamento');

   $total_investimento_janeiro = DB::table('total_investimentos')->join('periodos', 'periodos.id', '=', 'total_investimentos.periodo_id')
       ->where('total_investimentos.periodo_id', '=', 1)
       ->where('total_investimentos.ano_id', '=', $ano_id)
       ->where('total_investimentos.usuario_id', '=', $usuarioAutenticadoId)
       ->select(DB::raw('SUM(total_investimentos.valor) as valor'))
       ->value('valor');

   $investimento_janeiro = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
       ->where('investimentos.periodo_id', '=', 1)
       ->where('investimentos.ano_id', '=', $ano_id)
       ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)
       ->select(DB::raw('SUM(investimentos.valor) as valor'))
       ->value('valor');

   $despesa_janeiro = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
       ->where('despesas.periodo_id', '=', 1)
       ->where('despesas.ano_id', '=', $ano_id)
       ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)
       ->select(DB::raw('SUM(despesas.valor) as valor'))
       ->value('valor');;

   if (($investimento_janeiro + $total_investimento_janeiro + $despesa_janeiro) != 0)
   {
       $roi_janeiro = ($faturamento_janeiro - ($investimento_janeiro + $total_investimento_janeiro + $despesa_janeiro)) / ($investimento_janeiro + $total_investimento_janeiro + $despesa_janeiro);
       //ROI = (faturamento - (ações de marketing+investimentos+despesas com vendas)) / (ações de marketing+investimento+despesas com vendas)
       
   }
   else
   {
       $roi_janeiro = 0;
   }

   return $roi_janeiro;


    }





    public function roiPesquisaPeriodoFevereiro(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');


       //ROI fevereiro
       $faturamento_fevereiro = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
       ->where('faturamentos.periodo_id', '=', 2)
       ->where('faturamentos.ano_id', '=', $ano_id)
       ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)
       ->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
       ->value('valor_faturamento');

   $total_investimento_fevereiro = DB::table('total_investimentos')->join('periodos', 'periodos.id', '=', 'total_investimentos.periodo_id')
       ->where('total_investimentos.periodo_id', '=', 2)
       ->where('total_investimentos.ano_id', '=', $ano_id)
       ->where('total_investimentos.usuario_id', '=', $usuarioAutenticadoId)
       ->select(DB::raw('SUM(total_investimentos.valor) as valor'))
       ->value('valor');

   $investimento_fevereiro = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
       ->where('investimentos.periodo_id', '=', 2)
       ->where('investimentos.ano_id', '=', $ano_id)
       ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)
       ->select(DB::raw('SUM(investimentos.valor) as valor'))
       ->value('valor');

   $despesa_fevereiro = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
       ->where('despesas.periodo_id', '=', 2)
       ->where('despesas.ano_id', '=', $ano_id)
       ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)
       ->select(DB::raw('SUM(despesas.valor) as valor'))
       ->value('valor');;

   if (($investimento_fevereiro + $total_investimento_fevereiro + $despesa_fevereiro) != 0)
   {
       $roi_fevereiro = ($faturamento_fevereiro - ($investimento_fevereiro + $total_investimento_fevereiro + $despesa_fevereiro)) / ($investimento_fevereiro + $total_investimento_fevereiro + $despesa_fevereiro);
       //ROI = (faturamento - (ações de marketing+investimentos+despesas com vendas)) / (ações de marketing+investimento+despesas com vendas)
       
   }
   else
   {
       $roi_fevereiro = 0;
   }

   return $roi_fevereiro;


    }







    public function roiPesquisaPeriodoMarco(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');


       //ROI marco
       $faturamento_marco = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
       ->where('faturamentos.periodo_id', '=', 3)
       ->where('faturamentos.ano_id', '=', $ano_id)
       ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)
       ->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
       ->value('valor_faturamento');

   $total_investimento_marco = DB::table('total_investimentos')->join('periodos', 'periodos.id', '=', 'total_investimentos.periodo_id')
       ->where('total_investimentos.periodo_id', '=', 3)
       ->where('total_investimentos.ano_id', '=', $ano_id)
       ->where('total_investimentos.usuario_id', '=', $usuarioAutenticadoId)
       ->select(DB::raw('SUM(total_investimentos.valor) as valor'))
       ->value('valor');

   $investimento_marco = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
       ->where('investimentos.periodo_id', '=', 3)
       ->where('investimentos.ano_id', '=', $ano_id)
       ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)
       ->select(DB::raw('SUM(investimentos.valor) as valor'))
       ->value('valor');

   $despesa_marco = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
       ->where('despesas.periodo_id', '=', 3)
       ->where('despesas.ano_id', '=', $ano_id)
       ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)
       ->select(DB::raw('SUM(despesas.valor) as valor'))
       ->value('valor');;

   if (($investimento_marco + $total_investimento_marco + $despesa_marco) != 0)
   {
       $roi_marco = ($faturamento_marco - ($investimento_marco + $total_investimento_marco + $despesa_marco)) / ($investimento_marco + $total_investimento_marco + $despesa_marco);
       //ROI = (faturamento - (ações de marketing+investimentos+despesas com vendas)) / (ações de marketing+investimento+despesas com vendas)
       
   }
   else
   {
       $roi_marco = 0;
   }

   return $roi_marco;


    }







    public function roiPesquisaPeriodoAbril(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');


       //ROI abril
       $faturamento_abril = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
       ->where('faturamentos.periodo_id', '=', 4)
       ->where('faturamentos.ano_id', '=', $ano_id)
       ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)
       ->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
       ->value('valor_faturamento');

   $total_investimento_abril = DB::table('total_investimentos')->join('periodos', 'periodos.id', '=', 'total_investimentos.periodo_id')
       ->where('total_investimentos.periodo_id', '=', 4)
       ->where('total_investimentos.ano_id', '=', $ano_id)
       ->where('total_investimentos.usuario_id', '=', $usuarioAutenticadoId)
       ->select(DB::raw('SUM(total_investimentos.valor) as valor'))
       ->value('valor');

   $investimento_abril = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
       ->where('investimentos.periodo_id', '=', 4)
       ->where('investimentos.ano_id', '=', $ano_id)
       ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)
       ->select(DB::raw('SUM(investimentos.valor) as valor'))
       ->value('valor');

   $despesa_abril = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
       ->where('despesas.periodo_id', '=', 4)
       ->where('despesas.ano_id', '=', $ano_id)
       ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)
       ->select(DB::raw('SUM(despesas.valor) as valor'))
       ->value('valor');;

   if (($investimento_abril + $total_investimento_abril + $despesa_abril) != 0)
   {
       $roi_abril = ($faturamento_abril - ($investimento_abril + $total_investimento_abril + $despesa_abril)) / ($investimento_abril + $total_investimento_abril + $despesa_abril);
       //ROI = (faturamento - (ações de marketing+investimentos+despesas com vendas)) / (ações de marketing+investimento+despesas com vendas)
       
   }
   else
   {
       $roi_abril = 0;
   }

   return $roi_abril;


    }





    public function roiPesquisaPeriodoMaio(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');


       //ROI maio
       $faturamento_maio = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
       ->where('faturamentos.periodo_id', '=', 5)
       ->where('faturamentos.ano_id', '=', $ano_id)
       ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)
       ->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
       ->value('valor_faturamento');

   $total_investimento_maio = DB::table('total_investimentos')->join('periodos', 'periodos.id', '=', 'total_investimentos.periodo_id')
       ->where('total_investimentos.periodo_id', '=', 5)
       ->where('total_investimentos.ano_id', '=', $ano_id)
       ->where('total_investimentos.usuario_id', '=', $usuarioAutenticadoId)
       ->select(DB::raw('SUM(total_investimentos.valor) as valor'))
       ->value('valor');

   $investimento_maio = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
       ->where('investimentos.periodo_id', '=', 5)
       ->where('investimentos.ano_id', '=', $ano_id)
       ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)
       ->select(DB::raw('SUM(investimentos.valor) as valor'))
       ->value('valor');

   $despesa_maio = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
       ->where('despesas.periodo_id', '=', 5)
       ->where('despesas.ano_id', '=', $ano_id)
       ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)
       ->select(DB::raw('SUM(despesas.valor) as valor'))
       ->value('valor');;

   if (($investimento_maio + $total_investimento_maio + $despesa_maio) != 0)
   {
       $roi_maio = ($faturamento_maio - ($investimento_maio + $total_investimento_maio + $despesa_maio)) / ($investimento_maio + $total_investimento_maio + $despesa_maio);
       //ROI = (faturamento - (ações de marketing+investimentos+despesas com vendas)) / (ações de marketing+investimento+despesas com vendas)
       
   }
   else
   {
       $roi_maio = 0;
   }

   return $roi_maio;


    }






    public function roiPesquisaPeriodoJunho(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');


       //ROI junho
       $faturamento_junho = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
       ->where('faturamentos.periodo_id', '=', 6)
       ->where('faturamentos.ano_id', '=', $ano_id)
       ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)
       ->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
       ->value('valor_faturamento');

   $total_investimento_junho = DB::table('total_investimentos')->join('periodos', 'periodos.id', '=', 'total_investimentos.periodo_id')
       ->where('total_investimentos.periodo_id', '=', 6)
       ->where('total_investimentos.ano_id', '=', $ano_id)
       ->where('total_investimentos.usuario_id', '=', $usuarioAutenticadoId)
       ->select(DB::raw('SUM(total_investimentos.valor) as valor'))
       ->value('valor');

   $investimento_junho = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
       ->where('investimentos.periodo_id', '=', 6)
       ->where('investimentos.ano_id', '=', $ano_id)
       ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)
       ->select(DB::raw('SUM(investimentos.valor) as valor'))
       ->value('valor');

   $despesa_junho = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
       ->where('despesas.periodo_id', '=', 6)
       ->where('despesas.ano_id', '=', $ano_id)
       ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)
       ->select(DB::raw('SUM(despesas.valor) as valor'))
       ->value('valor');;

   if (($investimento_junho + $total_investimento_junho + $despesa_junho) != 0)
   {
       $roi_junho = ($faturamento_junho - ($investimento_junho + $total_investimento_junho + $despesa_junho)) / ($investimento_junho + $total_investimento_junho + $despesa_junho);
       //ROI = (faturamento - (ações de marketing+investimentos+despesas com vendas)) / (ações de marketing+investimento+despesas com vendas)
       
   }
   else
   {
       $roi_junho = 0;
   }

   return $roi_junho;


    }






    public function roiPesquisaPeriodoJulho(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');


       //ROI julho
       $faturamento_julho = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
       ->where('faturamentos.periodo_id', '=', 7)
       ->where('faturamentos.ano_id', '=', $ano_id)
       ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)
       ->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
       ->value('valor_faturamento');

   $total_investimento_julho = DB::table('total_investimentos')->join('periodos', 'periodos.id', '=', 'total_investimentos.periodo_id')
       ->where('total_investimentos.periodo_id', '=', 7)
       ->where('total_investimentos.ano_id', '=', $ano_id)
       ->where('total_investimentos.usuario_id', '=', $usuarioAutenticadoId)
       ->select(DB::raw('SUM(total_investimentos.valor) as valor'))
       ->value('valor');

   $investimento_julho = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
       ->where('investimentos.periodo_id', '=', 7)
       ->where('investimentos.ano_id', '=', $ano_id)
       ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)
       ->select(DB::raw('SUM(investimentos.valor) as valor'))
       ->value('valor');

   $despesa_julho = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
       ->where('despesas.periodo_id', '=', 7)
       ->where('despesas.ano_id', '=', $ano_id)
       ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)
       ->select(DB::raw('SUM(despesas.valor) as valor'))
       ->value('valor');;

   if (($investimento_julho + $total_investimento_julho + $despesa_julho) != 0)
   {
       $roi_julho = ($faturamento_julho - ($investimento_julho + $total_investimento_julho + $despesa_julho)) / ($investimento_julho + $total_investimento_julho + $despesa_julho);
       //ROI = (faturamento - (ações de marketing+investimentos+despesas com vendas)) / (ações de marketing+investimento+despesas com vendas)
       
   }
   else
   {
       $roi_julho = 0;
   }

   return $roi_julho;


    }





    public function roiPesquisaPeriodoAgosto(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');


       //ROI agosto
       $faturamento_agosto = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
       ->where('faturamentos.periodo_id', '=', 8)
       ->where('faturamentos.ano_id', '=', $ano_id)
       ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)
       ->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
       ->value('valor_faturamento');

   $total_investimento_agosto = DB::table('total_investimentos')->join('periodos', 'periodos.id', '=', 'total_investimentos.periodo_id')
       ->where('total_investimentos.periodo_id', '=', 8)
       ->where('total_investimentos.ano_id', '=', $ano_id)
       ->where('total_investimentos.usuario_id', '=', $usuarioAutenticadoId)
       ->select(DB::raw('SUM(total_investimentos.valor) as valor'))
       ->value('valor');

   $investimento_agosto = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
       ->where('investimentos.periodo_id', '=', 8)
       ->where('investimentos.ano_id', '=', $ano_id)
       ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)
       ->select(DB::raw('SUM(investimentos.valor) as valor'))
       ->value('valor');

   $despesa_agosto = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
       ->where('despesas.periodo_id', '=', 8)
       ->where('despesas.ano_id', '=', $ano_id)
       ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)
       ->select(DB::raw('SUM(despesas.valor) as valor'))
       ->value('valor');;

   if (($investimento_agosto + $total_investimento_agosto + $despesa_agosto) != 0)
   {
       $roi_agosto = ($faturamento_agosto - ($investimento_agosto + $total_investimento_agosto + $despesa_agosto)) / ($investimento_agosto + $total_investimento_agosto + $despesa_agosto);
       //ROI = (faturamento - (ações de marketing+investimentos+despesas com vendas)) / (ações de marketing+investimento+despesas com vendas)
       
   }
   else
   {
       $roi_agosto = 0;
   }

   return $roi_agosto;


    }




    public function roiPesquisaPeriodoSetembro(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');


       //ROI setembro
       $faturamento_setembro = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
       ->where('faturamentos.periodo_id', '=', 9)
       ->where('faturamentos.ano_id', '=', $ano_id)
       ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)
       ->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
       ->value('valor_faturamento');

   $total_investimento_setembro = DB::table('total_investimentos')->join('periodos', 'periodos.id', '=', 'total_investimentos.periodo_id')
       ->where('total_investimentos.periodo_id', '=', 9)
       ->where('total_investimentos.ano_id', '=', $ano_id)
       ->where('total_investimentos.usuario_id', '=', $usuarioAutenticadoId)
       ->select(DB::raw('SUM(total_investimentos.valor) as valor'))
       ->value('valor');

   $investimento_setembro = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
       ->where('investimentos.periodo_id', '=', 9)
       ->where('investimentos.ano_id', '=', $ano_id)
       ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)
       ->select(DB::raw('SUM(investimentos.valor) as valor'))
       ->value('valor');

   $despesa_setembro = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
       ->where('despesas.periodo_id', '=', 9)
       ->where('despesas.ano_id', '=', $ano_id)
       ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)
       ->select(DB::raw('SUM(despesas.valor) as valor'))
       ->value('valor');;

   if (($investimento_setembro + $total_investimento_setembro + $despesa_setembro) != 0)
   {
       $roi_setembro = ($faturamento_setembro - ($investimento_setembro + $total_investimento_setembro + $despesa_setembro)) / ($investimento_setembro + $total_investimento_setembro + $despesa_setembro);
       //ROI = (faturamento - (ações de marketing+investimentos+despesas com vendas)) / (ações de marketing+investimento+despesas com vendas)
       
   }
   else
   {
       $roi_setembro = 0;
   }

   return $roi_setembro;


    }




    public function roiPesquisaPeriodoOutubro(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');


       //ROI outubro
       $faturamento_outubro = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
       ->where('faturamentos.periodo_id', '=', 10)
       ->where('faturamentos.ano_id', '=', $ano_id)
       ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)
       ->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
       ->value('valor_faturamento');

   $total_investimento_outubro = DB::table('total_investimentos')->join('periodos', 'periodos.id', '=', 'total_investimentos.periodo_id')
       ->where('total_investimentos.periodo_id', '=', 10)
       ->where('total_investimentos.ano_id', '=', $ano_id)
       ->where('total_investimentos.usuario_id', '=', $usuarioAutenticadoId)
       ->select(DB::raw('SUM(total_investimentos.valor) as valor'))
       ->value('valor');

   $investimento_outubro = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
       ->where('investimentos.periodo_id', '=', 10)
       ->where('investimentos.ano_id', '=', $ano_id)
       ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)
       ->select(DB::raw('SUM(investimentos.valor) as valor'))
       ->value('valor');

   $despesa_outubro = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
       ->where('despesas.periodo_id', '=', 10)
       ->where('despesas.ano_id', '=', $ano_id)
       ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)
       ->select(DB::raw('SUM(despesas.valor) as valor'))
       ->value('valor');;

   if (($investimento_outubro + $total_investimento_outubro + $despesa_outubro) != 0)
   {
       $roi_outubro = ($faturamento_outubro - ($investimento_outubro + $total_investimento_outubro + $despesa_outubro)) / ($investimento_outubro + $total_investimento_outubro + $despesa_outubro);
       //ROI = (faturamento - (ações de marketing+investimentos+despesas com vendas)) / (ações de marketing+investimento+despesas com vendas)
       
   }
   else
   {
       $roi_outubro = 0;
   }

   return $roi_outubro;


    }




    public function roiPesquisaPeriodoNovembro(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');


       //ROI novembro
       $faturamento_novembro = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
       ->where('faturamentos.periodo_id', '=', 11)
       ->where('faturamentos.ano_id', '=', $ano_id)
       ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)
       ->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
       ->value('valor_faturamento');

   $total_investimento_novembro = DB::table('total_investimentos')->join('periodos', 'periodos.id', '=', 'total_investimentos.periodo_id')
       ->where('total_investimentos.periodo_id', '=', 11)
       ->where('total_investimentos.ano_id', '=', $ano_id)
       ->where('total_investimentos.usuario_id', '=', $usuarioAutenticadoId)
       ->select(DB::raw('SUM(total_investimentos.valor) as valor'))
       ->value('valor');

   $investimento_novembro = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
       ->where('investimentos.periodo_id', '=', 11)
       ->where('investimentos.ano_id', '=', $ano_id)
       ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)
       ->select(DB::raw('SUM(investimentos.valor) as valor'))
       ->value('valor');

   $despesa_novembro = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
       ->where('despesas.periodo_id', '=', 11)
       ->where('despesas.ano_id', '=', $ano_id)
       ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)
       ->select(DB::raw('SUM(despesas.valor) as valor'))
       ->value('valor');;

   if (($investimento_novembro + $total_investimento_novembro + $despesa_novembro) != 0)
   {
       $roi_novembro = ($faturamento_novembro - ($investimento_novembro + $total_investimento_novembro + $despesa_novembro)) / ($investimento_novembro + $total_investimento_novembro + $despesa_novembro);
       //ROI = (faturamento - (ações de marketing+investimentos+despesas com vendas)) / (ações de marketing+investimento+despesas com vendas)
       
   }
   else
   {
       $roi_novembro = 0;
   }

   return $roi_novembro;


    }





 public function roiPesquisaPeriodoDezembro(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');


       //ROI dezembro
       $faturamento_dezembro = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
       ->where('faturamentos.periodo_id', '=', 12)
       ->where('faturamentos.ano_id', '=', $ano_id)
       ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)
       ->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
       ->value('valor_faturamento');

   $total_investimento_dezembro = DB::table('total_investimentos')->join('periodos', 'periodos.id', '=', 'total_investimentos.periodo_id')
       ->where('total_investimentos.periodo_id', '=', 12)
       ->where('total_investimentos.ano_id', '=', $ano_id)
       ->where('total_investimentos.usuario_id', '=', $usuarioAutenticadoId)
       ->select(DB::raw('SUM(total_investimentos.valor) as valor'))
       ->value('valor');

   $investimento_dezembro = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
       ->where('investimentos.periodo_id', '=', 12)
       ->where('investimentos.ano_id', '=', $ano_id)
       ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)
       ->select(DB::raw('SUM(investimentos.valor) as valor'))
       ->value('valor');

   $despesa_dezembro = DB::table('despesas')->join('periodos', 'periodos.id', '=', 'despesas.periodo_id')
       ->where('despesas.periodo_id', '=', 12)
       ->where('despesas.ano_id', '=', $ano_id)
       ->where('despesas.usuario_id', '=', $usuarioAutenticadoId)
       ->select(DB::raw('SUM(despesas.valor) as valor'))
       ->value('valor');;

   if (($investimento_dezembro + $total_investimento_dezembro + $despesa_dezembro) != 0)
   {
       $roi_dezembro = ($faturamento_dezembro - ($investimento_dezembro + $total_investimento_dezembro + $despesa_dezembro)) / ($investimento_dezembro + $total_investimento_dezembro + $despesa_dezembro);
       //ROI = (faturamento - (ações de marketing+investimentos+despesas com vendas)) / (ações de marketing+investimento+despesas com vendas)
       
   }
   else
   {
       $roi_dezembro = 0;
   }

   return $roi_dezembro;


    }



    public function pesquisaRoi(Request $request)
    {

       
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;


        //ROI ATUAL
        $roi_pagina = $this->roiPesquisaPeriodo($request);

        //ROI JANEIRO
        $roi_janeiro = $this->roiPesquisaPeriodoJaneiro($request);

        //ROI FEVEREIRO
        $roi_fevereiro = $this->roiPesquisaPeriodoFevereiro($request);

        //ROI MARÇO
        $roi_marco = $this->roiPesquisaPeriodoMarco($request);

        //ROI ABRIL
        $roi_abril = $this->roiPesquisaPeriodoAbril($request);

        //ROI MAIO
       $roi_maio = $this->roiPesquisaPeriodoMaio($request);

        //ROI JUNHO
       $roi_junho = $this->roiPesquisaPeriodoJunho($request);

        //ROI JULHO
       $roi_julho = $this->roiPesquisaPeriodoJulho($request);

        //ROI AGOSTO
        $roi_agosto = $this->roiPesquisaPeriodoAgosto($request);

        //ROI SETEMBRO
       $roi_setembro = $this->roiPesquisaPeriodoSetembro($request);

        //ROI OUTUBRO
        $roi_outubro = $this->roiPesquisaPeriodoOutubro($request);

        //ROI NOVEMBRO
       $roi_novembro = $this->roiPesquisaPeriodoNovembro($request);

        //ROI DEZEMBRO
       $roi_dezembro = $this->roiPesquisaPeriodoDezembro($request);

        

        if ($roi_pagina == 0)
        {
            $valida_roi = "Investimento nulo.";
        }
        else if ($roi_pagina < 0)
        {
            $valida_roi = "Seus investimentos não estão dando retorno.";
        }
        else
        {
            $valida_roi = "Seu retorno de investimento foi de $roi_pagina x";
        }


        
        return response()->json([
            'roi_pagina' => number_format($roi_pagina,2),
            'valida_roi' => $valida_roi,
            'roi_janeiro' => $roi_janeiro,
            'roi_fevereiro' => $roi_fevereiro,
            'roi_marco' => $roi_marco,
            'roi_abril' => $roi_abril,
            'roi_maio' => $roi_maio,
            'roi_junho' => $roi_junho,
            'roi_julho' => $roi_julho,
            'roi_agosto' => $roi_agosto,
            'roi_setembro' => $roi_setembro,
            'roi_outubro' => $roi_outubro,
            'roi_novembro' => $roi_novembro,
            'roi_dezembro' => $roi_dezembro,


        ]);


        
    }












}