<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuario;
use App\Cargo;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\Routing\ResponseFactory;
use Auth;
use Session;
use Carbon\Carbon;

class ROMIMobileController extends Controller
{

    public function romiAtualAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');


        $faturamento_atual = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
            ->where('faturamentos.periodo_id', '=', $data_atual->month)
            ->where('faturamentos.ano_id', '=', $data_atual->year)
            ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
            ->value('valor_faturamento');

        $investimento_atual = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
            ->where('investimentos.periodo_id', '=', $data_atual->month)
            ->where('investimentos.ano_id', '=', $data_atual->year)
            ->where('investimentos.usuario_id ', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(investimentos.valor) as valor'))
            ->value('valor');

        if ($investimento_atual != 0)
        {
            $romi_pagina = number_format(($faturamento_atual - $investimento_atual) / $investimento_atual, 2);
        }
        else
        {
            $romi_pagina = 0;
        }


        return response()->json([
            'romi_pagina' => number_format($romi_pagina,2)
        ]);
        

    }





    public function romiPaginaAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');


        $faturamento_atual = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
            ->where('faturamentos.periodo_id', '=', $data_atual->month)
            ->where('faturamentos.ano_id', '=', $data_atual->year)
            ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
            ->value('valor_faturamento');

        $investimento_atual = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
            ->where('investimentos.periodo_id', '=', $data_atual->month)
            ->where('investimentos.ano_id', '=', $data_atual->year)
            ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(investimentos.valor) as valor'))
            ->value('valor');

        if ($investimento_atual != 0)
        {
            $romi_pagina = number_format(($faturamento_atual - $investimento_atual) / $investimento_atual, 2);
        }
        else
        {
            $romi_pagina = 0;
        }


      return $romi_pagina;
        

    }




    public function romiDezembroAnoPassadoAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        

        $faturamento_dezembro_ano_passado = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', 12)
        ->where('faturamentos.ano_id', '=', $data_atual->year - 1)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');

    $investimento_dezembro_ano_passado = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 12)
        ->where('investimentos.ano_id', '=', $data_atual->year - 1)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

        if ($investimento_dezembro_ano_passado != 0)
        {
            $romi_dezembro_ano_passado = ($faturamento_dezembro_ano_passado - $investimento_dezembro_ano_passado) / $investimento_dezembro_ano_passado;
        }
        else
        {
            $romi_dezembro_ano_passado = 0;
        }

        return $romi_dezembro_ano_passado;


    }






    public function romiJaneiroAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');


        $faturamento_janeiro = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', 1)
        ->where('faturamentos.ano_id', '=', $data_atual->year)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');

    $investimento_janeiro = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 1)
        ->where('investimentos.ano_id', '=', $data_atual->year)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

        if ($investimento_janeiro != 0)
        {
            $romi_janeiro = ($faturamento_janeiro - $investimento_janeiro) / $investimento_janeiro;
        }
        else
        {
            $romi_janeiro = 0;
        }

        return $romi_janeiro;


    }





    public function romiFevereiroAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

      
 

        $faturamento_fevereiro = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
            ->where('faturamentos.periodo_id', '=', 2)
            ->where('faturamentos.ano_id', '=', $data_atual->year)
            ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
            ->value('valor_faturamento');

        $investimento_fevereiro = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
            ->where('investimentos.periodo_id', '=', 2)
            ->where('investimentos.ano_id', '=', $data_atual->year)
            ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(investimentos.valor) as valor'))
            ->value('valor');

        if ($investimento_fevereiro != 0)
        {
            $romi_fevereiro = ($faturamento_fevereiro - $investimento_fevereiro) / $investimento_fevereiro;
        }
        else
        {
            $romi_fevereiro = 0;
        }

        return $romi_fevereiro;

    }



    public function romiMarcoAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

      

        $faturamento_marco = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', 3)
        ->where('faturamentos.ano_id', '=', $data_atual->year)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');

    $investimento_marco = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 3)
        ->where('investimentos.ano_id', '=', $data_atual->year)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

    if ($investimento_marco != 0)
    {
        $romi_marco = ($faturamento_marco - $investimento_marco) / $investimento_marco;
    }
    else
    {
        $romi_marco = 0;
    }

    return $romi_marco;

    }





    public function romiAbrilAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        
           
        $faturamento_abril = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', 4)
        ->where('faturamentos.ano_id', '=', $data_atual->year)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');

    $investimento_abril = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 4)
        ->where('investimentos.ano_id', '=', $data_atual->year)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

    if ($investimento_abril != 0)
    {
        $romi_abril = ($faturamento_abril - $investimento_abril) / $investimento_abril;
    }
    else
    {
        $romi_abril = 0;
    }

    return $romi_abril;


    }



    public function romiMaioAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

       

        $faturamento_maio = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', 5)
        ->where('faturamentos.ano_id', '=', $data_atual->year)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');

    $investimento_maio = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 5)
        ->where('investimentos.ano_id', '=', $data_atual->year)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

    if ($investimento_maio != 0)
    {
        $romi_maio = ($faturamento_maio - $investimento_maio) / $investimento_maio;
    }
    else
    {
        $romi_maio = 0;
    }

    return $romi_maio;

    }





    public function romiJunhoAPI(Request $request){
       
        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        

        $faturamento_junho = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', 6)
        ->where('faturamentos.ano_id', '=', $data_atual->year)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');

    $investimento_junho = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 6)
        ->where('investimentos.ano_id', '=', $data_atual->year)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

    if ($investimento_junho != 0)
    {
        $romi_junho = ($faturamento_junho - $investimento_junho) / $investimento_junho;
    }
    else
    {
        $romi_junho = 0;
    }

    return $romi_junho;


    }



    public function romiJulhoAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

      

        $faturamento_julho = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', 7)
        ->where('faturamentos.ano_id', '=', $data_atual->year)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');

    $investimento_julho = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 7)
        ->where('investimentos.ano_id', '=', $data_atual->year)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

    if ($investimento_julho != 0)
    {
        $romi_julho = ($faturamento_julho - $investimento_julho) / $investimento_julho;
    }
    else
    {
        $romi_julho = 0;
    }

    return $romi_julho;

    }


    public function romiAgostoAPI(Request $request){


        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

      

        $faturamento_agosto = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', 8)
        ->where('faturamentos.ano_id', '=', $data_atual->year)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');

    $investimento_agosto = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 8)
        ->where('investimentos.ano_id', '=', $data_atual->year)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

    if ($investimento_agosto != 0)
    {
        $romi_agosto = ($faturamento_agosto - $investimento_agosto) / $investimento_agosto;
    }
    else
    {
        $romi_agosto = 0;
    }


    return $romi_agosto;


    }


    public function romiSetembroAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

           

        $faturamento_setembro = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', 9)
        ->where('faturamentos.ano_id', '=', $data_atual->year)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');

    $investimento_setembro = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 9)
        ->where('investimentos.ano_id', '=', $data_atual->year)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

    if ($investimento_setembro != 0)
    {
        $romi_setembro = ($faturamento_setembro - $investimento_setembro) / $investimento_setembro;
    }
    else
    {
        $romi_setembro = 0;
    }

    return $romi_setembro;

    }





    public function romiOutubroAPI(Request $request){


        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

       


        $faturamento_outubro = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', 10)
        ->where('faturamentos.ano_id', '=', $data_atual->year)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');

    $investimento_outubro = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 10)
        ->where('investimentos.ano_id', '=', $data_atual->year)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

    if ($investimento_outubro != 0)
    {
        $romi_outubro = ($faturamento_outubro - $investimento_outubro) / $investimento_outubro;
    }
    else
    {
        $romi_outubro = 0;
    }


    return $romi_outubro;

    }




    public function romiNovembroAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        

        $faturamento_novembro = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', 11)
        ->where('faturamentos.ano_id', '=', $data_atual->year)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');

    $investimento_novembro = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 11)
        ->where('investimentos.ano_id', '=', $data_atual->year)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

    if ($investimento_novembro != 0)
    {
        $romi_novembro = ($faturamento_novembro - $investimento_novembro) / $investimento_novembro;
    }
    else
    {
        $romi_novembro = 0;
    }

    return $romi_novembro;



    }


    public function romiDezembroAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

       

        $faturamento_dezembro = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', 12)
        ->where('faturamentos.ano_id', '=', $data_atual->year)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');

    $investimento_dezembro = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 12)
        ->where('investimentos.ano_id', '=', $data_atual->year)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

    if ($investimento_dezembro != 0)
    {
        $romi_dezembro = ($faturamento_dezembro - $investimento_dezembro) / $investimento_dezembro;
    }
    else
    {
        $romi_dezembro = 0;
    }

    return $romi_dezembro;

    }






    public function paginaRomiAPI(Request $request)
    {

        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $data_atual = Carbon::now();


        //ROMI ATUAL
        $romi_pagina = $this->romiPaginaAPI($request);

        //ROMI DEZEMBRO DO ANO PASSADO
        $romi_dezembro_ano_passado = $this->romiDezembroAnoPassadoAPI($request);

        //ROMI JANEIRO
       $romi_janeiro = $this->romiJaneiroAPI($request);


        //ROMI FEVEREIRO
       $romi_fevereiro = $this->romiFevereiroAPI($request); 

        //ROMI MARÇO
        $romi_marco = $this->romiMarcoAPI($request);
        

        //ROMI ABRIL
        $romi_abril = $this->romiAbrilAPI($request);
      
        //ROMI MAIO
       $romi_maio = $this->romiMaioAPI($request);

        //ROMI JUNHO
        $romi_junho = $this->romiJunhoAPI($request);

        //ROMI JULHO
        $romi_julho = $this->romiJulhoAPI($request);
       
        //ROMI AGOSTO
        $romi_agosto = $this->romiAgostoAPI($request);
       
        //ROMI SETEMBRO
        $romi_setembro = $this->romiSetembroAPI($request);
       

        //ROMI OUTUBRO
        $romi_outubro = $this->romiOutubroAPI($request);
       

        //ROMI NOVEMBRO
        $romi_novembro = $this->romiNovembroAPI($request);
            

        //ROMI DEZEMBRO
        $romi_dezembro = $this->romiDezembroAPI($request);

      
        //Comparação de Dados

        //Janeiro/Dezembro do Ano Passado
        if ($romi_janeiro == 0 || $romi_dezembro_ano_passado == 0)
        {
            $romi_janeiro_dezembro = 0;
        }
        else
        {
            $romi_janeiro_dezembro = (($romi_janeiro / $romi_dezembro_ano_passado) - 1) * 100;
        }

        //Fevereiro/Janeiro
        if ($romi_fevereiro == 0 || $romi_janeiro == 0)
        {
            $romi_fevereiro_janeiro = 0;
        }
        else
        {
            $romi_fevereiro_janeiro = (($romi_fevereiro / $romi_janeiro) - 1) * 100;
        }

        //Marco/Fevereiro
        if ($romi_marco == 0 || $romi_fevereiro == 0)
        {
            $romi_marco_fevereiro = 0;
        }
        else
        {
            $romi_marco_fevereiro = (($romi_marco / $romi_fevereiro) - 1) * 100;
        }

        //Abril/Marco
        if ($romi_abril == 0 || $romi_marco == 0)
        {
            $romi_abril_marco = 0;
        }
        else
        {
            $romi_abril_marco = (($romi_abril / $romi_marco) - 1) * 100;
        }

        //Maio/Abril
        if ($romi_maio == 0 || $romi_abril == 0)
        {
            $romi_maio_abril = 0;
        }
        else
        {
            $romi_maio_abril = (($romi_maio / $romi_abril) - 1) * 100;
        }

        //Junho/Maio
        if ($romi_junho == 0 || $romi_maio == 0)
        {
            $romi_junho_maio = 0;
        }
        else
        {
            $romi_junho_maio = (($romi_junho / $romi_maio) - 1) * 100;
        }

        //Julho/Junho
        if ($romi_julho == 0 || $romi_junho == 0)
        {
            $romi_julho_junho = 0;
        }
        else
        {
            $romi_julho_junho = (($romi_julho / $romi_junho) - 1) * 100;
        }

        //Agosto/Julho
        if ($romi_agosto == 0 || $romi_julho == 0)
        {
            $romi_agosto_julho = 0;
        }
        else
        {
            $romi_agosto_julho = (($romi_agosto / $romi_julho) - 1) * 100;
        }

        //Setembro/Agosto
        if ($romi_setembro == 0 || $romi_agosto == 0)
        {
            $romi_setembro_agosto = 0;
        }
        else
        {
            $romi_setembro_agosto = (($romi_setembro / $romi_agosto) - 1) * 100;
        }

        //Outubro/Setembro
        if ($romi_outubro == 0 || $romi_setembro == 0)
        {
            $romi_outubro_setembro = 0;
        }
        else
        {
            $romi_outubro_setembro = (($romi_outubro / $romi_setembro) - 1) * 100;
        }

        //Novembro/Outubro
        if ($romi_novembro == 0 || $romi_outubro == 0)
        {
            $romi_novembro_outubro = 0;
        }
        else
        {
            $romi_novembro_outubro = (($romi_novembro / $romi_outubro) - 1) * 100;
        }

        //Dezembro/Novembro
        if ($romi_dezembro == 0 || $romi_novembro == 0)
        {
            $romi_dezembro_novembro = 0;
        }
        else
        {
            $romi_dezembro_novembro = (($romi_dezembro / $romi_novembro) - 1) * 100;
        }

        if ($romi_pagina == 0)
        {
            $valida_romi = "Investimento nulo.";
        }
        else if ($romi_pagina < 0)
        {
            $valida_romi = "Seus investimentos não estão dando retorno.";
        }
        else
        {
            $valida_romi = "Seu retorno de investimento foi de $romi_pagina x";
        }



        return response()->json([
            'romi_pagina' => number_format($romi_pagina,2),
            'valida_romi' => $valida_romi,
            'romi_janeiro'=> ($romi_janeiro),
            'romi_fevereiro' => ($romi_fevereiro),
            'romi_marco' => ($romi_marco),
            'romi_abril' => ($romi_abril),
            'romi_maio' => ($romi_maio),
            'romi_junho' => ($romi_junho),
            'romi_julho' => ($romi_julho),
            'romi_agosto' => ($romi_agosto),
            'romi_setembro' => ($romi_setembro),
            'romi_outubro'  =>($romi_outubro),
            'romi_novembro' => ($romi_novembro),
            'romi_dezembro'  => ($romi_dezembro),
        ]);


    
    }



    public function romiPesquisaPeriodo(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $faturamento_atual = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', $periodo_id)->where('faturamentos.ano_id', '=', $ano_id)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)
        ->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');

    $investimento_atual = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', $periodo_id)->where('investimentos.ano_id', '=', $ano_id)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)
        ->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

    if ($investimento_atual != 0)
    {
        $romi_pagina = number_format(($faturamento_atual - $investimento_atual) / $investimento_atual, 2);
    }
    else
    {
        $romi_pagina = 0;
    }

    return $romi_pagina;



    }

    public function romiPesquisaPeriodoJaneiro(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $faturamento_janeiro = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
            ->where('faturamentos.periodo_id', '=', 1)
            ->where('faturamentos.ano_id', '=', $ano_id)
            ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)
            ->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
            ->value('valor_faturamento');

        $investimento_janeiro = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
            ->where('investimentos.periodo_id', '=', 1)
            ->where('investimentos.ano_id', '=', $ano_id)
            ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)
            ->select(DB::raw('SUM(investimentos.valor) as valor'))
            ->value('valor');

        if ($investimento_janeiro != 0)
        {
            $romi_janeiro = ($faturamento_janeiro - $investimento_janeiro) / $investimento_janeiro;
        }
        else
        {
            $romi_janeiro = 0;
        }

        return $romi_janeiro;

    }


    public function romiPesquisaPeriodoFevereiro(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $faturamento_fevereiro = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', 2)
        ->where('faturamentos.ano_id', '=', $ano_id)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)
        ->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');

    $investimento_fevereiro = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 2)
        ->where('investimentos.ano_id', '=', $ano_id)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)
        ->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

    if ($investimento_fevereiro != 0)
    {
        $romi_fevereiro = ($faturamento_fevereiro - $investimento_fevereiro) / $investimento_fevereiro;
    }
    else
    {
        $romi_fevereiro = 0;
    }

    return $romi_fevereiro;



    }




    public function romiPesquisaPeriodoMarco(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');


        $faturamento_marco = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', 3)
        ->where('faturamentos.ano_id', '=', $ano_id)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)
        ->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');

    $investimento_marco = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 3)
        ->where('investimentos.ano_id', '=', $ano_id)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)
        ->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

    if ($investimento_marco != 0)
    {
        $romi_marco = ($faturamento_marco - $investimento_marco) / $investimento_marco;
    }
    else
    {
        $romi_marco = 0;
    }

    return $romi_marco;

    }




    public function romiPesquisaPeriodoAbril(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');


        $faturamento_abril = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', 4)
        ->where('faturamentos.ano_id', '=', $ano_id)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)
        ->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');

    $investimento_abril = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 4)
        ->where('investimentos.ano_id', '=', $ano_id)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)
        ->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

    if ($investimento_abril != 0)
    {
        $romi_abril = ($faturamento_abril - $investimento_abril) / $investimento_abril;
    }
    else
    {
        $romi_abril = 0;
    }

    return $romi_abril;

    }



    public function romiPesquisaPeriodoMaio(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');


        $faturamento_maio = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', 5)
        ->where('faturamentos.ano_id', '=', $ano_id)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)
        ->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');

    $investimento_maio = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 5)
        ->where('investimentos.ano_id', '=', $ano_id)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)
        ->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

    if ($investimento_maio != 0)
    {
        $romi_maio = ($faturamento_maio - $investimento_maio) / $investimento_maio;
    }
    else
    {
        $romi_maio = 0;
    }

    return $romi_maio;

    }



    public function romiPesquisaPeriodoJunho(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');


        $faturamento_junho = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', 6)
        ->where('faturamentos.ano_id', '=', $ano_id)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)
        ->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');

    $investimento_junho = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 6)
        ->where('investimentos.ano_id', '=', $ano_id)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)
        ->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

    if ($investimento_junho != 0)
    {
        $romi_junho = ($faturamento_junho - $investimento_junho) / $investimento_junho;
    }
    else
    {
        $romi_junho = 0;
    }

    return $romi_junho;

    }




    public function romiPesquisaPeriodoJulho(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');


        $faturamento_julho = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', 7)
        ->where('faturamentos.ano_id', '=', $ano_id)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)
        ->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');

    $investimento_julho = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 7)
        ->where('investimentos.ano_id', '=', $ano_id)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)
        ->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

    if ($investimento_julho != 0)
    {
        $romi_julho = ($faturamento_julho - $investimento_julho) / $investimento_julho;
    }
    else
    {
        $romi_julho = 0;
    }

    return $romi_julho;

    }





    public function romiPesquisaPeriodoAgosto(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');


        $faturamento_agosto = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', 8)
        ->where('faturamentos.ano_id', '=', $ano_id)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)
        ->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');

    $investimento_agosto = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 8)
        ->where('investimentos.ano_id', '=', $ano_id)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)
        ->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

    if ($investimento_agosto != 0)
    {
        $romi_agosto = ($faturamento_agosto - $investimento_agosto) / $investimento_agosto;
    }
    else
    {
        $romi_agosto = 0;
    }

    return $romi_agosto;

    }



    public function romiPesquisaPeriodoSetembro(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');


        $faturamento_setembro = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', 9)
        ->where('faturamentos.ano_id', '=', $ano_id)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)
        ->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');

    $investimento_setembro = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 9)
        ->where('investimentos.ano_id', '=', $ano_id)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)
        ->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

    if ($investimento_setembro != 0)
    {
        $romi_setembro = ($faturamento_setembro - $investimento_setembro) / $investimento_setembro;
    }
    else
    {
        $romi_setembro = 0;
    }

    return $romi_setembro;

    }



    public function romiPesquisaPeriodoOutubro(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');


        $faturamento_outubro = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', 10)
        ->where('faturamentos.ano_id', '=', $ano_id)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)
        ->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');

    $investimento_outubro = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 10)
        ->where('investimentos.ano_id', '=', $ano_id)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)
        ->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

    if ($investimento_outubro != 0)
    {
        $romi_outubro = ($faturamento_outubro - $investimento_outubro) / $investimento_outubro;
    }
    else
    {
        $romi_outubro = 0;
    }

    return $romi_outubro;

    }



    public function romiPesquisaPeriodoNovembro(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');


        $faturamento_novembro = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', 11)
        ->where('faturamentos.ano_id', '=', $ano_id)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)
        ->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');

    $investimento_novembro = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 11)
        ->where('investimentos.ano_id', '=', $ano_id)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)
        ->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

    if ($investimento_novembro != 0)
    {
        $romi_novembro = ($faturamento_novembro - $investimento_novembro) / $investimento_novembro;
    }
    else
    {
        $romi_novembro = 0;
    }

    return $romi_novembro;

    }




    public function romiPesquisaPeriodoDezembro(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');


        $faturamento_dezembro = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', 12)
        ->where('faturamentos.ano_id', '=', $ano_id)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)
        ->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');

    $investimento_dezembro = DB::table('investimentos')->join('periodos', 'periodos.id', '=', 'investimentos.periodo_id')
        ->where('investimentos.periodo_id', '=', 12)
        ->where('investimentos.ano_id', '=', $ano_id)
        ->where('investimentos.usuario_id', '=', $usuarioAutenticadoId)
        ->select(DB::raw('SUM(investimentos.valor) as valor'))
        ->value('valor');

    if ($investimento_dezembro != 0)
    {
        $romi_dezembro = ($faturamento_dezembro - $investimento_dezembro) / $investimento_dezembro;
    }
    else
    {
        $romi_dezembro = 0;
    }

    return $romi_dezembro;

    }







    public function pesquisaRomi(Request $request)
    {

       

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');



        //ROMI ATUAL
       $romi_pagina = $this->romiPesquisaPeriodo($request);

        //ROMI JANEIRO
       $romi_janeiro = $this->romiPesquisaPeriodoJaneiro($request);

        //ROMI FEVEREIRO
        $romi_fevereiro = $this->romiPesquisaPeriodoFevereiro($request);


        //ROMI MARÇO
        $romi_marco = $this->romiPesquisaPeriodoMarco($request);


       

        //ROMI ABRIL
        $romi_abril = $this->romiPesquisaPeriodoAbril($request);

        
        //ROMI MAIO
        $romi_maio = $this->romiPesquisaPeriodoMaio($request);

       

        //ROMI JUNHO
        $romi_junho = $this->romiPesquisaPeriodoJunho($request);

        

       

        //ROMI JULHO
        $romi_julho = $this->romiPesquisaPeriodoJulho($request);

       
        //ROMI AGOSTO
        $romi_agosto = $this->romiPesquisaPeriodoAgosto($request);

       
        //ROMI SETEMBRO
        $romi_setembro = $this->romiPesquisaPeriodoSetembro($request);

       
        //ROMI OUTUBRO
        $romi_outubro = $this->romiPesquisaPeriodoOutubro($request);

       
        //ROMI NOVEMBRO
        $romi_novembro = $this->romiPesquisaPeriodoNovembro($request);


       
        //ROMI DEZEMBRO
        $romi_dezembro = $this->romiPesquisaPeriodoDezembro($request);

       


        if ($romi_pagina == 0)
        {
            $valida_romi = "Investimento nulo.";
        }
        else if ($romi_pagina < 0)
        {
            $valida_romi = "Seus investimentos não estão dando retorno";
        }
        else
        {
            $valida_romi = "Seu retorno de investimento foi de $romi_pagina x";
        }


        return response()->json([
            'romi_pagina' => number_format($romi_pagina,2),
            'valida_romi' => $valida_romi,
            'romi_janeiro' => $romi_janeiro,
            'romi_fevereiro' => $romi_fevereiro,
            'romi_marco' => $romi_marco,
            'romi_abril' => $romi_abril,
            'romi_maio' => $romi_maio,
            'romi_junho' => $romi_junho,
            'romi_julho' => $romi_julho,
            'romi_agosto' => $romi_agosto,
            'romi_setembro' => $romi_setembro,
            'romi_outubro' => $romi_outubro,
            'romi_novembro' => $romi_novembro,
            'romi_dezembro' => $romi_dezembro,


        ]);
        
        


    }







}