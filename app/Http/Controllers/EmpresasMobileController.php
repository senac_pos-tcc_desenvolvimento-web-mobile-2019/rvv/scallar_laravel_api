<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Empresa;
use App\Segmento;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Routing\ResponseFactory;
use Auth;


class EmpresasMobileController extends Controller
{



    public function cadastrarEmpresaAPI(Request $request){

        $empresas = Empresa::create([

            'razao_social' => $request['razao_social'],
            'cnpj' => $request['cnpj'],
            'cod_empresa' => $request['cod_empresa'],
            'numero_funcionarios' => $request['numero_funcionarios'],
            'faturamento_anual' => $request['faturamento_anual'],
            'media_retencao_clientes' => $request['media_retencao_clientes'],
            'usuario_id' => $request['usuario_id'],
            'segmento_id' => $request['segmento_id'],


        ]);


        if($empresas){

            return response()->json(
                'Empresa cadastrado com sucesso'
            );

        }else{

            return response()->json(
                'Erro ao salvar a empresa'
            );

        } 


    }




    public function editarEmpresaAPI(Request $request, $id){

        $dados = $request->all();

        $empresaId = Empresa::find($id);

        $alt = $empresaId->update($dados);

        if ($alt)
        {

            return response()->json(
                'Empresa atualizada'
            );

        }else{

            return response()->json(
                'Erro ao atualizar a empresa'
            );
            

        }



    }





    public function excluirEmpresaAPI($id){

        $empresa = Empresa::find($id);

        if ($empresa->delete()){
                return response()->json(
                'Empresa deletada'
                );

        }else{
            return response()->json(
                'Erro ao excluir a empresa'
            );
        }
        
    }



    public function listarEmpresaAPI(Request $request){

        $usuario_autenticado_id = $request->session()->get('usuarioId');


        $empresas = Empresa::where('usuario_id', $usuario_autenticado_id)->orderBy('id', 'desc')
        ->paginate(4);


        return response()->json($empresas);


    }

    public function listarSegmentoEmpresaAPI(){

        $segmentos = Segmento::all();

        return response()->json($segmentos);


    }












    
}
