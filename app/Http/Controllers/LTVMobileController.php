<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuario;
use App\Cargo;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\Routing\ResponseFactory;
use Auth;
use Session;
use Carbon\Carbon;

class LTVMobileController extends Controller
{

    public function ltvAtualAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');


        $cliente_atual = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
        ->where('clientes.periodo_id', '=', $data_atual->month)
        ->where('clientes.ano_id', '=', $data_atual->year)
        ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
        ->value('clientes');

        $faturamento_atual = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', $data_atual->month)
        ->where('faturamentos.ano_id', '=', $data_atual->year)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');


        
        $media_retencao_clientes = DB::table('empresas')->where('empresas.usuario_id', $usuarioAutenticadoId)->select(DB::raw('empresas.media_retencao_clientes as media_retencao_clientes'))
        ->pluck('media_retencao_clientes')->first();



       //dd($media_retencao_clientes);

        if ($cliente_atual != 0)
        {
            $ltv_pagina = ($faturamento_atual / $cliente_atual) * $media_retencao_clientes;
        }
        else
        {
            $ltv_pagina = 0;
        }




        return response()->json([
            'ltv_pagina' => number_format($ltv_pagina,2)
        ]);

    }



    //LTV ATUAL

    public function ltvPaginaAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        
        
        $cliente_atual = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
        ->where('clientes.periodo_id', '=', $data_atual->month)
        ->where('clientes.ano_id', '=', $data_atual->year)
        ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
        ->value('clientes');

        $faturamento_atual = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', $data_atual->month)
        ->where('faturamentos.ano_id', '=', $data_atual->year)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');

        
        
        $media_retencao_clientes = DB::table('empresas')->where('empresas.usuario_id', $usuarioAutenticadoId)->select(DB::raw('empresas.media_retencao_clientes as media_retencao_clientes'))
        ->pluck('media_retencao_clientes')->first();



       //dd($media_retencao_clientes);

        if ($cliente_atual != 0)
        {
            $ltv_pagina = ($faturamento_atual / $cliente_atual) * $media_retencao_clientes;
        }
        else
        {
            $ltv_pagina = 0;
        }

        return $ltv_pagina;


    }




    //LTV DEZEMBRO DO ANO PASSADO

    public function ltvDezembroAnoPassadoAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

          

        $cliente_dezembro_ano_passado = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
        ->where('clientes.periodo_id', '=', 12)
        ->where('clientes.ano_id', '=', $data_atual->year - 1)
        ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
        ->value('clientes');
        

        $faturamento_dezembro_ano_passado = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', 12)
        ->where('faturamentos.ano_id', '=', $data_atual->year - 1)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');



       

        
        $media_retencao_clientes = DB::table('empresas')->where('empresas.usuario_id', $usuarioAutenticadoId)->select(DB::raw('empresas.media_retencao_clientes as media_retencao_clientes'))
        ->pluck('media_retencao_clientes')->first();

       


    if ($cliente_dezembro_ano_passado != 0)
    {
        $ltv_dezembro_ano_passado = ($faturamento_dezembro_ano_passado / $cliente_dezembro_ano_passado) * $media_retencao_clientes;
    }
    else
    {
        $ltv_dezembro_ano_passado = 0;
    }

    return $ltv_dezembro_ano_passado;




    }


    //LTV JANEIRO

    public function ltvJaneiroAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        

        $cliente_janeiro = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
        ->where('clientes.periodo_id', '=', 1)
        ->where('clientes.ano_id', '=', $data_atual->year)
        ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
        ->value('clientes');
        

        $faturamento_janeiro = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', 1)
        ->where('faturamentos.ano_id', '=', $data_atual->year)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');



       

        
            $media_retencao_clientes = DB::table('empresas')->where('empresas.usuario_id', $usuarioAutenticadoId)->select(DB::raw('empresas.media_retencao_clientes as media_retencao_clientes'))
            ->pluck('media_retencao_clientes')->first();
    
           


    if ($cliente_janeiro != 0)
    {
        $ltv_janeiro = ($faturamento_janeiro / $cliente_janeiro) * $media_retencao_clientes;
    }
    else
    {
        $ltv_janeiro = 0;
    }

    return $ltv_janeiro;




    }




      //LTV FEVEREIRO

      public function ltvFevereiroAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        

        $cliente_fevereiro = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
        ->where('clientes.periodo_id', '=', 2)
        ->where('clientes.ano_id', '=', $data_atual->year)
        ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
        ->value('clientes');
        

        $faturamento_fevereiro = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', 2)
        ->where('faturamentos.ano_id', '=', $data_atual->year)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');



       
            $media_retencao_clientes = DB::table('empresas')->where('empresas.usuario_id', $usuarioAutenticadoId)->select(DB::raw('empresas.media_retencao_clientes as media_retencao_clientes'))
            ->pluck('media_retencao_clientes')->first();
    
         


    if ($cliente_fevereiro != 0)
    {
        $ltv_fevereiro = ($faturamento_fevereiro / $cliente_fevereiro) * $media_retencao_clientes;
    }
    else
    {
        $ltv_fevereiro = 0;
    }

    return $ltv_fevereiro;





    }





    //LTV MARÇO

    public function ltvMarcoAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

       

        $cliente_marco = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
        ->where('clientes.periodo_id', '=', 3)
        ->where('clientes.ano_id', '=', $data_atual->year)
        ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
        ->value('clientes');
        

        $faturamento_marco = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', 3)
        ->where('faturamentos.ano_id', '=', $data_atual->year)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');



       

        
            $media_retencao_clientes = DB::table('empresas')->where('empresas.usuario_id', $usuarioAutenticadoId)->select(DB::raw('empresas.media_retencao_clientes as media_retencao_clientes'))
            ->pluck('media_retencao_clientes')->first();
    
         


    if ($cliente_marco != 0)
    {
        $ltv_marco = ($faturamento_marco / $cliente_marco) * $media_retencao_clientes;
    }
    else
    {
        $ltv_marco = 0;
    }

    return $ltv_marco;





    }








    //LTV ABRIL

    public function ltvAbrilAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');


        
        $cliente_abril = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
        ->where('clientes.periodo_id', '=', 4)
        ->where('clientes.ano_id', '=', $data_atual->year)
        ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
        ->value('clientes');
        

        $faturamento_abril = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', 4)
        ->where('faturamentos.ano_id', '=', $data_atual->year)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');




        
            $media_retencao_clientes = DB::table('empresas')->where('empresas.usuario_id', $usuarioAutenticadoId)->select(DB::raw('empresas.media_retencao_clientes as media_retencao_clientes'))
            ->pluck('media_retencao_clientes')->first();
    


    if ($cliente_abril != 0)
    {
        $ltv_abril = ($faturamento_abril / $cliente_abril) * $media_retencao_clientes;
    }
    else
    {
        $ltv_abril = 0;
    }

    return $ltv_abril;



    }






     //LTV MAIO

     public function ltvMaioAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

         

        $cliente_maio = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
        ->where('clientes.periodo_id', '=', 5)
        ->where('clientes.ano_id', '=', $data_atual->year)
        ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
        ->value('clientes');
        

        $faturamento_maio = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', 5)
        ->where('faturamentos.ano_id', '=', $data_atual->year)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');



        

        
            $media_retencao_clientes = DB::table('empresas')->where('empresas.usuario_id', $usuarioAutenticadoId)->select(DB::raw('empresas.media_retencao_clientes as media_retencao_clientes'))
            ->pluck('media_retencao_clientes')->first();
    
            

    if ($cliente_maio != 0)
    {
        $ltv_maio = ($faturamento_maio / $cliente_maio) * $media_retencao_clientes;
    }
    else
    {
        $ltv_maio = 0;
    }

    return $ltv_maio;



    }







     //LTV JUNHO

     public function ltvJunhoAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

         
        $cliente_junho = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
        ->where('clientes.periodo_id', '=', 6)
        ->where('clientes.ano_id', '=', $data_atual->year)
        ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
        ->value('clientes');
        

        $faturamento_junho = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', 6)
        ->where('faturamentos.ano_id', '=', $data_atual->year)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');




        
            $media_retencao_clientes = DB::table('empresas')->where('empresas.usuario_id', $usuarioAutenticadoId)->select(DB::raw('empresas.media_retencao_clientes as media_retencao_clientes'))
            ->pluck('media_retencao_clientes')->first();
    
            

    if ($cliente_junho != 0)
    {
        $ltv_junho = ($faturamento_junho / $cliente_junho) * $media_retencao_clientes;
    }
    else
    {
        $ltv_junho = 0;
    }

    return $ltv_junho;



    }











    //LTV JULHO

    public function ltvJulhoAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

         

        $cliente_julho = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
        ->where('clientes.periodo_id', '=', 7)
        ->where('clientes.ano_id', '=', $data_atual->year)
        ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
        ->value('clientes');
        

        $faturamento_julho = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', 7)
        ->where('faturamentos.ano_id', '=', $data_atual->year)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');



        
            $media_retencao_clientes = DB::table('empresas')->where('empresas.usuario_id', $usuarioAutenticadoId)->select(DB::raw('empresas.media_retencao_clientes as media_retencao_clientes'))
            ->pluck('media_retencao_clientes')->first();
    
           

    if ($cliente_julho != 0)
    {
        $ltv_julho = ($faturamento_julho / $cliente_julho) * $media_retencao_clientes;
    }
    else
    {
        $ltv_julho = 0;
    }

    return $ltv_julho;



    }





    //LTV AGOSTO

    public function ltvAgostoAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        

        $cliente_agosto = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
        ->where('clientes.periodo_id', '=', 8)
        ->where('clientes.ano_id', '=', $data_atual->year)
        ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
        ->value('clientes');
        

        $faturamento_agosto = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', 8)
        ->where('faturamentos.ano_id', '=', $data_atual->year)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');




        
            $media_retencao_clientes = DB::table('empresas')->where('empresas.usuario_id', $usuarioAutenticadoId)->select(DB::raw('empresas.media_retencao_clientes as media_retencao_clientes'))
            ->pluck('media_retencao_clientes')->first();
    
           

    if ($cliente_agosto != 0)
    {
        $ltv_agosto = ($faturamento_agosto / $cliente_agosto) * $media_retencao_clientes;
    }
    else
    {
        $ltv_agosto = 0;
    }

    return $ltv_agosto;



    }






      //LTV SETEMBRO

      public function ltvSetembroAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

       

        $cliente_setembro = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
        ->where('clientes.periodo_id', '=', 9)
        ->where('clientes.ano_id', '=', $data_atual->year)
        ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
        ->value('clientes');
        

        $faturamento_setembro = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', 9)
        ->where('faturamentos.ano_id', '=', $data_atual->year)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');





        
            $media_retencao_clientes = DB::table('empresas')->where('empresas.usuario_id', $usuarioAutenticadoId)->select(DB::raw('empresas.media_retencao_clientes as media_retencao_clientes'))
            ->pluck('media_retencao_clientes')->first();
    
            

    if ($cliente_setembro != 0)
    {
        $ltv_setembro = ($faturamento_setembro / $cliente_setembro) * $media_retencao_clientes;
    }
    else
    {
        $ltv_setembro = 0;
    }

    return $ltv_setembro;



    }






     //LTV OUTUBRO

     public function ltvOutubroAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

          

        $cliente_outubro = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
        ->where('clientes.periodo_id', '=', 10)
        ->where('clientes.ano_id', '=', $data_atual->year)
        ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
        ->value('clientes');
        

        $faturamento_outubro = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', 10)
        ->where('faturamentos.ano_id', '=', $data_atual->year)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');




        
            $media_retencao_clientes = DB::table('empresas')->where('empresas.usuario_id', $usuarioAutenticadoId)->select(DB::raw('empresas.media_retencao_clientes as media_retencao_clientes'))
            ->pluck('media_retencao_clientes')->first();
    
            


    if ($cliente_outubro != 0)
    {
        $ltv_outubro = ($faturamento_outubro / $cliente_outubro) * $media_retencao_clientes;
    }
    else
    {
        $ltv_outubro = 0;
    }

    return $ltv_outubro;



    }



    //LTV NOVEMBRO

    public function ltvNovembroAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

       
        

        $cliente_novembro = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
        ->where('clientes.periodo_id', '=', 11)
        ->where('clientes.ano_id', '=', $data_atual->year)
        ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
        ->value('clientes');
        

        $faturamento_novembro = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', 11)
        ->where('faturamentos.ano_id', '=', $data_atual->year)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');



        
            $media_retencao_clientes = DB::table('empresas')->where('empresas.usuario_id', $usuarioAutenticadoId)->select(DB::raw('empresas.media_retencao_clientes as media_retencao_clientes'))
            ->pluck('media_retencao_clientes')->first();
    
            

    if ($cliente_novembro != 0)
    {
        $ltv_novembro = ($faturamento_novembro / $cliente_novembro) * $media_retencao_clientes;
    }
    else
    {
        $ltv_novembro = 0;
    }

    return $ltv_novembro;



    }



     //LTV DEZEMBRO

     public function ltvDezembroAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        

        $cliente_dezembro = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
        ->where('clientes.periodo_id', '=', 12)
        ->where('clientes.ano_id', '=', $data_atual->year)
        ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
        ->value('clientes');
        

        $faturamento_dezembro = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', 12)
        ->where('faturamentos.ano_id', '=', $data_atual->year)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');



        
            $media_retencao_clientes = DB::table('empresas')->where('empresas.usuario_id', $usuarioAutenticadoId)->select(DB::raw('empresas.media_retencao_clientes as media_retencao_clientes'))
            ->pluck('media_retencao_clientes')->first();
    
         


    if ($cliente_dezembro != 0)
    {
        $ltv_dezembro = ($faturamento_dezembro / $cliente_dezembro) * $media_retencao_clientes;
    }
    else
    {
        $ltv_dezembro = 0;
    }

    return $ltv_dezembro;



    }









    public function paginaLtvAPI(Request $request)
    {

        

        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $data_atual = Carbon::now();
         

        //DADOS DO CAC ATUAL
        $cac_comparacao = app('App\Http\Controllers\CACMobileController')->cacPaginaAPI($request);

        //LTV ATUAL
        $ltv_pagina= $this->ltvPaginaAPI($request);

        //LTV DEZEMBRO DO ANO PASSADO
        $ltv_dezembro_ano_passado = $this->ltvDezembroAnoPassadoAPI($request);

        //LTV JANEIRO
        $ltv_janeiro = $this->ltvJaneiroAPI($request);

        //LTV FEVEREIRO
        $ltv_fevereiro = $this->ltvFevereiroAPI($request);


        //LTV MARÇO
        $ltv_marco = $this->ltvMarcoAPI($request);

        //LTV ABRIL
        $ltv_abril = $this->ltvAbrilAPI($request);

        //LTV MAIO
        $ltv_maio = $this->ltvMaioAPI($request);

        //LTV JUNHO
        $ltv_junho = $this->ltvJunhoAPI($request);

        //LTV JULHO
        $ltv_julho = $this->ltvJulhoAPI($request);

        //LTV AGOSTO
        $ltv_agosto = $this->ltvAgostoAPI($request);

        //LTV SETEMBRO
        $ltv_setembro = $this->ltvSetembroAPI($request);

        //LTV OUTUBRO
        $ltv_outubro = $this->ltvOutubroAPI($request);

        //LTV NOVEMBRO
        $ltv_novembro = $this->ltvNovembroAPI($request);

        //LTV DEZEMRO
        $ltv_dezembro = $this->ltvDezembroAPI($request);
       

      
        //Comparação de Dados

        //Janeiro/Dezembro do Ano Passado
        

        if ($ltv_janeiro == 0 || $ltv_dezembro_ano_passado == 0)
        {
            $ltv_janeiro_dezembro = 0;
        }
        else
        {
            $ltv_janeiro_dezembro = (($ltv_janeiro / $ltv_dezembro_ano_passado) - 1) * 100;
        }

        //Fevereiro/Janeiro
        if ($ltv_fevereiro == 0 || $ltv_janeiro == 0)
        {
            $ltv_fevereiro_janeiro = 0;
        }
        else
        {
            $ltv_fevereiro_janeiro = (($ltv_fevereiro / $ltv_janeiro) - 1) * 100;
        }

        //Marco/Fevereiro
        if ($ltv_marco == 0 || $ltv_fevereiro == 0)
        {
            $ltv_marco_fevereiro = 0;
        }
        else
        {
            $ltv_marco_fevereiro = (($ltv_marco / $ltv_fevereiro) - 1) * 100;
        }

        //Abril/Marco
        if ($ltv_abril == 0 || $ltv_marco == 0)
        {
            $ltv_abril_marco = 0;
        }
        else
        {
            $ltv_abril_marco = (($ltv_abril / $ltv_marco) - 1) * 100;
        }

        //Maio/Abril
        if ($ltv_maio == 0 || $ltv_abril == 0)
        {
            $ltv_maio_abril = 0;
        }
        else
        {
            $ltv_maio_abril = (($ltv_maio / $ltv_abril) - 1) * 100;
        }

        //Junho/Maio
        if ($ltv_junho == 0 || $ltv_maio == 0)
        {
            $ltv_junho_maio = 0;
        }
        else
        {
            $ltv_junho_maio = (($ltv_junho / $ltv_maio) - 1) * 100;
        }

        //Julho/Junho
        if ($ltv_julho == 0 || $ltv_junho == 0)
        {
            $ltv_julho_junho = 0;
        }
        else
        {
            $ltv_julho_junho = (($ltv_julho / $ltv_junho) - 1) * 100;
        }

        //Agosto/Julho
        if ($ltv_agosto == 0 || $ltv_julho == 0)
        {
            $ltv_agosto_julho = 0;
        }
        else
        {
            $ltv_agosto_julho = (($ltv_agosto / $ltv_julho) - 1) * 100;
        }

        //Setembro/Agosto
        if ($ltv_setembro == 0 || $ltv_agosto == 0)
        {
            $ltv_setembro_agosto = 0;
        }
        else
        {
            $ltv_setembro_agosto = (($ltv_setembro / $ltv_agosto) - 1) * 100;
        }

        //Outubro/Setembro
        if ($ltv_outubro == 0 || $ltv_setembro == 0)
        {
            $ltv_outubro_setembro = 0;
        }
        else
        {
            $ltv_outubro_setembro = (($ltv_outubro / $ltv_setembro) - 1) * 100;
        }

        //Novembro/Outubro
        if ($ltv_novembro == 0 || $ltv_outubro == 0)
        {
            $ltv_novembro_outubro = 0;
        }
        else
        {
            $ltv_novembro_outubro = (($ltv_novembro / $ltv_outubro) - 1) * 100;
        }

        //Dezembro/Novembro
        if ($ltv_dezembro == 0 || $ltv_novembro == 0)
        {
            $ltv_dezembro_novembro = 0;
        }
        else
        {
            $ltv_dezembro_novembro = (($ltv_dezembro / $ltv_novembro) - 1) * 100;
        }

        if ($ltv_pagina == 0)
        {
            $valida_ltv = "Sem dados para comparar o LTV:CAC";
        }
        else if ($ltv_pagina >= 3 * ($cac_comparacao))
        {
            $valida_ltv = "Seu LTV está bom.";
        }
        else if ($ltv_pagina < 3 * ($cac_comparacao))
        {
            $valida_ltv = "Seu LTV está ruim.";
        }
        else if ($ltv_pagina == 0)
        {
            $valida_ltv = "Sem dados para comparar o LTV:CAC";
        }


       
        //,'usuario_autenticado_tipo_usuario','permissaoUsuarioLogadoLer','permissaoUsuarioLogadoEscrever'

        return response()->json([
            'ltv_pagina' => number_format($ltv_pagina,2),
            'valida_ltv' => $valida_ltv,
            'ltv_janeiro' => ($ltv_janeiro),
            'ltv_fevereiro' => ($ltv_fevereiro),
            'ltv_marco' => ($ltv_marco),
            'ltv_abril'=> ($ltv_abril),
            'ltv_maio' => ($ltv_maio),
            'ltv_junho' => ($ltv_junho),
            'ltv_julho' => ($ltv_julho),
            'ltv_agosto' => ($ltv_agosto),
            'ltv_setembro' => ($ltv_setembro),
            'ltv_outubro' => ($ltv_outubro),
            'ltv_novembro' => ($ltv_novembro),
            'ltv_dezembro' => ($ltv_dezembro),
        ]);


    }

    



    //DADOS DO LTV NO PERIODO PESQUISA

    public function ltvPesquisaPeriodo(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $cliente_atual = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
            ->where('clientes.periodo_id', '=', $periodo_id)
            ->where('clientes.ano_id', '=', $ano_id)
            ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)
            ->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
            ->value('clientes');

            $faturamento_atual = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
            ->where('faturamentos.periodo_id', '=', $periodo_id)->where('faturamentos.ano_id', '=', $ano_id)->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)
            ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)
            ->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
            ->value('valor_faturamento');

            $media_retencao_clientes = DB::table('empresas')
            ->where('empresas.usuario_id', $usuarioAutenticadoId)
            ->select(DB::raw('empresas.media_retencao_clientes as media_retencao_clientes'))
            ->value('media_retencao_clientes');


        if ($cliente_atual != 0)
        {
            $ltv_pagina = ($faturamento_atual / $cliente_atual) * $media_retencao_clientes;
        }
        else
        {
            $ltv_pagina = 0;
        }

        return $ltv_pagina;


    }


    //DADOS DO LTV NO PERIODO DE JANEIRO
    public function ltvPesquisaPeriodoJaneiro(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $cliente_janeiro = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
            ->where('clientes.periodo_id', '=', 1)
            ->where('clientes.ano_id', '=', $ano_id)
            ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)
            
            
            ->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
            ->value('clientes');

        $faturamento_janeiro = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
            ->where('faturamentos.periodo_id', '=', 1)->where('faturamentos.ano_id', '=', $ano_id)
            ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)
            
            
            ->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
            ->value('valor_faturamento');

            $media_retencao_clientes = DB::table('empresas')
            ->where('empresas.usuario_id', $usuarioAutenticadoId)
            ->select(DB::raw('empresas.media_retencao_clientes as media_retencao_clientes'))
            ->value('media_retencao_clientes');


        if ($cliente_janeiro != 0)
        {
            $ltv_janeiro = ($faturamento_janeiro / $cliente_janeiro) * $media_retencao_clientes;
        }
        else
        {
            $ltv_janeiro = 0;
        }

        return $ltv_janeiro;


    }


    //DADOS DO LTV NO PERIODO DE FEVEREIRO

    public function ltvPesquisaPeriodoFevereiro(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $cliente_fevereiro = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
            ->where('clientes.periodo_id', '=', 2)
            ->where('clientes.ano_id', '=', $ano_id)
            ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)
            
            
            ->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
            ->value('clientes');

        $faturamento_fevereiro = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
            ->where('faturamentos.periodo_id', '=', 2)->where('faturamentos.ano_id', '=', $ano_id)
            ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)
            
            
            ->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
            ->value('valor_faturamento');

            $media_retencao_clientes = DB::table('empresas')
            ->where('empresas.usuario_id', $usuarioAutenticadoId)
            ->select(DB::raw('empresas.media_retencao_clientes as media_retencao_clientes'))
            ->value('media_retencao_clientes');


        if ($cliente_fevereiro != 0)
        {
            $ltv_fevereiro = ($faturamento_fevereiro / $cliente_fevereiro) * $media_retencao_clientes;
        }
        else
        {
            $ltv_fevereiro = 0;
        }

        return $ltv_fevereiro;


    }



    //DADOS DO LTV NO PERIODO DE MARÇO

    public function ltvPesquisaPeriodoMarco(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $cliente_marco = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
            ->where('clientes.periodo_id', '=', 3)
            ->where('clientes.ano_id', '=', $ano_id)
            ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)
            
            
            ->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
            ->value('clientes');

        $faturamento_marco = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
            ->where('faturamentos.periodo_id', '=', 3)->where('faturamentos.ano_id', '=', $ano_id)
            ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)
            
            ->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
            ->value('valor_faturamento');

            $media_retencao_clientes = DB::table('empresas')
            ->where('empresas.usuario_id', $usuarioAutenticadoId)
            
            ->select(DB::raw('empresas.media_retencao_clientes as media_retencao_clientes'))
            ->value('media_retencao_clientes');


        if ($cliente_marco != 0)
        {
            $ltv_marco = ($faturamento_marco / $cliente_marco) * $media_retencao_clientes;
        }
        else
        {
            $ltv_marco = 0;
        }

        return $ltv_marco;


    }






    //DADOS DO LTV NO PERIODO DE ABRIL

    public function ltvPesquisaPeriodoAbril(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $cliente_abril = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
            ->where('clientes.periodo_id', '=', 4)
            ->where('clientes.ano_id', '=', $ano_id)
            ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)
            
            
            ->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
            ->value('clientes');

        $faturamento_abril = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
            ->where('faturamentos.periodo_id', '=', 4)->where('faturamentos.ano_id', '=', $ano_id)
            ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)
            
            ->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
            ->value('valor_faturamento');

            $media_retencao_clientes = DB::table('empresas')
            ->where('empresas.usuario_id', $usuarioAutenticadoId)
            
            ->select(DB::raw('empresas.media_retencao_clientes as media_retencao_clientes'))
            ->value('media_retencao_clientes');


        if ($cliente_abril != 0)
        {
            $ltv_abril = ($faturamento_abril / $cliente_abril) * $media_retencao_clientes;
        }
        else
        {
            $ltv_abril = 0;
        }

        return $ltv_abril;


    }






     //DADOS DO LTV NO PERIODO DE MAIO

     public function ltvPesquisaPeriodoMaio(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $cliente_maio = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
            ->where('clientes.periodo_id', '=', 5)
            ->where('clientes.ano_id', '=', $ano_id)
            ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)
            
            
            ->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
            ->value('clientes');

        $faturamento_maio = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
            ->where('faturamentos.periodo_id', '=', 5)->where('faturamentos.ano_id', '=', $ano_id)
            ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)
            
            ->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
            ->value('valor_faturamento');

            $media_retencao_clientes = DB::table('empresas')
            ->where('empresas.usuario_id', $usuarioAutenticadoId)
            
            ->select(DB::raw('empresas.media_retencao_clientes as media_retencao_clientes'))
            ->value('media_retencao_clientes');

        if ($cliente_maio != 0)
        {
            $ltv_maio = ($faturamento_maio / $cliente_maio) * $media_retencao_clientes;
        }
        else
        {
            $ltv_maio = 0;
        }

        return $ltv_maio;


    }



      //DADOS DO LTV NO PERIODO DE JUNHO

      public function ltvPesquisaPeriodoJunho(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $cliente_junho = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
            ->where('clientes.periodo_id', '=', 6)
            ->where('clientes.ano_id', '=', $ano_id)
            ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)
            
            
            ->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
            ->value('clientes');

        $faturamento_junho = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
            ->where('faturamentos.periodo_id', '=', 6)->where('faturamentos.ano_id', '=', $ano_id)
            ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)
            
            ->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
            ->value('valor_faturamento');

            $media_retencao_clientes = DB::table('empresas')
            ->where('empresas.usuario_id', $usuarioAutenticadoId)
            
            ->select(DB::raw('empresas.media_retencao_clientes as media_retencao_clientes'))
            ->value('media_retencao_clientes');

        if ($cliente_junho != 0)
        {
            $ltv_junho = ($faturamento_junho / $cliente_junho) * $media_retencao_clientes;
        }
        else
        {
            $ltv_junho = 0;
        }

        return $ltv_junho;


    }



      //DADOS DO LTV NO PERIODO DE JULHO

      public function ltvPesquisaPeriodoJulho(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $cliente_julho = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
            ->where('clientes.periodo_id', '=', 7)
            ->where('clientes.ano_id', '=', $ano_id)
            ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)
            
            
            ->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
            ->value('clientes');

        $faturamento_julho = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
            ->where('faturamentos.periodo_id', '=', 7)->where('faturamentos.ano_id', '=', $ano_id)
            ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)
            
            ->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
            ->value('valor_faturamento');

            $media_retencao_clientes = DB::table('empresas')
            ->where('empresas.usuario_id', $usuarioAutenticadoId)
            
            ->select(DB::raw('empresas.media_retencao_clientes as media_retencao_clientes'))
            ->value('media_retencao_clientes');

        if ($cliente_julho != 0)
        {
            $ltv_julho = ($faturamento_julho / $cliente_julho) * $media_retencao_clientes;
        }
        else
        {
            $ltv_julho = 0;
        }

        return $ltv_julho;


    }





      //DADOS DO LTV NO PERIODO DE AGOSTO

      public function ltvPesquisaPeriodoAgosto(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $cliente_agosto = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
            ->where('clientes.periodo_id', '=', 8)
            ->where('clientes.ano_id', '=', $ano_id)
            ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)
            
            
            ->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
            ->value('clientes');

        $faturamento_agosto = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
            ->where('faturamentos.periodo_id', '=', 8)->where('faturamentos.ano_id', '=', $ano_id)
            ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)
            
            ->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
            ->value('valor_faturamento');

            $media_retencao_clientes = DB::table('empresas')
            ->where('empresas.usuario_id', $usuarioAutenticadoId)
            
            ->select(DB::raw('empresas.media_retencao_clientes as media_retencao_clientes'))
            ->value('media_retencao_clientes');

        if ($cliente_agosto != 0)
        {
            $ltv_agosto = ($faturamento_agosto / $cliente_agosto) * $media_retencao_clientes;
        }
        else
        {
            $ltv_agosto = 0;
        }

        return $ltv_agosto;


    }





      //DADOS DO LTV NO PERIODO DE SETEMBRO

      public function ltvPesquisaPeriodoSetembro(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $cliente_setembro = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
            ->where('clientes.periodo_id', '=', 9)
            ->where('clientes.ano_id', '=', $ano_id)
            ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)
            
            
            ->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
            ->value('clientes');

        $faturamento_setembro = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
            ->where('faturamentos.periodo_id', '=', 9)->where('faturamentos.ano_id', '=', $ano_id)
            ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)
            
            
            ->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
            ->value('valor_faturamento');

            $media_retencao_clientes = DB::table('empresas')
            ->where('empresas.usuario_id', $usuarioAutenticadoId)
            
            ->select(DB::raw('empresas.media_retencao_clientes as media_retencao_clientes'))
            ->value('media_retencao_clientes');

        if ($cliente_setembro != 0)
        {
            $ltv_setembro = ($faturamento_setembro / $cliente_setembro) * $media_retencao_clientes;
        }
        else
        {
            $ltv_setembro = 0;
        }

        return $ltv_setembro;


    }




      //DADOS DO LTV NO PERIODO DE OUTUBRO

      public function ltvPesquisaPeriodoOutubro(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $cliente_outubro = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
            ->where('clientes.periodo_id', '=', 10)
            ->where('clientes.ano_id', '=', $ano_id)
            ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)
            
            
            ->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
            ->value('clientes');

        $faturamento_outubro = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
            ->where('faturamentos.periodo_id', '=', 10)->where('faturamentos.ano_id', '=', $ano_id)
            ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)
            
            ->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
            ->value('valor_faturamento');

            $media_retencao_clientes = DB::table('empresas')
            ->where('empresas.usuario_id', $usuarioAutenticadoId)
            
            ->select(DB::raw('empresas.media_retencao_clientes as media_retencao_clientes'))
            ->value('media_retencao_clientes');

        if ($cliente_outubro != 0)
        {
            $ltv_outubro = ($faturamento_outubro / $cliente_outubro) * $media_retencao_clientes;
        }
        else
        {
            $ltv_outubro = 0;
        }

        return $ltv_outubro;


    }


      //DADOS DO LTV NO PERIODO DE NOVEMBRO

      public function ltvPesquisaPeriodoNovembro(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $cliente_novembro = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
            ->where('clientes.periodo_id', '=', 11)
            ->where('clientes.ano_id', '=', $ano_id)
            ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)
            
            
            ->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
            ->value('clientes');

        $faturamento_novembro = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
            ->where('faturamentos.periodo_id', '=', 11)->where('faturamentos.ano_id', '=', $ano_id)
            ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)
            
            ->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
            ->value('valor_faturamento');

            $media_retencao_clientes = DB::table('empresas')
            ->where('empresas.usuario_id', $usuarioAutenticadoId)
            
            ->select(DB::raw('empresas.media_retencao_clientes as media_retencao_clientes'))
            ->value('media_retencao_clientes');

        if ($cliente_novembro != 0)
        {
            $ltv_novembro = ($faturamento_novembro / $cliente_novembro) * $media_retencao_clientes;
        }
        else
        {
            $ltv_novembro = 0;
        }

        return $ltv_novembro;


    }



      //DADOS DO LTV NO PERIODO DE DEZEMBRO

      public function ltvPesquisaPeriodoDezembro(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $cliente_dezembro = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
            ->where('clientes.periodo_id', '=', 12)
            ->where('clientes.ano_id', '=', $ano_id)
            ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)
            
            
            ->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
            ->value('clientes');

        $faturamento_dezembro = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
            ->where('faturamentos.periodo_id', '=', 12)->where('faturamentos.ano_id', '=', $ano_id)
            ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)
            
            ->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
            ->value('valor_faturamento');

            $media_retencao_clientes = DB::table('empresas')
            ->where('empresas.usuario_id', $usuarioAutenticadoId)
            
            ->select(DB::raw('empresas.media_retencao_clientes as media_retencao_clientes'))
            ->value('media_retencao_clientes');

        if ($cliente_dezembro != 0)
        {
            $ltv_dezembro = ($faturamento_dezembro / $cliente_dezembro) * $media_retencao_clientes;
        }
        else
        {
            $ltv_dezembro = 0;
        }

        return $ltv_dezembro;


    }








    public function pesquisaLtv(Request $request)
    {

      

        $usuarioAutenticadoId = $request->session()->get('usuarioId');
      
      
        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $empresa_id = $request->empresa_id;

        //DADOS DO CAC ATUAL
        $cac_comparacao = app('App\Http\Controllers\CACMobileController')->cacPesquisaPeriodo($request);

        //LTV ATUAL
        $ltv = $this->ltvPesquisaPeriodo($request);

        //LTV JANEIRO
        $ltv_janeiro = $this->ltvPesquisaPeriodoJaneiro($request);
        
        //LTV FEVEREIRO
        $ltv_fevereiro = $this->ltvPesquisaPeriodoFevereiro($request);

        //LTV MARÇO
        $ltv_marco = $this->ltvPesquisaPeriodoMarco($request);


        //LTV ABRIL
        $ltv_abril = $this->ltvPesquisaPeriodoAbril($request);

        //LTV MAIO
        $ltv_maio = $this->ltvPesquisaPeriodoMaio($request);

        //LTV JUNHO
        $ltv_junho = $this->ltvPesquisaPeriodoJunho($request);


        //LTV JULHO
        $ltv_julho = $this->ltvPesquisaPeriodoJulho($request);

        
        //LTV AGOSTO
       $ltv_agosto = $this->ltvPesquisaPeriodoAgosto($request);

        //LTV SETEMBRO
       $ltv_setembro = $this->ltvPesquisaPeriodoSetembro($request);
        
        //LTV OUTUBRO
       $ltv_outubro = $this->ltvPesquisaPeriodoOutubro($request);
        
        //LTV NOVEMBRO
        $ltv_novembro = $this->ltvPesquisaPeriodoNovembro($request);

        //LTV DEZEMBRO
        $ltv_dezembro = $this->ltvPesquisaPeriodoDezembro($request);


      

        if ($ltv == 0)
        {
            $valida_ltv = "Sem dados para avaliar o LTV";
        }
        else if ($ltv >= 3 * ($cac_comparacao))
        {
            $valida_ltv = "Seu LTV está bom.";
        }
        else if ($ltv < 3 * ($cac_comparacao))
        {
            $valida_ltv = "Seu LTV está ruim.";
        }
        else if ($ltv == 0)
        {
            $valida_ltv = "Sem dados para comparar o LTV";
        }



        return response()->json([
            'ltv' => number_format($ltv,2),
            'valida_ltv' => $valida_ltv,
            'ltv_janeiro' => $ltv_janeiro,
            'ltv_fevereiro' => $ltv_fevereiro,
            'ltv_marco' => $ltv_marco,
            'ltv_abril' => $ltv_abril,
            'ltv_maio' => $ltv_maio,
            'ltv_junho' => $ltv_junho,
            'ltv_julho' => $ltv_julho,
            'ltv_agosto' => $ltv_agosto,
            'ltv_setembro' => $ltv_setembro,
            'ltv_outubro' => $ltv_outubro,
            'ltv_novembro' => $ltv_novembro,
            'ltv_dezembro' => $ltv_dezembro,


        ]);
       




    }








}