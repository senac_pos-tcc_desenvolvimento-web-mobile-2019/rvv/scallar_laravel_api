<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\NetPromoterScore;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Routing\ResponseFactory;
use Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class DadosNPSController extends Controller
{



    public function cadastrarDadosNPSAPI(Request $request){

        $usuario_autenticado_id = $request->session()->get('usuarioId');


        $dadosnps = NetPromoterScore::create([

            'promotores' => $request['promotores'],
            'detratores' => $request['detratores'],
            'neutros' => $request['neutros'],
            'periodo_id' => $request['periodo_id'],
            'ano_id' => $request['ano_id'],
            'empresa_id' => $request['empresa_id'],
            'usuario_id' => $usuario_autenticado_id,

        ]);


        if($dadosnps){

            return response()->json(
                'Dados do NPS cadastrado com sucesso'
            );

        }else{

            return response()->json(
                'Erro ao salvar os Dados do NPS'
            );

        }


    }




    public function editarDadosNPSAPI(Request $request, $id){

        $dados = $request->all();

        $dadoNPSlId = NetPromoterScore::find($id);

        $alt = $dadoNPSlId->update($dados);

        if ($alt)
        {

            return response()->json(
                'Dados do NPS atualizado'
            );

        }else{

            return response()->json(
                'Erro ao atualizar os Dados do NPS'
            );


        }



    }





    public function excluirDadosNPSAPI($id){

        $dadosnps = NetPromoterScore::find($id);

        if ($dadosnps->delete()){
                return response()->json(
                'Dados do NPS deletado'
                );

        }else{
            return response()->json(
                'Erro ao excluir os Dados do NPS'
            );
        }

    }



    public function listarDadosNPSAPI(Request $request){

        $usuario_autenticado_id = $request->session()->get('usuarioId');


        $dadosnps = NetPromoterScore::where('usuario_id', $usuario_autenticado_id)->orderBy('ano_id', 'desc')
        ->orderBy('periodo_id', 'desc')
        ->paginate(4);


        return response()->json($dadosnps);


    }



    public function graficosDadosNPSAPI(Request $request){


        $usuario_autenticado_id = $request->session()->get('usuarioId');
        $data_atual = Carbon::now();

        $promotores_janeiro = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 1)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)->select(DB::raw('SUM(net_promoter_scores.promotores) as promotores'))
        ->sum('promotores');

        $detratores_janeiro = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 1)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)->select(DB::raw('SUM(net_promoter_scores.detratores) as detratores'))
        ->sum('detratores');

        $neutros_janeiro = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 1)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)->select(DB::raw('SUM(net_promoter_scores.neutros) as neutros'))
        ->sum('neutros');





        $promotores_fevereiro = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 2)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)->select(DB::raw('SUM(net_promoter_scores.promotores) as promotores'))
        ->sum('promotores');

        $detratores_fevereiro = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 2)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)->select(DB::raw('SUM(net_promoter_scores.detratores) as detratores'))
        ->sum('detratores');

        $neutros_fevereiro = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 2)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)->select(DB::raw('SUM(net_promoter_scores.neutros) as neutros'))
        ->sum('neutros');







        $promotores_marco = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 3)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)->select(DB::raw('SUM(net_promoter_scores.promotores) as promotores'))
        ->sum('promotores');

        $detratores_marco = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 3)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)->select(DB::raw('SUM(net_promoter_scores.detratores) as detratores'))
        ->sum('detratores');

        $neutros_marco = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 3)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)->select(DB::raw('SUM(net_promoter_scores.neutros) as neutros'))
        ->sum('neutros');









        $promotores_abril = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 4)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)->select(DB::raw('SUM(net_promoter_scores.promotores) as promotores'))
        ->sum('promotores');

        $detratores_abril = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 4)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)->select(DB::raw('SUM(net_promoter_scores.detratores) as detratores'))
        ->sum('detratores');

        $neutros_abril = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 4)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)->select(DB::raw('SUM(net_promoter_scores.neutros) as neutros'))
        ->sum('neutros');











        $promotores_maio = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 5)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)->select(DB::raw('SUM(net_promoter_scores.promotores) as promotores'))
        ->sum('promotores');

        $detratores_maio = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 5)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)->select(DB::raw('SUM(net_promoter_scores.detratores) as detratores'))
        ->sum('detratores');

        $neutros_maio = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 5)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)->select(DB::raw('SUM(net_promoter_scores.neutros) as neutros'))
        ->sum('neutros');










        $promotores_junho = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 6)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)->select(DB::raw('SUM(net_promoter_scores.promotores) as promotores'))
        ->sum('promotores');

        $detratores_junho = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 6)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)->select(DB::raw('SUM(net_promoter_scores.detratores) as detratores'))
        ->sum('detratores');

        $neutros_junho = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 6)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)->select(DB::raw('SUM(net_promoter_scores.neutros) as neutros'))
        ->sum('neutros');








        $promotores_julho = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 7)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)->select(DB::raw('SUM(net_promoter_scores.promotores) as promotores'))
        ->sum('promotores');

        $detratores_julho = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 7)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)->select(DB::raw('SUM(net_promoter_scores.detratores) as detratores'))
        ->sum('detratores');

        $neutros_julho = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 7)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)->select(DB::raw('SUM(net_promoter_scores.neutros) as neutros'))
        ->sum('neutros');








        $promotores_agosto = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 8)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)->select(DB::raw('SUM(net_promoter_scores.promotores) as promotores'))
        ->sum('promotores');

        $detratores_agosto = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 8)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)->select(DB::raw('SUM(net_promoter_scores.detratores) as detratores'))
        ->sum('detratores');

        $neutros_agosto = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 8)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)->select(DB::raw('SUM(net_promoter_scores.neutros) as neutros'))
        ->sum('neutros');









        $promotores_setembro = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 9)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)->select(DB::raw('SUM(net_promoter_scores.promotores) as promotores'))
        ->sum('promotores');

        $detratores_setembro = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 9)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)->select(DB::raw('SUM(net_promoter_scores.detratores) as detratores'))
        ->sum('detratores');

        $neutros_setembro = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 9)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)->select(DB::raw('SUM(net_promoter_scores.neutros) as neutros'))
        ->sum('neutros');












        $promotores_outubro = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 10)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)->select(DB::raw('SUM(net_promoter_scores.promotores) as promotores'))
        ->sum('promotores');

        $detratores_outubro = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 10)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)->select(DB::raw('SUM(net_promoter_scores.detratores) as detratores'))
        ->sum('detratores');

        $neutros_outubro = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 10)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)->select(DB::raw('SUM(net_promoter_scores.neutros) as neutros'))
        ->sum('neutros');










        $promotores_novembro = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 11)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)->select(DB::raw('SUM(net_promoter_scores.promotores) as promotores'))
        ->sum('promotores');

        $detratores_novembro = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 11)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)->select(DB::raw('SUM(net_promoter_scores.detratores) as detratores'))
        ->sum('detratores');

        $neutros_novembro = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 11)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)->select(DB::raw('SUM(net_promoter_scores.neutros) as neutros'))
        ->sum('neutros');











        $promotores_dezembro = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 12)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)->select(DB::raw('SUM(net_promoter_scores.promotores) as promotores'))
        ->sum('promotores');

        $detratores_dezembro = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 12)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)->select(DB::raw('SUM(net_promoter_scores.detratores) as detratores'))
        ->sum('detratores');

        $neutros_dezembro = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 12)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)->select(DB::raw('SUM(net_promoter_scores.neutros) as neutros'))
        ->sum('neutros');









        return response()->json(array(

            $promotores_janeiro,
            $promotores_fevereiro,
            $promotores_marco,
            $promotores_abril,
            $promotores_maio,
            $promotores_junho,
            $promotores_julho,
            $promotores_agosto,
            $promotores_setembro,
            $promotores_outubro,
            $promotores_novembro,
            $promotores_dezembro,
            $detratores_janeiro,
            $detratores_fevereiro,
            $detratores_marco,
            $detratores_abril,
            $detratores_maio,
            $detratores_junho,
            $detratores_julho,
            $detratores_agosto,
            $detratores_setembro,
            $detratores_outubro,
            $detratores_novembro,
            $detratores_dezembro,
            $neutros_janeiro,
            $neutros_fevereiro,
            $neutros_marco,
            $neutros_abril,
            $neutros_maio,
            $neutros_junho,
            $neutros_julho,
            $neutros_agosto,
            $neutros_setembro,
            $neutros_outubro,
            $neutros_novembro,
            $neutros_dezembro



        ));





    }










}
