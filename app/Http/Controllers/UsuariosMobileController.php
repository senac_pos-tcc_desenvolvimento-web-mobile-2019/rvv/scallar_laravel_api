<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuario;
use App\Cargo;
use App\Empresa;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\Routing\ResponseFactory;
use Auth;
use Session;
use Carbon\Carbon;




class UsuariosMobileController extends Controller
{

    public function cadastrarUsuarioAPI(Request $request)
    {

        $usuarios = Usuario::create([
            'nome' => $request['nome'],
            'sobrenome' => $request['sobrenome'],
            'email' => $request['email'],
            'password' => bcrypt($request['password']),
            'telefone' => bcrypt($request['telefone']),
            'cargo_id' => $request['cargo_id'],
            'tipo_usuario_id' => $request['tipo_usuario_id'],


        ]);

        if ($usuarios) {

            return response()->json(
                'Usuário cadastrado com sucesso'
            );
        }
    }




    public function loginUsuarioAPI(Request $request)
    {

        $validator = validator($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {


            return response()->json(
                'Você deve digitar E-Mail e Senha !, Tente Novamente'
            );

        }

        $credentials = ['email' => $request->get('email'), 'password' => $request->get('password')];

        if (auth()->guard()->attempt($credentials)) {

            return response()->json(array(

                $usuario_autenticado_id = Auth::guard()->user()->id,
                $usuario_autenticado_nome = Auth::guard()->user()->nome,
                $usuario_autenticado_cargo = Auth::guard()->user()->cargo_id,

                $request->session()->put(['usuarioId' =>$usuario_autenticado_id,'usuarioNome' => $usuario_autenticado_nome, 'usuarioAutenticadoCargo' => $usuario_autenticado_cargo]),


                Session::save()


            ));


        } else {


            return response()->json(
                'E-Mail ou/e Senha Inválida(os) !, Tente Novamente'
            );


        }
    }

    public function perfilUsuarioLogadoAPI(Request $request){


        $usuarioAutenticadoNome = $request->session()->get('usuarioNome');
        $usuarioAutenticadoId = $request->session()->get('usuarioId');
        $usuarioAutenticadoCargo = $request->session()->get('usuarioAutenticadoCargo');

        $usuarioAutenticadoCargoNome = DB::table('usuarios')->join('cargos', 'cargos.id', '=', 'usuarios.cargo_id')
        ->where('cargos.id', '=', $usuarioAutenticadoCargo)
        ->selectRaw('cargos.nome_cargo as usuario_autenticado_cargo')
        ->value('usuario_autenticado_cargo');

        $data_atual = Carbon::now();

        $faturamentoMesAtual = DB::table('faturamentos')->where('usuario_id', $usuarioAutenticadoId)->where('periodo_id', $data_atual->month)
        ->where('ano_id', $data_atual->year)
        ->sum('valor_faturamento');

        $clientesMesAtual = DB::table('clientes')->where('usuario_id', $usuarioAutenticadoId)->where('periodo_id', $data_atual->month)
            ->where('ano_id', $data_atual->year)
            ->sum('numero_clientes');

        $verbaMarketingMesAtual = DB::table('verbas')->where('usuario_id', $usuarioAutenticadoId)->where('periodo_id', $data_atual->month)
            ->where('ano_id', $data_atual->year)
            ->sum('verba');

        $acoesMarketingMesAtual = DB::table('investimentos')->where('usuario_id', $usuarioAutenticadoId)->where('periodo_id', $data_atual->month)
            ->where('ano_id', $data_atual->year)
            ->sum('valor');

        $investimentosMesAtual = DB::table('total_investimentos')->where('usuario_id', $usuarioAutenticadoId)->where('periodo_id', $data_atual->month)
            ->where('ano_id', $data_atual->year)
            ->sum('valor');

        $despesasMesAtual = DB::table('despesas')->where('usuario_id', $usuarioAutenticadoId)->where('periodo_id', $data_atual->month)
            ->where('ano_id', $data_atual->year)
            ->sum('valor');

        $npsMesAtual = DB::table('net_promoter_scores')->where('usuario_id', $usuarioAutenticadoId)->where('periodo_id', $data_atual->month)
            ->where('ano_id', $data_atual->year)
            ->sum('promotores');




        return response()->json(array(

            'faturamento' => $faturamentoMesAtual,
            'clientes' => $clientesMesAtual,
            'verbas' => $verbaMarketingMesAtual,
            'acoes_marketing' => $acoesMarketingMesAtual,
            'investimento' => $investimentosMesAtual,
            'despesas' => $despesasMesAtual,
            'nps' => $npsMesAtual

        ));


    }


    public function empresaUsuarioLogadoAPI (Request $request){

        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $empresaUsuarioLogado = DB::table('empresas')->where('usuario_id', $usuarioAutenticadoId)->get('id');

        return response()->json(array(
            'empresaUsuarioLogado' => $empresaUsuarioLogado
        ));

    }




    public function editarUsuarioLogadoAPI(Request $request, $id){

        $dados = $request->all();

        $usuarioId = Usuario::find($id);

        $alt = $usuarioId->update($dados);

        if ($alt)
        {

            return response()->json(
                'Perfil atualizado'
            );

        }else{

            return response()->json(
                'Erro ao atualizar o perfil'
            );


        }



    }
















    public function informacoesUsuarioLogadoAPI(Request $request){


        $usuarioAutenticadoNome = $request->session()->get('usuarioNome');
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $nome = DB::table('usuarios')->where('id', $usuarioAutenticadoId)->value('nome');
        $sobrenome = DB::table('usuarios')->where('id', $usuarioAutenticadoId)->value('sobrenome');
        $email = DB::table('usuarios')->where('id', $usuarioAutenticadoId)->value('email');
        $telefone = DB::table('usuarios')->where('id', $usuarioAutenticadoId)->value('telefone');
        $usuarioAutenticadoCargo = DB::table('usuarios')->where('id', $usuarioAutenticadoId)->value('cargo_id');



        return response()->json(array(

            'nome' => $nome,
            'sobrenome' => $sobrenome,
            'email' => $email,
            'telefone' => $telefone,
            'cargo_id' => $usuarioAutenticadoCargo


        ));


    }









}
