<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Faturamento;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Routing\ResponseFactory;
use Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;


class FaturamentosMobileController extends Controller
{



    public function cadastrarFaturamentoAPI(Request $request){

        $usuario_autenticado_id = $request->session()->get('usuarioId');


        $faturamentos = Faturamento::create([

            'valor_faturamento' => $request['valor_faturamento'],
            'periodo_id' => $request['periodo_id'],
            'ano_id' => $request['ano_id'],
            'empresa_id' => $request['empresa_id'],
            'usuario_id' => $usuario_autenticado_id,

        ]);


        if($faturamentos){

            return response()->json(
                'Faturamento cadastrado com sucesso'
            );

        }else{

            return response()->json(
                'Erro ao salvar o faturamento'
            );

        } 


    }




    public function editarFaturamentoAPI(Request $request, $id){

        $dados = $request->all();

        $faturamentoId = Faturamento::find($id);

        $alt = $faturamentoId->update($dados);

        if ($alt)
        {

            return response()->json(
                'Faturamento atualizado'
            );

        }else{

            return response()->json(
                'Erro ao atualizar o faturamento'
            );
            

        }



    }





    public function excluirFaturamentoAPI($id){

        $faturamento = Faturamento::find($id);

        if ($faturamento->delete()){
                return response()->json(
                'Faturamento deletado'
                );

        }else{
            return response()->json(
                'Erro ao excluir o faturamento'
            );
        }
        
    }



    public function listarFaturamentoAPI(Request $request){

        $usuario_autenticado_id = $request->session()->get('usuarioId');


        $faturamentos = Faturamento::where('usuario_id', $usuario_autenticado_id)->orderBy('ano_id', 'desc')
        ->orderBy('periodo_id', 'desc')
        ->paginate(4);


        return response()->json($faturamentos);


    }



    public function graficosFaturamentoAPI(Request $request){

        $usuario_autenticado_id = $request->session()->get('usuarioId');
        $data_atual = Carbon::now();


        $faturamentosJaneiro = DB::table('faturamentos')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 1)
            ->where('ano_id', $data_atual->year)
            ->sum('valor_faturamento');

        $faturamentosFevereiro = DB::table('faturamentos')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 2)
            ->where('ano_id', $data_atual->year)
            ->sum('valor_faturamento');

        $faturamentosMarco = DB::table('faturamentos')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 3)
            ->where('ano_id', $data_atual->year)
            ->sum('valor_faturamento');

        $faturamentosAbril = DB::table('faturamentos')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 4)
            ->where('ano_id', $data_atual->year)
            ->sum('valor_faturamento');

        $faturamentosMaio = DB::table('faturamentos')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 5)
            ->where('ano_id', $data_atual->year)
            ->sum('valor_faturamento');

        $faturamentosJunho = DB::table('faturamentos')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 6)
            ->where('ano_id', $data_atual->year)
            ->sum('valor_faturamento');

        $faturamentosJulho = DB::table('faturamentos')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 7)
            ->where('ano_id', $data_atual->year)
            ->sum('valor_faturamento');

        $faturamentosAgosto = DB::table('faturamentos')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 8)
            ->where('ano_id', $data_atual->year)
            ->sum('valor_faturamento');

        $faturamentosSetembro = DB::table('faturamentos')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 9)
            ->where('ano_id', $data_atual->year)
            ->sum('valor_faturamento');

        $faturamentosOutubro = DB::table('faturamentos')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 10)
            ->where('ano_id', $data_atual->year)
            ->sum('valor_faturamento');

        $faturamentosNovembro = DB::table('faturamentos')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 11)
            ->where('ano_id', $data_atual->year)
            ->sum('valor_faturamento');

        $faturamentosDezembro = DB::table('faturamentos')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 12)
            ->where('ano_id', $data_atual->year)
            ->sum('valor_faturamento');



        return response()->json(array(

            $faturamentosJaneiro, 
            $faturamentosFevereiro, 
            $faturamentosMarco, 
            $faturamentosAbril, 
            $faturamentosMaio, 
            $faturamentosJunho, 
            $faturamentosJulho,
            $faturamentosAgosto,
            $faturamentosSetembro,
            $faturamentosOutubro,
            $faturamentosNovembro,
            $faturamentosDezembro,

        ));

    }












    
}
