<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Verba;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Routing\ResponseFactory;
use Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class VerbasMobileController extends Controller
{



    public function cadastrarVerbaAPI(Request $request){

        $usuario_autenticado_id = $request->session()->get('usuarioId');


        $verbas = Verba::create([

            'verba' => $request['verba'],
            'periodo_id' => $request['periodo_id'],
            'ano_id' => $request['ano_id'],
            'empresa_id' => $request['empresa_id'],
            'usuario_id' => $usuario_autenticado_id,

        ]);


        if($verbas){

            return response()->json(
                'Verba de marketing cadastrada com sucesso'
            );

        }else{

            return response()->json(
                'Erro ao salvar a verba de marketing'
            );

        } 


    }




    public function editarVerbaAPI(Request $request, $id){

        $dados = $request->all();

        $verbaId = Verba::find($id);

        $alt = $verbaId->update($dados);

        if ($alt)
        {

            return response()->json(
                'Verba de marketing atualizada'
            );

        }else{

            return response()->json(
                'Erro ao atualizar a verba de marketing'
            );
            

        }



    }





    public function excluirVerbaAPI($id){

        $verba = Verba::find($id);

        if ($verba->delete()){
                return response()->json(
                'Verba de marketing deletada'
                );

        }else{
            return response()->json(
                'Erro ao excluir a verba de marketing'
            );
        }
        
    }



    public function listarVerbaAPI(Request $request){

        $usuario_autenticado_id = $request->session()->get('usuarioId');

        $verbas = Verba::where('usuario_id', $usuario_autenticado_id)->orderBy('ano_id', 'desc')
        ->orderBy('periodo_id', 'desc')
        ->paginate(4);


        return response()->json($verbas);


    }



    public function graficosVerbaAPI(Request $request){

        $usuario_autenticado_id = $request->session()->get('usuarioId');
        $data_atual = Carbon::now();


        $verbasJaneiro = DB::table('verbas')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 1)
            ->where('ano_id', $data_atual->year)
            ->sum('verba');

        $verbasFevereiro = DB::table('verbas')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 2)
            ->where('ano_id', $data_atual->year)
            ->sum('verba');

        $verbasMarco = DB::table('verbas')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 3)
            ->where('ano_id', $data_atual->year)
            ->sum('verba');

        $verbasAbril = DB::table('verbas')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 4)
            ->where('ano_id', $data_atual->year)
            ->sum('verba');

        $verbasMaio = DB::table('verbas')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 5)
            ->where('ano_id', $data_atual->year)
            ->sum('verba');

        $verbasJunho = DB::table('verbas')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 6)
            ->where('ano_id', $data_atual->year)
            ->sum('verba');

        $verbasJulho = DB::table('verbas')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 7)
            ->where('ano_id', $data_atual->year)
            ->sum('verba');

        $verbasAgosto = DB::table('verbas')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 8)
            ->where('ano_id', $data_atual->year)
            ->sum('verba');

        $verbasSetembro = DB::table('verbas')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 9)
            ->where('ano_id', $data_atual->year)
            ->sum('verba');

        $verbasOutubro = DB::table('verbas')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 10)
            ->where('ano_id', $data_atual->year)
            ->sum('verba');

        $verbasNovembro = DB::table('verbas')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 11)
            ->where('ano_id', $data_atual->year)
            ->sum('verba');

        $verbasDezembro = DB::table('verbas')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 12)
            ->where('ano_id', $data_atual->year)
            ->sum('verba');


            return response()->json(array(

                $verbasJaneiro, 
                $verbasFevereiro, 
                $verbasMarco, 
                $verbasAbril, 
                $verbasMaio, 
                $verbasJunho, 
                $verbasJulho,
                $verbasAgosto,
                $verbasSetembro,
                $verbasOutubro,
                $verbasNovembro,
                $verbasDezembro,
    
            ));
        



    }











    
}
