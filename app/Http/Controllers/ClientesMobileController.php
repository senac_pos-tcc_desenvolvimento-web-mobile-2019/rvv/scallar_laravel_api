<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cliente;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Routing\ResponseFactory;
use Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;


class ClientesMobileController extends Controller
{



    public function cadastrarClienteAPI(Request $request){

        $usuario_autenticado_id = $request->session()->get('usuarioId');


        $clientes = Cliente::create([

            'numero_clientes' => $request['numero_clientes'],
            'novos_clientes' => $request['novos_clientes'],
            'usuario_id' => $usuario_autenticado_id,
            'empresa_id' => $request['empresa_id'],
            'periodo_id' => $request['periodo_id'],
            'ano_id' => $request['ano_id'],

        ]);


        if($clientes){

            return response()->json(
                'Cliente Cadastrado Com Sucesso'
            );

        }else{

            return response()->json(
                'Erro ao salvar o cliente'
            );

        }


    }




    public function editarClienteAPI(Request $request, $id){

        $dados = $request->all();

        $clienteId = Cliente::find($id);

        $alt = $clienteId->update($dados);

        if ($alt)
        {

            return response()->json(
                'Cliente atualizado'
            );

        }else{

            return response()->json(
                'Erro ao atualizar o cliente'
            );


        }



    }





    public function excluirClienteAPI($id){

        $cliente = Cliente::find($id);

        if ($cliente->delete()){
                return response()->json(
                'Cliente deletado'
                );

        }else{
            return response()->json(
                'Erro ao excluir o cliente'
            );
        }

    }



    public function listarClienteAPI(Request $request){

        $usuario_autenticado_id = $request->session()->get('usuarioId');


        $clientes = Cliente::where('usuario_id', $usuario_autenticado_id)->orderBy('ano_id', 'desc')
        ->orderBy('periodo_id', 'desc')
        ->paginate(4);


        return response()->json($clientes);


    }



    public function graficosClienteAPI(Request $request){


        $usuario_autenticado_id = $request->session()->get('usuarioId');
        $data_atual = Carbon::now();


        $clientesJaneiro = DB::table('clientes')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 1)
            ->where('ano_id', $data_atual->year)
            ->sum('numero_clientes');

        $clientesFevereiro = DB::table('clientes')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 2)
            ->where('ano_id', $data_atual->year)
            ->sum('numero_clientes');

        $clientesMarco = DB::table('clientes')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 3)
            ->where('ano_id', $data_atual->year)
            ->sum('numero_clientes');

        $clientesAbril = DB::table('clientes')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 4)
            ->where('ano_id', $data_atual->year)
            ->sum('numero_clientes');

        $clientesMaio = DB::table('clientes')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 5)
            ->where('ano_id', $data_atual->year)
            ->sum('numero_clientes');

        $clientesJunho = DB::table('clientes')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 6)
            ->where('ano_id', $data_atual->year)
            ->sum('numero_clientes');

        $clientesJulho = DB::table('clientes')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 7)
            ->where('ano_id', $data_atual->year)
            ->sum('numero_clientes');

        $clientesAgosto = DB::table('clientes')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 8)
            ->where('ano_id', $data_atual->year)
            ->sum('numero_clientes');

        $clientesSetembro = DB::table('clientes')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 9)
            ->where('ano_id', $data_atual->year)
            ->sum('numero_clientes');

        $clientesOutubro = DB::table('clientes')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 10)
            ->where('ano_id', $data_atual->year)
            ->sum('numero_clientes');

        $clientesNovembro = DB::table('clientes')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 11)
            ->where('ano_id', $data_atual->year)
            ->sum('numero_clientes');

        $clientesDezembro = DB::table('clientes')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 12)
            ->where('ano_id', $data_atual->year)
            ->sum('numero_clientes');


            return response()->json(array(

                $clientesJaneiro,
                $clientesFevereiro,
                $clientesMarco,
                $clientesAbril,
                $clientesMaio,
                $clientesJunho,
                $clientesJulho,
                $clientesAgosto,
                $clientesSetembro,
                $clientesOutubro,
                $clientesNovembro,
                $clientesDezembro,

            ));



    }













}
