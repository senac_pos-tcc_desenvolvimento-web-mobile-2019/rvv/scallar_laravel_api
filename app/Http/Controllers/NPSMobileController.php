<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuario;
use App\Cargo;
use App\Empresa;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\Routing\ResponseFactory;
use Auth;
use Session;
use Carbon\Carbon;




class NPSMobileController extends Controller
{


    public function npsAtualAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

      


        $promotores_atual = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', $data_atual->month)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(net_promoter_scores.promotores) as promotores'))
        ->value('promotores');

    $detratores_atual = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', $data_atual->month)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(net_promoter_scores.detratores) as detratores'))
        ->value('detratores');

    $neutros_atual = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', $data_atual->month)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(net_promoter_scores.neutros) as neutros'))
        ->value('neutros');

    if ($promotores_atual + $detratores_atual + $neutros_atual != 0)
    {
        $nps_pagina = ($promotores_atual - $detratores_atual) / ($promotores_atual + $detratores_atual + $neutros_atual) * 100;
    }
    else
    {
        $nps_pagina = 0;
    }

    return $nps_pagina;


    }





  
    public function npsJaneiroAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

      

        $promotores_janeiro = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 1)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(net_promoter_scores.promotores) as promotores'))
        ->value('promotores');

    $detratores_janeiro = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 1)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(net_promoter_scores.detratores) as detratores'))
        ->value('detratores');

    $neutros_janeiro = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 1)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(net_promoter_scores.neutros) as neutros'))
        ->value('neutros');

    if ($promotores_janeiro + $detratores_janeiro + $neutros_janeiro != 0)
    {

        $nps_janeiro = ($promotores_janeiro - $detratores_janeiro) / ($promotores_janeiro + $detratores_janeiro + $neutros_janeiro) * 100;
    }
    else
    {
        $nps_janeiro = 0;
    }

    return $nps_janeiro;


    }


    public function npsFevereiroAPI(Request $request){


        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

       

        $promotores_fevereiro = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 2)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(net_promoter_scores.promotores) as promotores'))
        ->value('promotores');

    $detratores_fevereiro = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 2)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(net_promoter_scores.detratores) as detratores'))
        ->value('detratores');

    $neutros_fevereiro = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 2)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(net_promoter_scores.neutros) as neutros'))
        ->value('neutros');

    if ($promotores_fevereiro + $detratores_fevereiro + $neutros_fevereiro != 0)
    {

        $nps_fevereiro = ($promotores_fevereiro - $detratores_fevereiro) / ($promotores_fevereiro + $detratores_fevereiro + $neutros_fevereiro) * 100;
    }
    else
    {
        $nps_fevereiro = 0;
    }


    return $nps_fevereiro;


    }




    public function npsMarcoAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

         

        $promotores_marco = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 3)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(net_promoter_scores.promotores) as promotores'))
        ->value('promotores');

    $detratores_marco = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 3)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(net_promoter_scores.detratores) as detratores'))
        ->value('detratores');

    $neutros_marco = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 3)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(net_promoter_scores.neutros) as neutros'))
        ->value('neutros');

    if ($promotores_marco + $detratores_marco + $neutros_marco != 0)
    {

        $nps_marco = ($promotores_marco - $detratores_marco) / ($promotores_marco + $detratores_marco + $neutros_marco) * 100;
    }
    else
    {
        $nps_marco = 0;
    }

    return $nps_marco;

    }

    public function npsAbrilAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

      

        $promotores_abril = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 4)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(net_promoter_scores.promotores) as promotores'))
        ->value('promotores');

    $detratores_abril = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 4)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(net_promoter_scores.detratores) as detratores'))
        ->value('detratores');

    $neutros_abril = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 4)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(net_promoter_scores.neutros) as neutros'))
        ->value('neutros');

    if ($promotores_abril + $detratores_abril + $neutros_abril != 0)
    {

        $nps_abril = ($promotores_abril - $detratores_abril) / ($promotores_abril + $detratores_abril + $neutros_abril) * 100;
    }
    else
    {
        $nps_abril = 0;
    }

    return $nps_abril;

    }


    public function npsMaioAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

       
        $promotores_maio = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 5)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(net_promoter_scores.promotores) as promotores'))
        ->value('promotores');

    $detratores_maio = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 5)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(net_promoter_scores.detratores) as detratores'))
        ->value('detratores');

    $neutros_maio = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 5)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(net_promoter_scores.neutros) as neutros'))
        ->value('neutros');

    if ($promotores_maio + $detratores_maio + $neutros_maio != 0)
    {

        $nps_maio = ($promotores_maio - $detratores_maio) / ($promotores_maio + $detratores_maio + $neutros_maio) * 100;
    }
    else
    {
        $nps_maio = 0;
    }

    return $nps_maio;

    }


    public function npsJunhoAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

      

        $promotores_junho = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 6)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(net_promoter_scores.promotores) as promotores'))
        ->value('promotores');

    $detratores_junho = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 6)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(net_promoter_scores.detratores) as detratores'))
        ->value('detratores');

    $neutros_junho = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 6)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(net_promoter_scores.neutros) as neutros'))
        ->value('neutros');

    if ($promotores_junho + $detratores_junho + $neutros_junho != 0)
    {

        $nps_junho = ($promotores_junho - $detratores_junho) / ($promotores_junho + $detratores_junho + $neutros_junho) * 100;
    }
    else
    {
        $nps_junho = 0;
    }

    return $nps_junho;

    }

    public function npsJulhoAPI(Request $request){


        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $promotores_julho = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 7)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(net_promoter_scores.promotores) as promotores'))
        ->value('promotores');

    $detratores_julho = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 7)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(net_promoter_scores.detratores) as detratores'))
        ->value('detratores');

    $neutros_julho = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 7)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(net_promoter_scores.neutros) as neutros'))
        ->value('neutros');

    if ($promotores_julho + $detratores_julho + $neutros_julho != 0)
    {

        $nps_julho = ($promotores_julho - $detratores_julho) / ($promotores_julho + $detratores_julho + $neutros_julho) * 100;
    }
    else
    {
        $nps_julho = 0;
    }


    return $nps_julho;


    }

    public function npsAgostoAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $promotores_agosto = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 8)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(net_promoter_scores.promotores) as promotores'))
        ->value('promotores');

    $detratores_agosto = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 8)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(net_promoter_scores.detratores) as detratores'))
        ->value('detratores');

    $neutros_agosto = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 8)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(net_promoter_scores.neutros) as neutros'))
        ->value('neutros');

    if ($promotores_agosto + $detratores_agosto + $neutros_agosto != 0)
    {

        $nps_agosto = ($promotores_agosto - $detratores_agosto) / ($promotores_agosto + $detratores_agosto + $neutros_agosto) * 100;
    }
    else
    {
        $nps_agosto = 0;
    }

    return $nps_agosto;


    }





    public function npsSetembroAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        
        $promotores_setembro = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 9)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(net_promoter_scores.promotores) as promotores'))
        ->value('promotores');

    $detratores_setembro = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 9)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(net_promoter_scores.detratores) as detratores'))
        ->value('detratores');

    $neutros_setembro = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 9)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(net_promoter_scores.neutros) as neutros'))
        ->value('neutros');

    if ($promotores_setembro + $detratores_setembro + $neutros_setembro != 0)
    {

        $nps_setembro = ($promotores_setembro - $detratores_setembro) / ($promotores_setembro + $detratores_setembro + $neutros_setembro) * 100;
    }
    else
    {
        $nps_setembro = 0;
    }

    return $nps_setembro;


    }


    public function npsOutubroAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        
        $promotores_outubro = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 10)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(net_promoter_scores.promotores) as promotores'))
        ->value('promotores');

    $detratores_outubro = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 10)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(net_promoter_scores.detratores) as detratores'))
        ->value('detratores');

    $neutros_outubro = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 10)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(net_promoter_scores.neutros) as neutros'))
        ->value('neutros');

    if ($promotores_outubro + $detratores_outubro + $neutros_outubro != 0)
    {

        $nps_outubro = ($promotores_outubro - $detratores_outubro) / ($promotores_outubro + $detratores_outubro + $neutros_outubro) * 100;
    }
    else
    {
        $nps_outubro = 0;
    }

    return $nps_outubro;
    }


    public function npsNovembroAPI(Request $request){

        
        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        
          
        $promotores_novembro = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 11)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(net_promoter_scores.promotores) as promotores'))
        ->value('promotores');

    $detratores_novembro = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 11)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(net_promoter_scores.detratores) as detratores'))
        ->value('detratores');

    $neutros_novembro = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 11)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(net_promoter_scores.neutros) as neutros'))
        ->value('neutros');

    if ($promotores_novembro + $detratores_novembro + $neutros_novembro != 0)
    {

        $nps_novembro = ($promotores_novembro - $detratores_novembro) / ($promotores_novembro + $detratores_novembro + $neutros_novembro) * 100;
    }
    else
    {
        $nps_novembro = 0;
    }

    return $nps_novembro;


    }



    public function npsDezembroAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        
        $promotores_dezembro = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 12)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(net_promoter_scores.promotores) as promotores'))
        ->value('promotores');

    $detratores_dezembro = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 12)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(net_promoter_scores.detratores) as detratores'))
        ->value('detratores');

    $neutros_dezembro = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 12)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(net_promoter_scores.neutros) as neutros'))
        ->value('neutros');

    if ($promotores_dezembro + $detratores_dezembro + $neutros_dezembro != 0)
    {

        $nps_dezembro = ($promotores_dezembro - $detratores_dezembro) / ($promotores_dezembro + $detratores_dezembro + $neutros_dezembro) * 1200;
    }
    else
    {
        $nps_dezembro = 0;
    }

    return $nps_dezembro;


    }









    public function paginaNpsAPI(Request $request){

    
        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        
        $promotores_atual = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', $data_atual->month)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(net_promoter_scores.promotores) as promotores'))
        ->value('promotores');

    $detratores_atual = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', $data_atual->month)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(net_promoter_scores.detratores) as detratores'))
        ->value('detratores');

    $neutros_atual = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', $data_atual->month)
        ->where('net_promoter_scores.ano_id', '=', $data_atual->year)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(net_promoter_scores.neutros) as neutros'))
        ->value('neutros');

   

        //NPS ATUAL
       $nps_pagina = $this->npsAtualAPI($request);

     
        //NPS JANEIRO
        $nps_janeiro = $this->npsJaneiroAPI($request);


      

        //NPS FEVEREIRO
        $nps_fevereiro = $this->npsFevereiroAPI($request);

       
        //NPS MARCO
        $nps_marco = $this->npsMarcoAPI($request);
        

       

        //NPS ABRIL
        $nps_abril = $this->npsAbrilAPI($request);        

      

        //NPS MAIO
        $nps_maio = $this->npsMaioAPI($request);

        

        //NPS JUNHO
        $nps_junho = $this->npsJunhoAPI($request);

       

        //NPS JULHO
        $nps_julho = $this->npsJulhoAPI($request);

       
        //NPS AGOSTO
        $nps_agosto = $this->npsAgostoAPI($request);
    
    
        //NPS SETEMBRO
        $nps_setembro = $this->npsSetembroAPI($request);    

        
        //NPS OUTUBRO
        $nps_outubro = $this->npsOutubroAPI($request);
        

    
        //NPS NOVEMBRO
        $nps_novembro = $this->npsNovembroAPI($request);    

       

        //NPS DEZEMBRO
        $nps_dezembro = $this->npsDezembroAPI($request);

      

        if (is_null($promotores_atual) && is_null($detratores_atual) && is_null($neutros_atual))
        {
            $valida_nps = "Sem dados para avaliar o NPS.";
        }
        else if ($nps_pagina >= - 100 && $nps_pagina <= 0)
        {
            $valida_nps = "Seu NPS está na Zona Crítica.
            É recomendado que sua empresa aceite de forma humilde os problemas de relacionamento com clientes, peça desculpas e distribua brindes ou vantagens para mudar o quadro atual.
            Caso medidas não sejam tomadas de forma contínua para a melhoria da experiência do usuário, seus clientes deixarão de fazer negócios com sua marca, irão procurar a concorrência e recomendarão negativamente cada vez mais sua marca.";
        }
        else if ($nps_pagina >= 1 && $nps_pagina <= 50)
        {
            $valida_nps = "Seu NPS esta na Zona de aperfeiçoamento.
            Seu número de clientes promotores e detratores são semelhantes.
            Recomenda-se que você documente os resultados e corrija pequenos detalhes e surpreenda os clientes visando torná-los promotores.";
        }
        else if ($nps_pagina >= 51 && $nps_pagina <= 75)
        {
            $valida_nps = "Seu NPS está na zona de qualidade.
            Sua empresa demonstra ter preocupação em oferecer uma experiência positiva para os seus clientes.
            Sua empresa deve ter uma experiência de consumo perfeita e deve criar a partir destes momento estratégias de Cross Sell e Up Sell.";
        }
        else if ($nps_pagina >= 76 && $nps_pagina <= 100)
        {
            $valida_nps = "Sua empresa está na zona de excelência, parabéns!
            Vocês alcançaram algo que poucas empresas na história conseguiram.
            Seu objetivo deverá ser manter o padrão de excelência, através de um contato pró-ativo com seu cliente, sem fadiga e estimulando a divulgação boca-a-boca. Caso ainda não tenha feito, devem ser criadas as estratégias de Up Sell e Cross Sell.";
        }
        else if (is_null($nps_pagina))
        {
            $valida_nps = "Sem dados para avaliar o NPS.";
        }

        return response()->json([
            'nps_pagina' => ($nps_pagina),
            'valida_nps' => $valida_nps,
            'nps_janeiro' => ($nps_janeiro),
            'nps_fevereiro' => ($nps_fevereiro),
            'nps_marco' => ($nps_marco),
            'nps_abril' => ($nps_abril),
            'nps_maio' => ($nps_maio),
            'nps_junho' => ($nps_junho),
            'nps_julho' => ($nps_julho),
            'nps_agosto' => ($nps_agosto),
            'nps_setembro' => ($nps_setembro),
            'nps_outubro' => ($nps_outubro),
            'nps_novembro' => ($nps_novembro),
            'nps_dezembro' => ($nps_dezembro)
        ]);
       


    }







    public function npsPesquisaPeriodo(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');


        $promotores_atual = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', $periodo_id)->where('net_promoter_scores.ano_id', '=', $ano_id)
       // ->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)
        ->select(DB::raw('SUM(net_promoter_scores.promotores) as promotores'))
        ->value('promotores');

    $detratores_atual = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', $periodo_id)->where('net_promoter_scores.ano_id', '=', $ano_id)
        //->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)

        ->select(DB::raw('SUM(net_promoter_scores.detratores) as detratores'))
        ->value('detratores');

    $neutros_atual = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', $periodo_id)->where('net_promoter_scores.ano_id', '=', $ano_id)
        //->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)
        
        ->select(DB::raw('SUM(net_promoter_scores.neutros) as neutros'))
        ->value('neutros');

    if ($promotores_atual + $detratores_atual + $neutros_atual != 0)
    {
        $nps_pagina = ($promotores_atual - $detratores_atual) / ($promotores_atual + $detratores_atual + $neutros_atual) * 100;
    }
    else
    {
        $nps_pagina = 0;
    }

    return $nps_pagina;


    }

    public function npsPesquisaPeriodoJaneiro(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $promotores_janeiro = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 1)
        ->where('net_promoter_scores.ano_id', '=', $ano_id)
        //->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)

        ->select(DB::raw('SUM(net_promoter_scores.promotores) as promotores'))
        ->value('promotores');

    $detratores_janeiro = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 1)
        ->where('net_promoter_scores.ano_id', '=', $ano_id)
       // ->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)
       ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)

        ->select(DB::raw('SUM(net_promoter_scores.detratores) as detratores'))
        ->value('detratores');

    $neutros_janeiro = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 1)
        ->where('net_promoter_scores.ano_id', '=', $ano_id)
        //->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)

        ->select(DB::raw('SUM(net_promoter_scores.neutros) as neutros'))
        ->value('neutros');

    if ($promotores_janeiro + $detratores_janeiro + $neutros_janeiro != 0)
    {

        $nps_janeiro = ($promotores_janeiro - $detratores_janeiro) / ($promotores_janeiro + $detratores_janeiro + $neutros_janeiro) * 100;
    }
    else
    {
        $nps_janeiro = 0;
    }

    return $nps_janeiro;

    }


    public function npsPesquisaPeriodoFevereiro(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $promotores_fevereiro = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 2)
        ->where('net_promoter_scores.ano_id', '=', $ano_id)
        //->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)

        ->select(DB::raw('SUM(net_promoter_scores.promotores) as promotores'))
        ->value('promotores');

    $detratores_fevereiro = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 2)
        ->where('net_promoter_scores.ano_id', '=', $ano_id)
        //->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)

        ->select(DB::raw('SUM(net_promoter_scores.detratores) as detratores'))
        ->value('detratores');

    $neutros_fevereiro = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 2)
        ->where('net_promoter_scores.ano_id', '=', $ano_id)
        //->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)

        ->select(DB::raw('SUM(net_promoter_scores.neutros) as neutros'))
        ->value('neutros');

    if ($promotores_fevereiro + $detratores_fevereiro + $neutros_fevereiro != 0)
    {

        $nps_fevereiro = ($promotores_fevereiro - $detratores_fevereiro) / ($promotores_fevereiro + $detratores_fevereiro + $neutros_fevereiro) * 100;
    }
    else
    {
        $nps_fevereiro = 0;
    }

    return $nps_fevereiro;


    }



    public function npsPesquisaPeriodoMarco(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $promotores_marco = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 3)
        ->where('net_promoter_scores.ano_id', '=', $ano_id)
        //->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)

        ->select(DB::raw('SUM(net_promoter_scores.promotores) as promotores'))
        ->value('promotores');

    $detratores_marco = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 3)
        ->where('net_promoter_scores.ano_id', '=', $ano_id)
        //->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)

        ->select(DB::raw('SUM(net_promoter_scores.detratores) as detratores'))
        ->value('detratores');

    $neutros_marco = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 3)
        ->where('net_promoter_scores.ano_id', '=', $ano_id)
        //->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)

        ->select(DB::raw('SUM(net_promoter_scores.neutros) as neutros'))
        ->value('neutros');

    if ($promotores_marco + $detratores_marco + $neutros_marco != 0)
    {

        $nps_marco = ($promotores_marco - $detratores_marco) / ($promotores_marco + $detratores_marco + $neutros_marco) * 100;
    }
    else
    {
        $nps_marco = 0;
    }

    return $nps_marco;

    }


    public function npsPesquisaPeriodoAbril(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $promotores_abril = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 4)
        ->where('net_promoter_scores.ano_id', '=', $ano_id)
        //->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)

        ->select(DB::raw('SUM(net_promoter_scores.promotores) as promotores'))
        ->value('promotores');

    $detratores_abril = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 4)
        ->where('net_promoter_scores.ano_id', '=', $ano_id)
        //->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)

        ->select(DB::raw('SUM(net_promoter_scores.detratores) as detratores'))
        ->value('detratores');

    $neutros_abril = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 4)
        ->where('net_promoter_scores.ano_id', '=', $ano_id)
        //->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)

        ->select(DB::raw('SUM(net_promoter_scores.neutros) as neutros'))
        ->value('neutros');

    if ($promotores_abril + $detratores_abril + $neutros_abril != 0)
    {

        $nps_abril = ($promotores_abril - $detratores_abril) / ($promotores_abril + $detratores_abril + $neutros_abril) * 100;
    }
    else
    {
        $nps_abril = 0;
    }

    return $nps_abril;

    }


    public function npsPesquisaPeriodoMaio(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $promotores_maio = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 5)
        ->where('net_promoter_scores.ano_id', '=', $ano_id)
        //->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)

        ->select(DB::raw('SUM(net_promoter_scores.promotores) as promotores'))
        ->value('promotores');

    $detratores_maio = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 5)
        ->where('net_promoter_scores.ano_id', '=', $ano_id)
        //->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)

        ->select(DB::raw('SUM(net_promoter_scores.detratores) as detratores'))
        ->value('detratores');

    $neutros_maio = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 5)
        ->where('net_promoter_scores.ano_id', '=', $ano_id)
        //->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)

        ->select(DB::raw('SUM(net_promoter_scores.neutros) as neutros'))
        ->value('neutros');

    if ($promotores_maio + $detratores_maio + $neutros_maio != 0)
    {

        $nps_maio = ($promotores_maio - $detratores_maio) / ($promotores_maio + $detratores_maio + $neutros_maio) * 100;
    }
    else
    {
        $nps_maio = 0;
    }

    return $nps_maio;

    }

    public function npsPesquisaPeriodoJunho(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');


        $promotores_junho = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 6)
        ->where('net_promoter_scores.ano_id', '=', $ano_id)
        //->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)

        ->select(DB::raw('SUM(net_promoter_scores.promotores) as promotores'))
        ->value('promotores');

    $detratores_junho = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 6)
        ->where('net_promoter_scores.ano_id', '=', $ano_id)
        //->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)

        ->select(DB::raw('SUM(net_promoter_scores.detratores) as detratores'))
        ->value('detratores');

    $neutros_junho = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 6)
        ->where('net_promoter_scores.ano_id', '=', $ano_id)
        //->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)

        ->select(DB::raw('SUM(net_promoter_scores.neutros) as neutros'))
        ->value('neutros');

    if ($promotores_junho + $detratores_junho + $neutros_junho != 0)
    {

        $nps_junho = ($promotores_junho - $detratores_junho) / ($promotores_junho + $detratores_junho + $neutros_junho) * 100;
    }
    else
    {
        $nps_junho = 0;
    }


    return $nps_junho;

    }


    public function npsPesquisaPeriodoJulho(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $promotores_julho = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 7)
        ->where('net_promoter_scores.ano_id', '=', $ano_id)
        //->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)

        ->select(DB::raw('SUM(net_promoter_scores.promotores) as promotores'))
        ->value('promotores');

    $detratores_julho = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 7)
        ->where('net_promoter_scores.ano_id', '=', $ano_id)
        //->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)

        ->select(DB::raw('SUM(net_promoter_scores.detratores) as detratores'))
        ->value('detratores');

    $neutros_julho = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 7)
        ->where('net_promoter_scores.ano_id', '=', $ano_id)
        //->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)

        ->select(DB::raw('SUM(net_promoter_scores.neutros) as neutros'))
        ->value('neutros');

    if ($promotores_julho + $detratores_julho + $neutros_julho != 0)
    {

        $nps_julho = ($promotores_julho - $detratores_julho) / ($promotores_julho + $detratores_julho + $neutros_julho) * 100;
    }
    else
    {
        $nps_julho = 0;
    }

    return $nps_julho;

    }





    public function npsPesquisaPeriodoAgosto(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $promotores_agosto = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 8)
        ->where('net_promoter_scores.ano_id', '=', $ano_id)
       // ->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)
       ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)

        ->select(DB::raw('SUM(net_promoter_scores.promotores) as promotores'))
        ->value('promotores');

    $detratores_agosto = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 8)
        ->where('net_promoter_scores.ano_id', '=', $ano_id)
       // ->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)
       ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)

        ->select(DB::raw('SUM(net_promoter_scores.detratores) as detratores'))
        ->value('detratores');

    $neutros_agosto = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 8)
        ->where('net_promoter_scores.ano_id', '=', $ano_id)
        //->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)

        ->select(DB::raw('SUM(net_promoter_scores.neutros) as neutros'))
        ->value('neutros');

    if ($promotores_agosto + $detratores_agosto + $neutros_agosto != 0)
    {

        $nps_agosto = ($promotores_agosto - $detratores_agosto) / ($promotores_agosto + $detratores_agosto + $neutros_agosto) * 100;
    }
    else
    {
        $nps_agosto = 0;
    }

    return $nps_agosto;


    }


    public function npsPesquisaPeriodoSetembro(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $promotores_setembro = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 9)
        ->where('net_promoter_scores.ano_id', '=', $ano_id)
        //->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)

        ->select(DB::raw('SUM(net_promoter_scores.promotores) as promotores'))
        ->value('promotores');

    $detratores_setembro = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 9)
        ->where('net_promoter_scores.ano_id', '=', $ano_id)
        //->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)

        ->select(DB::raw('SUM(net_promoter_scores.detratores) as detratores'))
        ->value('detratores');

    $neutros_setembro = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 9)
        ->where('net_promoter_scores.ano_id', '=', $ano_id)
        //->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)

        ->select(DB::raw('SUM(net_promoter_scores.neutros) as neutros'))
        ->value('neutros');

    if ($promotores_setembro + $detratores_setembro + $neutros_setembro != 0)
    {

        $nps_setembro = ($promotores_setembro - $detratores_setembro) / ($promotores_setembro + $detratores_setembro + $neutros_setembro) * 100;
    }
    else
    {
        $nps_setembro = 0;
    }

    return $nps_setembro;




    }




    public function npsPesquisaPeriodoOutubro(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $promotores_outubro = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 10)
        ->where('net_promoter_scores.ano_id', '=', $ano_id)
        //->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)

        ->select(DB::raw('SUM(net_promoter_scores.promotores) as promotores'))
        ->value('promotores');

    $detratores_outubro = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 10)
        ->where('net_promoter_scores.ano_id', '=', $ano_id)
        //->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)

        ->select(DB::raw('SUM(net_promoter_scores.detratores) as detratores'))
        ->value('detratores');

    $neutros_outubro = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 10)
        ->where('net_promoter_scores.ano_id', '=', $ano_id)
        //->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)

        ->select(DB::raw('SUM(net_promoter_scores.neutros) as neutros'))
        ->value('neutros');

    if ($promotores_outubro + $detratores_outubro + $neutros_outubro != 0)
    {

        $nps_outubro = ($promotores_outubro - $detratores_outubro) / ($promotores_outubro + $detratores_outubro + $neutros_outubro) * 100;
    }
    else
    {
        $nps_outubro = 0;
    }

    return $nps_outubro;

    }


    public function npsPesquisaPeriodoNovembro(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');


        $promotores_novembro = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 11)
        ->where('net_promoter_scores.ano_id', '=', $ano_id)
        //->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)

        ->select(DB::raw('SUM(net_promoter_scores.promotores) as promotores'))
        ->value('promotores');

    $detratores_novembro = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 11)
        ->where('net_promoter_scores.ano_id', '=', $ano_id)
        //->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)

        ->select(DB::raw('SUM(net_promoter_scores.detratores) as detratores'))
        ->value('detratores');

    $neutros_novembro = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 11)
        ->where('net_promoter_scores.ano_id', '=', $ano_id)
        //->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)

        ->select(DB::raw('SUM(net_promoter_scores.neutros) as neutros'))
        ->value('neutros');

    if ($promotores_novembro + $detratores_novembro + $neutros_novembro != 0)
    {

        $nps_novembro = ($promotores_novembro - $detratores_novembro) / ($promotores_novembro + $detratores_novembro + $neutros_novembro) * 100;
    }
    else
    {
        $nps_novembro = 0;
    }

    return $nps_novembro;

    }



    public function npsPesquisaPeriodoDezembro(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $promotores_dezembro = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 12)
        ->where('net_promoter_scores.ano_id', '=', $ano_id)
        //->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)

        ->select(DB::raw('SUM(net_promoter_scores.promotores) as promotores'))
        ->value('promotores');

    $detratores_dezembro = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 12)
        ->where('net_promoter_scores.ano_id', '=', $ano_id)
        //->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)

        ->select(DB::raw('SUM(net_promoter_scores.detratores) as detratores'))
        ->value('detratores');

    $neutros_dezembro = DB::table('net_promoter_scores')->join('periodos', 'periodos.id', '=', 'net_promoter_scores.periodo_id')
        ->where('net_promoter_scores.periodo_id', '=', 12)
        ->where('net_promoter_scores.ano_id', '=', $ano_id)
        //->where('net_promoter_scores.usuario_id', '=', $usuario_autenticado_id)
        ->where('net_promoter_scores.usuario_id', '=', $usuarioAutenticadoId)

        ->select(DB::raw('SUM(net_promoter_scores.neutros) as neutros'))
        ->value('neutros');

    if ($promotores_dezembro + $detratores_dezembro + $neutros_dezembro != 0)
    {

        $nps_dezembro = ($promotores_dezembro - $detratores_dezembro) / ($promotores_dezembro + $detratores_dezembro + $neutros_dezembro) * 1200;
    }
    else
    {
        $nps_dezembro = 0;
    }

    return $nps_dezembro;

    }




    public function pesquisaNps(Request $request)
    {

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        //NPS ATUAL
        $nps_pagina = $this->npsPesquisaPeriodo($request);

        //NPS JANEIRO
        $nps_janeiro = $this->npsPesquisaPeriodoJaneiro($request);

        

        //NPS FEVEREIRO
        $nps_fevereiro = $this->npsPesquisaPeriodoFevereiro($request);


       

        //NPS MARCO
        $nps_marco = $this->npsPesquisaPeriodoMarco($request);


       

        //NPS ABRIL
        $nps_abril = $this->npsPesquisaPeriodoAbril($request);


       

        //NPS MAIO
        $nps_maio = $this->npsPesquisaPeriodoMaio($request);


        
        //NPS JUNHO
        $nps_junho = $this->npsPesquisaPeriodoJunho($request);


       
        //NPS JULHO
        $nps_julho = $this->npsPesquisaPeriodoJulho($request);


        

        //NPS AGOSTO
        $nps_agosto = $this->npsPesquisaPeriodoAgosto($request);


       
        //NPS SETEMBRO
        $nps_setembro = $this->npsPesquisaPeriodoSetembro($request);


       

        //NPS OUTUBRO
        $nps_outubro = $this->npsPesquisaPeriodoOutubro($request);


      

        //NPS NOVEMBRO
        $nps_novembro = $this->npsPesquisaPeriodoNovembro($request);


     

        //NPS DEZEMBRO
        $nps_dezembro = $this->npsPesquisaPeriodoDezembro($request);


        if (is_null($promotores_atual) && is_null($detratores_atual) && is_null($neutros_atual))
        {
            $valida_nps = "Sem dados para avaliar o NPS.";
        }
        else if ($nps_pagina >= - 100 && $nps_pagina <= 0)
        {
            $valida_nps = "Seu NPS está na Zona Crítica.
            É recomendado que sua empresa aceite de forma humilde os problemas de relacionamento com clientes, peça desculpas e distribua brindes ou vantagens para mudar o quadro atual.
            Caso medidas não sejam tomadas de forma contínua para a melhoria da experiência do usuário, seus clientes deixarão de fazer negócios com sua marca, irão procurar a concorrência e recomendarão negativamente cada vez mais sua marca.";
        }
        else if ($nps_pagina >= 1 && $nps_pagina <= 50)
        {
            $valida_nps = "Seu NPS esta na Zona de aperfeiçoamento.
            Seu número de clientes promotores e detratores são semelhantes.
            Recomenda-se que você documente os resultados e corrija pequenos detalhes e surpreenda os clientes visando torná-los promotores.";
        }
        else if ($nps_pagina >= 51 && $nps_pagina <= 75)
        {
            $valida_nps = "Seu NPS está na zona de qualidade.
            Sua empresa demonstra ter preocupação em oferecer uma experiência positiva para os seus clientes.
            Sua empresa deve ter uma experiência de consumo perfeita e deve criar a partir destes momento estratégias de Cross Sell e Up Sell.";
        }
        else if ($nps_pagina >= 76 && $nps_pagina <= 100)
        {
            $valida_nps = "Sua empresa está na zona de excelência, parabéns!
            Vocês alcançaram algo que poucas empresas na história conseguiram.
            Seu objetivo deverá ser manter o padrão de excelência, através de um contato pró-ativo com seu cliente, sem fadiga e estimulando a divulgação boca-a-boca. Caso ainda não tenha feito, devem ser criadas as estratégias de Up Sell e Cross Sell.";
        }
        else if (is_null($nps_pagina))
        {
            $valida_nps = "Sem dados para avaliar o NPS.";
        }

       
        


        
    }












}