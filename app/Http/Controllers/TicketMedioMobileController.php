<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuario;
use App\Cargo;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\Routing\ResponseFactory;
use Auth;
use Session;
use Carbon\Carbon;



class TicketMedioMobileController extends Controller
{

    public function ticketMedioAtualAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');




        $cliente_atual = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
            ->where('clientes.periodo_id', '=', $data_atual->month)
            ->where('clientes.ano_id', '=', $data_atual->year)
            ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
            ->value('clientes');

        $faturamento_atual = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
            ->where('faturamentos.periodo_id', '=', $data_atual->month)
            ->where('faturamentos.ano_id', '=', $data_atual->year)
            ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
            ->value('valor_faturamento');

        if ($cliente_atual != 0)
        {
            $ticket_medio_pagina = $faturamento_atual / $cliente_atual;
        }
        else
        {
            $ticket_medio_pagina = 0;
        }

        return response()->json([
            'ticket_medio_pagina' => number_format($ticket_medio_pagina,2)
        ]);


    }



     /* TICKET MEDIO ATUAL */

     public function ticketMedioPaginaAPI(Request $request)
     {
 
         $data_atual = Carbon::now();
         $usuarioAutenticadoId = $request->session()->get('usuarioId');
     

         $cliente_atual = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
             ->where('clientes.periodo_id', '=', $data_atual->month)
             ->where('clientes.ano_id', '=', $data_atual->year)
             ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
             ->value('clientes');
 
         $faturamento_atual = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
             ->where('faturamentos.periodo_id', '=', $data_atual->month)
             ->where('faturamentos.ano_id', '=', $data_atual->year)
             ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
             ->value('valor_faturamento');
 
         if ($cliente_atual != 0)
         {
             $ticket_medio_pagina = $faturamento_atual / $cliente_atual;
         }
         else
         {
             $ticket_medio_pagina = 0;
         }
 
         return $ticket_medio_pagina;
 
     }
 
 



    public function ticketMedioDezembroAnoPassadoAPI(Request $request)
    {

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

         

        $cliente_dezembro_ano_passado = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
            ->where('clientes.periodo_id', '=', 12)
            ->where('clientes.ano_id', '=', $data_atual->year - 1)
            ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
            ->value('clientes');

        $faturamento_dezembro_ano_passado = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
            ->where('faturamentos.periodo_id', '=', 12)
            ->where('faturamentos.ano_id', '=', $data_atual->year - 1)
            ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
            ->value('valor_faturamento');

        if ($faturamento_dezembro_ano_passado != 0)
        {
            $ticket_dezembro_ano_passado = $faturamento_dezembro_ano_passado / $cliente_dezembro_ano_passado;
        }
        else
        {
            $ticket_dezembro_ano_passado = 0;
        }

        return $ticket_dezembro_ano_passado;

    }






    public function ticketMedioJaneiroAPI(Request $request)
    {

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');


        $cliente_janeiro = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
            ->where('clientes.periodo_id', '=', 1)
            ->where('clientes.ano_id', '=', $data_atual->year)
            ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
            ->value('clientes');

        $faturamento_janeiro = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
            ->where('faturamentos.periodo_id', '=', 1)
            ->where('faturamentos.ano_id', '=', $data_atual->year)
            ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
            ->value('valor_faturamento');

        if ($cliente_janeiro != 0)
        {
            $ticket_medio_janeiro = $faturamento_janeiro / $cliente_janeiro;
        }
        else
        {
            $ticket_medio_janeiro = 0;
        }

        return $ticket_medio_janeiro;

    }

    /*TICKET MEDIO FEVEREIRO*/

    public function ticketMedioFevereiroAPI(Request $request)
    {

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

       

        $cliente_fevereiro = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
            ->where('clientes.periodo_id', '=', 2)
            ->where('clientes.ano_id', '=', $data_atual->year)
            ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
            ->value('clientes');

        $faturamento_fevereiro = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
            ->where('faturamentos.periodo_id', '=', 2)
            ->where('faturamentos.ano_id', '=', $data_atual->year)
            ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
            ->value('valor_faturamento');

        if ($cliente_fevereiro != 0)
        {
            $ticket_medio_fevereiro = $faturamento_fevereiro / $cliente_fevereiro;
        }
        else
        {
            $ticket_medio_fevereiro = 0;
        }

        return $ticket_medio_fevereiro;

    }

    /*TICKET MEDIO MARCO*/

    public function ticketMedioMarcoAPI(Request $request)
    {

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

         

        $cliente_marco = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
            ->where('clientes.periodo_id', '=', 3)
            ->where('clientes.ano_id', '=', $data_atual->year)
            ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
            ->value('clientes');

        $faturamento_marco = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
            ->where('faturamentos.periodo_id', '=', 3)
            ->where('faturamentos.ano_id', '=', $data_atual->year)
            ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
            ->value('valor_faturamento');

        if ($cliente_marco != 0)
        {
            $ticket_medio_marco = $faturamento_marco / $cliente_marco;
        }
        else
        {
            $ticket_medio_marco = 0;
        }

        return $ticket_medio_marco;

    }

    /*TICKET MEDIO ABRIL*/

    public function ticketMedioAbrilAPI(Request $request)
    {

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        

        $cliente_abril = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
            ->where('clientes.periodo_id', '=', 4)
            ->where('clientes.ano_id', '=', $data_atual->year)
            ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
            ->value('clientes');

        $faturamento_abril = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
            ->where('faturamentos.periodo_id', '=', 4)
            ->where('faturamentos.ano_id', '=', $data_atual->year)
            ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
            ->value('valor_faturamento');

        if ($cliente_abril != 0)
        {
            $ticket_medio_abril = $faturamento_abril / $cliente_abril;
        }
        else
        {
            $ticket_medio_abril = 0;
        }

        return $ticket_medio_abril;

    }

    /*TICKET MEDIO MAIO */

    public function ticketMedioMaioAPI(Request $request)
    {

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

       
        $cliente_maio = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
            ->where('clientes.periodo_id', '=', 5)
            ->where('clientes.ano_id', '=', $data_atual->year)
            ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
            ->value('clientes');

        $faturamento_maio = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
            ->where('faturamentos.periodo_id', '=', 5)
            ->where('faturamentos.ano_id', '=', $data_atual->year)
            ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
            ->value('valor_faturamento');

        if ($cliente_maio != 0)
        {
            $ticket_medio_maio = $faturamento_maio / $cliente_maio;
        }
        else
        {
            $ticket_medio_maio = 0;
        }

        return $ticket_medio_maio;

    }

    /*TICKET MEDIO JUNHO*/

    public function ticketMedioJunhoAPI(Request $request)
    {

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        
             

        $cliente_junho = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
            ->where('clientes.periodo_id', '=', 6)
            ->where('clientes.ano_id', '=', $data_atual->year)
            ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
            ->value('clientes');

        $faturamento_junho = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
            ->where('faturamentos.periodo_id', '=', 6)
            ->where('faturamentos.ano_id', '=', $data_atual->year)
            ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
            ->value('valor_faturamento');

        if ($cliente_junho != 0)
        {
            $ticket_medio_junho = $faturamento_junho / $cliente_junho;
        }
        else
        {
            $ticket_medio_junho = 0;
        }

        return $ticket_medio_junho;

    }

    /*TICKET MEDIO JULHO*/

    public function ticketMedioJulhoAPI(Request $request)
    {

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

           
        $cliente_julho = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
            ->where('clientes.periodo_id', '=', 7)
            ->where('clientes.ano_id', '=', $data_atual->year)
            ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
            ->value('clientes');

        $faturamento_julho = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
            ->where('faturamentos.periodo_id', '=', 7)
            ->where('faturamentos.ano_id', '=', $data_atual->year)
            ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
            ->value('valor_faturamento');

        if ($cliente_julho != 0)
        {
            $ticket_medio_julho = $faturamento_julho / $cliente_julho;
        }
        else
        {
            $ticket_medio_julho = 0;
        }

        return $ticket_medio_julho;

    }

    /*TICKET MEDIO AGOSTO*/

    public function ticketMedioAgostoAPI(Request $request)
    {

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

         

        $cliente_agosto = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
            ->where('clientes.periodo_id', '=', 8)
            ->where('clientes.ano_id', '=', $data_atual->year)
            ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
            ->value('clientes');

        $faturamento_agosto = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
            ->where('faturamentos.periodo_id', '=', 8)
            ->where('faturamentos.ano_id', '=', $data_atual->year)
            ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
            ->value('valor_faturamento');

        if ($cliente_agosto != 0)
        {
            $ticket_medio_agosto = $faturamento_agosto / $cliente_agosto;
        }
        else
        {
            $ticket_medio_agosto = 0;
        }

        return $ticket_medio_agosto;

    }

    /*TICKET MEDIO SETEMBRO */

    public function ticketMedioSetembroAPI(Request $request)
    {

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

       
       

        $cliente_setembro = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
            ->where('clientes.periodo_id', '=', 9)
            ->where('clientes.ano_id', '=', $data_atual->year)
            ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
            ->value('clientes');

        $faturamento_setembro = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
            ->where('faturamentos.periodo_id', '=', 9)
            ->where('faturamentos.ano_id', '=', $data_atual->year)
            ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
            ->value('valor_faturamento');

        if ($cliente_setembro != 0)
        {
            $ticket_medio_setembro = $faturamento_setembro / $cliente_setembro;
        }
        else
        {
            $ticket_medio_setembro = 0;
        }

        return $ticket_medio_setembro;

    }

    /*TICKET MEDIO OUTUBRO*/

    public function ticketMedioOutubroAPI(Request $request)
    {

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

       
        $cliente_outubro = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
            ->where('clientes.periodo_id', '=', 10)
            ->where('clientes.ano_id', '=', $data_atual->year)
            ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
            ->value('clientes');

        $faturamento_outubro = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
            ->where('faturamentos.periodo_id', '=', 10)
            ->where('faturamentos.ano_id', '=', $data_atual->year)
            ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
            ->value('valor_faturamento');

        if ($cliente_outubro != 0)
        {
            $ticket_medio_outubro = $faturamento_outubro / $cliente_outubro;
        }
        else
        {
            $ticket_medio_outubro = 0;
        }

        return $ticket_medio_outubro;

    }

    /*TICKET MEDIO NOVEMBRO*/

    public function ticketMedioNovembroAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

      


        $cliente_novembro = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
        ->where('clientes.periodo_id', '=', 11)
        ->where('clientes.ano_id', '=', $data_atual->year)
        ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
        ->value('clientes');

        $faturamento_novembro = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', 11)
        ->where('faturamentos.ano_id', '=', $data_atual->year)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');

        if ($cliente_novembro != 0)
        {
            $ticket_medio_novembro = $faturamento_novembro / $cliente_novembro;
        }
        else
        {
            $ticket_medio_novembro = 0;
        }

        return $ticket_medio_novembro;

    }

    /*TICKET MEDIO DEZEMBRO*/

    public function ticketMedioDezembroAPI(Request $request){

        $data_atual = Carbon::now();
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

       

        $cliente_dezembro = DB::table('clientes')->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
        ->where('clientes.periodo_id', '=', 12)
        ->where('clientes.ano_id', '=', $data_atual->year)
        ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
        ->value('clientes');

        $faturamento_dezembro = DB::table('faturamentos')->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
        ->where('faturamentos.periodo_id', '=', 12)
        ->where('faturamentos.ano_id', '=', $data_atual->year)
        ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
        ->value('valor_faturamento');

        if ($cliente_dezembro != 0)
        {
            $ticket_medio_dezembro = $faturamento_dezembro / $cliente_dezembro;
        }
        else
        {
            $ticket_medio_dezembro = 0;
        }

        return $ticket_medio_dezembro;


    }











    public function paginaTicketMedioAPI(Request $request)
    {

        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $data_atual = Carbon::now();



        //DADOS DO CAC ATUAL
        $cac_comparacao = app('App\Http\Controllers\CACMobileController')->cacPaginaAPI($request);

        //TICKET MEDIO ATUAL
        $ticket_medio_pagina = $this->ticketMedioPaginaAPI($request);


        //CAC JANEIRO

        //TICKET MEDIO DEZEMBRO ANO PASSADO
        $ticket_medio_dezembro_ano_passado = $this->ticketMedioDezembroAnoPassadoAPI($request);

        //TICKET MEDIO JANEIRO
        $ticket_medio_janeiro = $this->ticketMedioJaneiroAPI($request);

        //CAC FEVEREIRO


        //TICKET MEDIO FEVEREIRO
        $ticket_medio_fevereiro = $this->ticketMedioFevereiroAPI($request);

        

        //CAC MARÇO


        //TICKET MEDIO MARCO
        $ticket_medio_marco = $this->ticketMedioMarcoAPI($request);

        //CAC ABRIL

        //TICKET MEDIO ABRIL
        $ticket_medio_abril = $this->ticketMedioAbrilAPI($request);

        //CAC MAIO


        //TICKET MEDIO MAIO
        $ticket_medio_maio = $this->ticketMedioMaioAPI($request);

      

        //TICKET MEDIO MAIO
        $ticket_medio_junho = $this->ticketMedioJunhoAPI($request);

        

        //TICKET MEDIO MAIO
        $ticket_medio_julho = $this->ticketMedioJulhoAPI($request);


        //TICKET MEDIO AGOSTO
        $ticket_medio_agosto = $this->ticketMedioAgostoAPI($request);

        //TICKET MEDIO SETEMBRO
        $ticket_medio_setembro = $this->ticketMedioSetembroAPI($request);

      
        //TICKET MEDIO OUTUBRO
        $ticket_medio_outubro = $this->ticketMedioOutubroAPI($request);

       

        //TICKET MEDIO NOVEMBRO
       
        $ticket_medio_novembro = $this->ticketMedioNovembroAPI($request);

        

        //TICKET MEDIO DEZEMBRO       
        $ticket_medio_dezembro = $this->ticketMedioDezembroAPI($request);

        

        //Comparação de Dados

        //Dezembro/Janeiro

        if ($ticket_medio_janeiro == 0 || $ticket_medio_dezembro_ano_passado == 0)
        {
            $ticket_medio_janeiro_dezembro = 0;
        }
        else
        {
            $ticket_medio_janeiro_dezembro = (($ticket_medio_janeiro / $ticket_medio_dezembro_ano_passado) - 1) * 100;
        }

        //Fevereiro/Janeiro
        if ($ticket_medio_fevereiro == 0 || $ticket_medio_janeiro == 0)
        {
            $ticket_medio_fevereiro_janeiro = 0;
        }
        else
        {
            $ticket_medio_fevereiro_janeiro = (($ticket_medio_fevereiro / $ticket_medio_janeiro) - 1) * 100;
        }

        //Marco/Fevereiro
        if ($ticket_medio_marco == 0 || $ticket_medio_fevereiro == 0)
        {
            $ticket_medio_marco_fevereiro = 0;
        }
        else
        {
            $ticket_medio_marco_fevereiro = (($ticket_medio_marco / $ticket_medio_fevereiro) - 1) * 100;
        }

        //Abril/Marco
        if ($ticket_medio_abril == 0 || $ticket_medio_marco == 0)
        {
            $ticket_medio_abril_marco = 0;
        }
        else
        {
            $ticket_medio_abril_marco = (($ticket_medio_abril / $ticket_medio_marco) - 1) * 100;
        }

        //Maio/Abril
        if ($ticket_medio_maio == 0 || $ticket_medio_abril == 0)
        {
            $ticket_medio_maio_abril = 0;
        }
        else
        {
            $ticket_medio_maio_abril = (($ticket_medio_maio / $ticket_medio_abril) - 1) * 100;
        }

        //Junho/Maio
        if ($ticket_medio_junho == 0 || $ticket_medio_maio == 0)
        {
            $ticket_medio_junho_maio = 0;
        }
        else
        {
            $ticket_medio_junho_maio = (($ticket_medio_junho / $ticket_medio_maio) - 1) * 100;
        }

        //Julho/Junho
        if ($ticket_medio_julho == 0 || $ticket_medio_junho == 0)
        {
            $ticket_medio_julho_junho = 0;
        }
        else
        {
            $ticket_medio_julho_junho = (($ticket_medio_julho / $ticket_medio_junho) - 1) * 100;
        }

        //Agosto/Julho
        if ($ticket_medio_agosto == 0 || $ticket_medio_julho == 0)
        {
            $ticket_medio_agosto_julho = 0;
        }
        else
        {
            $ticket_medio_agosto_julho = (($ticket_medio_agosto / $ticket_medio_julho) - 1) * 100;
        }

        //Setembro/Agosto
        if ($ticket_medio_setembro == 0 || $ticket_medio_agosto == 0)
        {
            $ticket_medio_setembro_agosto = 0;
        }
        else
        {
            $ticket_medio_setembro_agosto = (($ticket_medio_setembro / $ticket_medio_agosto) - 1) * 100;
        }

        //Outubro/Setembro
        if ($ticket_medio_outubro == 0 || $ticket_medio_setembro == 0)
        {
            $ticket_medio_outubro_setembro = 0;
        }
        else
        {
            $ticket_medio_outubro_setembro = (($ticket_medio_outubro / $ticket_medio_setembro) - 1) * 100;
        }

        //Novembro/Outubro
        if ($ticket_medio_novembro == 0 || $ticket_medio_outubro == 0)
        {
            $ticket_medio_novembro_outubro = 0;
        }
        else
        {
            $ticket_medio_novembro_outubro = (($ticket_medio_novembro / $ticket_medio_outubro) - 1) * 100;
        }

        //Dezembro/Novembro
        if ($ticket_medio_dezembro == 0 || $ticket_medio_novembro == 0)
        {
            $ticket_medio_dezembro_novembro = 0;
        }
        else
        {
            $ticket_medio_dezembro_novembro = (($ticket_medio_dezembro / $ticket_medio_novembro) - 1) * 100;
        }

        if ($cac_comparacao > $ticket_medio_pagina)
        {
            $valida_ticket_medio = "A situação de sua empresa é crítica e exige uma mudança de estratégia, pois cada cliente adquirido está gerando um prejuízo monetário.";
        }
        else if ($cac_comparacao < $ticket_medio_pagina)
        {
            $valida_ticket_medio = "Tudo certo, sua empresa está obtendo lucratividade.";
        }
        else
        {
            $valida_ticket_medio = "Sem dados para avaliar o Ticket Médio.";
        }

        

        return response()->json([
            'ticket_medio_pagina' => number_format($ticket_medio_pagina,2),
            'valida_ticket_medio' => $valida_ticket_medio,
            'ticket_medio_janeiro' => ($ticket_medio_janeiro),
            'ticket_medio_fevereiro' => ($ticket_medio_fevereiro),
            'ticket_medio_marco' => ($ticket_medio_marco),
            'ticket_medio_abril' => ($ticket_medio_abril),
            'ticket_medio_maio' => ($ticket_medio_maio),
            'ticket_medio_junho' => ($ticket_medio_junho),
            'ticket_medio_julho' => ($ticket_medio_julho),
            'ticket_medio_agosto' => ($ticket_medio_agosto),
            'ticket_medio_setembro' => ($ticket_medio_setembro),
            'ticket_medio_outubro' => ($ticket_medio_outubro),
            'ticket_medio_novembro' => ($ticket_medio_novembro),
            'ticket_medio_dezembro' => ($ticket_medio_dezembro)
        ]);



    }

    /*TICKET MEDIO PESQUISA PERIODO*/

    public function ticketMedioPesquisaPeriodo(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $cliente_atual = DB::table('clientes')
            ->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
            ->where('clientes.periodo_id', '=', $periodo_id)
            ->where('clientes.ano_id', '=', $ano_id)
            ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)
            ->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
            ->value('clientes');


        $faturamento_atual = DB::table('faturamentos')
            ->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
            ->where('faturamentos.periodo_id', '=', $periodo_id)
            ->where('faturamentos.ano_id', '=', $ano_id)
            ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)
            ->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
            ->value('valor_faturamento');

        if ($cliente_atual != 0) {
            $ticket_medio_pagina = $faturamento_atual / $cliente_atual;
        } else {
            $ticket_medio_pagina = 0;
        }

        return $ticket_medio_pagina;


    }

      /*TICKET MEDIO PESQUISA PERIODO JANEIRO*/

      public function ticketMedioPesquisaPeriodoJaneiro(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $cliente_janeiro = DB::table('clientes')
            ->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
            ->where('clientes.periodo_id', '=', 1)
            ->where('clientes.ano_id', '=', $ano_id)
            ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)
            ->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
            ->value('clientes');


        $faturamento_janeiro = DB::table('faturamentos')
            ->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
            ->where('faturamentos.periodo_id', '=', 1)
            ->where('faturamentos.ano_id', '=', $ano_id)
            ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)
            ->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
            ->value('valor_faturamento');

        if ($cliente_janeiro != 0) {
            $ticket_medio_janeiro = $faturamento_janeiro / $cliente_janeiro;
        } else {
            $ticket_medio_janeiro = 0;
        }

        return $ticket_medio_janeiro;


    }


     /*TICKET MEDIO PESQUISA PERIODO FEVEREIRO*/

     public function ticketMedioPesquisaPeriodoFevereiro(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $cliente_fevereiro = DB::table('clientes')
            ->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
            ->where('clientes.periodo_id', '=', 2)
            ->where('clientes.ano_id', '=', $ano_id)
            ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)
            ->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
            ->value('clientes');


        $faturamento_fevereiro = DB::table('faturamentos')
            ->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
            ->where('faturamentos.periodo_id', '=', 2)
            ->where('faturamentos.ano_id', '=', $ano_id)
            ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)
            ->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
            ->value('valor_faturamento');

        if ($cliente_fevereiro != 0) {
            $ticket_medio_fevereiro = $faturamento_fevereiro / $cliente_fevereiro;
        } else {
            $ticket_medio_fevereiro = 0;
        }

        return $ticket_medio_fevereiro;



    }





     /*TICKET MEDIO PESQUISA PERIODO MARÇO*/

     public function ticketMedioPesquisaPeriodoMarco(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $cliente_marco = DB::table('clientes')
            ->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
            ->where('clientes.periodo_id', '=', 3)
            ->where('clientes.ano_id', '=', $ano_id)
            ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)
            ->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
            ->value('clientes');


        $faturamento_marco = DB::table('faturamentos')
            ->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
            ->where('faturamentos.periodo_id', '=', 3)
            ->where('faturamentos.ano_id', '=', $ano_id)
           ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)
            ->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
            ->value('valor_faturamento');

        if ($cliente_marco != 0) {
            $ticket_medio_marco = $faturamento_marco / $cliente_marco;
        } else {
            $ticket_medio_marco = 0;
        }

        return $ticket_medio_marco;



    }



    /*TICKET MEDIO PESQUISA PERIODO ABRIL*/

    public function ticketMedioPesquisaPeriodoAbril(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $cliente_abril = DB::table('clientes')
            ->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
            ->where('clientes.periodo_id', '=', 4)
            ->where('clientes.ano_id', '=', $ano_id)
            ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)
            ->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
            ->value('clientes');


        $faturamento_abril = DB::table('faturamentos')
            ->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
            ->where('faturamentos.periodo_id', '=', 4)
            ->where('faturamentos.ano_id', '=', $ano_id)
            ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)
            ->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
            ->value('valor_faturamento');

        if ($cliente_abril != 0) {
            $ticket_medio_abril = $faturamento_abril / $cliente_abril;
        } else {
            $ticket_medio_abril = 0;
        }

        return $ticket_medio_abril;



    }


      /*TICKET MEDIO PESQUISA PERIODO MAIO*/

      public function ticketMedioPesquisaPeriodoMaio(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $cliente_maio = DB::table('clientes')
            ->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
            ->where('clientes.periodo_id', '=', 5)
            ->where('clientes.ano_id', '=', $ano_id)
            

            ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)
            ->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
            ->value('clientes');


        $faturamento_maio = DB::table('faturamentos')
            ->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
            ->where('faturamentos.periodo_id', '=', 5)
            ->where('faturamentos.ano_id', '=', $ano_id)
            ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)
            ->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
            ->value('valor_faturamento');

        if ($cliente_maio != 0) {
            $ticket_medio_maio = $faturamento_maio / $cliente_maio;
        } else {
            $ticket_medio_maio = 0;
        }

        return $ticket_medio_maio;



    }





     /*TICKET MEDIO PESQUISA PERIODO JUNHO*/

     public function ticketMedioPesquisaPeriodoJunho(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $empresa_id = $request->empresa_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $cliente_junho = DB::table('clientes')
            ->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
            ->where('clientes.periodo_id', '=', 6)
            ->where('clientes.ano_id', '=', $ano_id)
           ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)
            ->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
            ->value('clientes');


        $faturamento_junho = DB::table('faturamentos')
            ->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
            ->where('faturamentos.periodo_id', '=', 6)
            ->where('faturamentos.ano_id', '=', $ano_id)
            ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)

            //->where('faturamentos.usuario_id', '=', $usuario_autenticado_id)
            ->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
            ->value('valor_faturamento');

        if ($cliente_junho != 0) {
            $ticket_medio_junho = $faturamento_junho / $cliente_junho;
        } else {
            $ticket_medio_junho = 0;
        }

        return $ticket_medio_junho;



    }








      /*TICKET MEDIO PESQUISA PERIODO JULHO*/

      public function ticketMedioPesquisaPeriodoJulho(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $empresa_id = $request->empresa_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $cliente_julho = DB::table('clientes')
            ->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
            ->where('clientes.periodo_id', '=', 7)
            ->where('clientes.ano_id', '=', $ano_id)
            

           ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)
            ->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
            ->value('clientes');


        $faturamento_julho = DB::table('faturamentos')
            ->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
            ->where('faturamentos.periodo_id', '=', 7)
            ->where('faturamentos.ano_id', '=', $ano_id)
            ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)

           // ->where('faturamentos.usuario_id', '=', $usuario_autenticado_id)
            ->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
            ->value('valor_faturamento');

        if ($cliente_julho != 0) {
            $ticket_medio_julho = $faturamento_julho / $cliente_julho;
        } else {
            $ticket_medio_julho = 0;
        }

        return $ticket_medio_julho;



    }







     /*TICKET MEDIO PESQUISA PERIODO AGOSTO*/

     public function ticketMedioPesquisaPeriodoAgosto(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $empresa_id = $request->empresa_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $cliente_agosto = DB::table('clientes')
            ->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
            ->where('clientes.periodo_id', '=', 8)
            ->where('clientes.ano_id', '=', $ano_id)
            

           ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)
            ->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
            ->value('clientes');


        $faturamento_agosto = DB::table('faturamentos')
            ->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
            ->where('faturamentos.periodo_id', '=', 8)
            ->where('faturamentos.ano_id', '=', $ano_id)
            ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)

           // ->where('faturamentos.usuario_id', '=', $usuario_autenticado_id)
            ->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
            ->value('valor_faturamento');

        if ($cliente_agosto != 0) {
            $ticket_medio_agosto = $faturamento_agosto / $cliente_agosto;
        } else {
            $ticket_medio_agosto = 0;
        }

        return $ticket_medio_agosto;



    }











     /*TICKET MEDIO PESQUISA PERIODO SETEMBRO*/

     public function ticketMedioPesquisaPeriodoSetembro(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $empresa_id = $request->empresa_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $cliente_setembro = DB::table('clientes')
            ->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
            ->where('clientes.periodo_id', '=', 9)
            ->where('clientes.ano_id', '=', $ano_id)
            

           ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)
            ->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
            ->value('clientes');


        $faturamento_setembro = DB::table('faturamentos')
            ->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
            ->where('faturamentos.periodo_id', '=', 9)
            ->where('faturamentos.ano_id', '=', $ano_id)
            ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)
            ->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
            ->value('valor_faturamento');

        if ($cliente_setembro != 0) {
            $ticket_medio_setembro = $faturamento_setembro / $cliente_setembro;
        } else {
            $ticket_medio_setembro = 0;
        }

        return $ticket_medio_setembro;



    }






      /*TICKET MEDIO PESQUISA PERIODO OUTUBRO*/

      public function ticketMedioPesquisaPeriodoOutubro(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $empresa_id = $request->empresa_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $cliente_outubro = DB::table('clientes')
            ->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
            ->where('clientes.periodo_id', '=', 10)
            ->where('clientes.ano_id', '=', $ano_id)
            

           ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)
            ->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
            ->value('clientes');


        $faturamento_outubro = DB::table('faturamentos')
            ->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
            ->where('faturamentos.periodo_id', '=', 10)
            ->where('faturamentos.ano_id', '=', $ano_id)
            ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)
            ->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
            ->value('valor_faturamento');

        if ($cliente_outubro != 0) {
            $ticket_medio_outubro = $faturamento_outubro / $cliente_setembro;
        } else {
            $ticket_medio_outubro = 0;
        }

        return $ticket_medio_outubro;



    }





     /*TICKET MEDIO PESQUISA PERIODO NOVEMBRO*/

     public function ticketMedioPesquisaPeriodoNovembro(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $empresa_id = $request->empresa_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $cliente_novembro = DB::table('clientes')
            ->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
            ->where('clientes.periodo_id', '=', 11)
            ->where('clientes.ano_id', '=', $ano_id)
           ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)
            ->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
            ->value('clientes');


        $faturamento_novembro = DB::table('faturamentos')
            ->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
            ->where('faturamentos.periodo_id', '=', 11)
            ->where('faturamentos.ano_id', '=', $ano_id)
            ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)
            ->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
            ->value('valor_faturamento');

        if ($cliente_novembro != 0) {
            $ticket_medio_novembro = $faturamento_novembro / $cliente_novembro;
        } else {
            $ticket_medio_novembro = 0;
        }

        return $ticket_medio_novembro;



    }



     /*TICKET MEDIO PESQUISA PERIODO DEZEMBRO*/

     public function ticketMedioPesquisaPeriodoDezembro(Request $request){

        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;
        $empresa_id = $request->empresa_id;
        $usuarioAutenticadoId = $request->session()->get('usuarioId');

        $cliente_dezembro = DB::table('clientes')
            ->join('periodos', 'periodos.id', '=', 'clientes.periodo_id')
            ->where('clientes.periodo_id', '=', 12)
            ->where('clientes.ano_id', '=', $ano_id)
            

           ->where('clientes.usuario_id', '=', $usuarioAutenticadoId)
            ->select(DB::raw('SUM(clientes.numero_clientes) as clientes'))
            ->value('clientes');


        $faturamento_dezembro = DB::table('faturamentos')
            ->join('periodos', 'periodos.id', '=', 'faturamentos.periodo_id')
            ->where('faturamentos.periodo_id', '=', 12)
            ->where('faturamentos.ano_id', '=', $ano_id)
            ->where('faturamentos.usuario_id', '=', $usuarioAutenticadoId)
            ->select(DB::raw('SUM(faturamentos.valor_faturamento) as valor_faturamento'))
            ->value('valor_faturamento');

        if ($cliente_dezembro != 0) {
            $ticket_medio_dezembro = $faturamento_dezembro / $cliente_dezembro;
        } else {
            $ticket_medio_dezembro = 0;
        }

        return $ticket_medio_dezembro;



    }






    /*PESQUISA DO TICKET MEDIO*/

    public function pesquisaTicketMedio(Request $request)
    {


        $usuarioAutenticadoId = $request->session()->get('usuarioId');
        $data_atual = Carbon::now();
        $periodo_id = $request->periodo_id;
        $ano_id = $request->ano_id;

        //DADOS DO CAC NO PERIODO
        $cac_comparacao = app('App\Http\Controllers\CACMobileController')->cacPesquisaPeriodo($request);

        //TICKET MEDIO NO PERIODO
        $ticket_medio = $this->ticketMedioPesquisaPeriodo($request);

         //TICKET MEDIO NO PERIODO DE JANEIRO
         $ticket_medio_janeiro = $this->ticketMedioPesquisaPeriodoJaneiro($request);

         //TICKET MEDIO NO PERIODO DE FEVEREIRO
         $ticket_medio_fevereiro = $this->ticketMedioPesquisaPeriodoFevereiro($request);

         //TICKET MEDIO NO PERIODO DE MARÇO
         $ticket_medio_marco = $this->ticketMedioPesquisaPeriodoMarco($request);

         //TICKET MEDIO NO PERIODO DE ABRIL
         $ticket_medio_abril = $this->ticketMedioPesquisaPeriodoAbril($request);

         //TICKET MEDIO NO PERIODO DE MAIO
         $ticket_medio_maio = $this->ticketMedioPesquisaPeriodoMaio($request);

        //TICKET MEDIO NO PERIODO DE JUNHO
        $ticket_medio_junho = $this->ticketMedioPesquisaPeriodoJunho($request);

         //TICKET MEDIO NO PERIODO DE JULHO
         $ticket_medio_julho = $this->ticketMedioPesquisaPeriodoJulho($request);

          //TICKET MEDIO NO PERIODO DE AGOSTO
          $ticket_medio_agosto = $this->ticketMedioPesquisaPeriodoAgosto($request);

          //TICKET MEDIO NO PERIODO DE SETEMBRO
          $ticket_medio_setembro = $this->ticketMedioPesquisaPeriodoSetembro($request);

           //TICKET MEDIO NO PERIODO DE OUTUBRO
           $ticket_medio_outubro = $this->ticketMedioPesquisaPeriodoOutubro($request);

             //TICKET MEDIO NO PERIODO DE NOVEMBRO
             $ticket_medio_novembro = $this->ticketMedioPesquisaPeriodoNovembro($request);

             //TICKET MEDIO NO PERIODO DE DEZEMBRO
             $ticket_medio_dezembro = $this->ticketMedioPesquisaPeriodoDezembro($request);

             



        if ($cac_comparacao > $ticket_medio)
        {
            $valida_ticket_medio = "A situação de sua empresa é crítica e exige uma mudança de estratégia, pois cada cliente adquirido está gerando um prejuízo monetário.";
        }
        else if ($cac_comparacao < $ticket_medio)
        {
            $valida_ticket_medio = "Tudo certo, sua empresa está obtendo lucratividade.";
        }
        else
        {
            $valida_ticket_medio = "Sem dados para avaliar o Ticket Médio.";
        }


        return response()->json([
            'ticket_medio' => number_format($ticket_medio,2),
            'valida_ticket_medio' => $valida_ticket_medio,
            'ticket_medio_janeiro' => $ticket_medio_janeiro,
            'ticket_medio_fevereiro' => $ticket_medio_fevereiro,
            'ticket_medio_marco' => $ticket_medio_marco,
            'ticket_medio_abril' => $ticket_medio_abril,
            'ticket_medio_maio' => $ticket_medio_maio,
            'ticket_medio_junho' => $ticket_medio_junho,
            'ticket_medio_julho' => $ticket_medio_julho,
            'ticket_medio_agosto' => $ticket_medio_agosto,
            'ticket_medio_setembro' => $ticket_medio_setembro,
            'ticket_medio_outubro' => $ticket_medio_outubro,
            'ticket_medio_novembro' => $ticket_medio_novembro,
            'ticket_medio_dezembro' => $ticket_medio_dezembro,


        ]);


      
    }


}