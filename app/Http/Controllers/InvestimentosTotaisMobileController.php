<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TotalInvestimento;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Routing\ResponseFactory;
use Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;


class InvestimentosTotaisMobileController extends Controller
{



    public function cadastrarInvestimentoTotalAPI(Request $request){

        $usuario_autenticado_id = $request->session()->get('usuarioId');


        $investimentos = TotalInvestimento::create([

            'valor' => $request['valor'],
            'nome' => $request['nome'],
            'periodo_id' => $request['periodo_id'],
            'ano_id' => $request['ano_id'],
            'empresa_id' => $request['empresa_id'],
            'usuario_id' => $usuario_autenticado_id,

        ]);


        if($investimentos){

            return response()->json(
                'Investimento total cadastrado com sucesso'
            );

        }else{

            return response()->json(
                'Erro ao salvar o investimento total'
            );

        }


    }




    public function editarInvestimentoTotalAPI(Request $request, $id){

        $dados = $request->all();

        $investimentoTotalId = TotalInvestimento::find($id);

        $alt = $investimentoTotalId->update($dados);

        if ($alt)
        {

            return response()->json(
                'Investimento total atualizado'
            );

        }else{

            return response()->json(
                'Erro ao atualizar o investimento total'
            );


        }



    }





    public function excluirInvestimentoTotalAPI($id){

        $investimento = TotalInvestimento::find($id);

        if ($investimento->delete()){
                return response()->json(
                'Investimento total deletado'
                );

        }else{
            return response()->json(
                'Erro ao excluir o investimento total'
            );
        }

    }



    public function listarInvestimentoTotalAPI(Request $request){

        $usuario_autenticado_id = $request->session()->get('usuarioId');


        $investimentos = TotalInvestimento::where('usuario_id', $usuario_autenticado_id)->orderBy('ano_id', 'desc')
        ->orderBy('periodo_id', 'desc')
        ->paginate(4);


        return response()->json($investimentos);


    }


    public function graficosInvestimentosTotaisAPI(Request $request){

        $usuario_autenticado_id = $request->session()->get('usuarioId');
        $data_atual = Carbon::now();

        $investimentosTotaisJaneiro = DB::table('total_investimentos')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 1)
            ->where('ano_id', $data_atual->year)
            ->sum('valor');

        $investimentosTotaisFevereiro = DB::table('total_investimentos')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 2)
            ->where('ano_id', $data_atual->year)
            ->sum('valor');

        $investimentosTotaisMarco = DB::table('total_investimentos')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 3)
            ->where('ano_id', $data_atual->year)
            ->sum('valor');

        $investimentosTotaisAbril = DB::table('total_investimentos')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 4)
            ->where('ano_id', $data_atual->year)
            ->sum('valor');

        $investimentosTotaisMaio = DB::table('total_investimentos')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 5)
            ->where('ano_id', $data_atual->year)
            ->sum('valor');

        $investimentosTotaisJunho = DB::table('total_investimentos')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 6)
            ->where('ano_id', $data_atual->year)
            ->sum('valor');

        $investimentosTotaisJulho = DB::table('total_investimentos')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 7)
            ->where('ano_id', $data_atual->year)
            ->sum('valor');

        $investimentosTotaisAgosto = DB::table('total_investimentos')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 8)
            ->where('ano_id', $data_atual->year)
            ->sum('valor');

        $investimentosTotaisSetembro = DB::table('total_investimentos')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 9)
            ->where('ano_id', $data_atual->year)
            ->sum('valor');

        $investimentosTotaisOutubro = DB::table('total_investimentos')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 10)
            ->where('ano_id', $data_atual->year)
            ->sum('valor');

        $investimentosTotaisNovembro = DB::table('total_investimentos')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 11)
            ->where('ano_id', $data_atual->year)
            ->sum('valor');

        $investimentosTotaisDezembro = DB::table('total_investimentos')->where('usuario_id', $usuario_autenticado_id)->where('periodo_id', 12)
            ->where('ano_id', $data_atual->year)
            ->sum('valor');


            return response()->json(array(

                $investimentosTotaisJaneiro,
                $investimentosTotaisFevereiro,
                $investimentosTotaisMarco,
                $investimentosTotaisAbril,
                $investimentosTotaisMaio,
                $investimentosTotaisJunho,
                $investimentosTotaisJulho,
                $investimentosTotaisAgosto,
                $investimentosTotaisSetembro,
                $investimentosTotaisOutubro,
                $investimentosTotaisNovembro,
                $investimentosTotaisDezembro,

            ));


    }













}
