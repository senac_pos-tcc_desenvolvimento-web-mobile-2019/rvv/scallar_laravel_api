<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $fillable = ['numero_clientes', 'data_inicial','data_final', 'usuario_id','empresa_id','periodo_id','ano_id','novos_clientes'];

    public function empresas()
    {
        return $this->hasOne('App\Empresa', 'id', 'empresa_id');
    }

    public function periodos()
    {
        return $this->hasOne('App\Periodo', 'id', 'periodo_id');
    }

    public function anos()
    {
        return $this->hasOne('App\Ano', 'id', 'ano_id');
    }

}
