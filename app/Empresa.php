<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{

    protected $fillable = ['razao_social','media_retencao_clientes', 'numero_funcionarios','faturamento_anual', 'usuario_id','segmento_id', 'cnpj','cod_empresa'];

    public function segmentos()
    {
        return $this->hasOne('App\Segmento', 'id', 'segmento_id');
    }

    // public function usuarios()
    // {
    //     return $this->belongsToMany('App\Usuario', 'usuario_empresa');
    // }

    public function usuarios()
    {
        return $this->belongsToMany('App\Usuario', 'usuario_empresa');
    }


}






