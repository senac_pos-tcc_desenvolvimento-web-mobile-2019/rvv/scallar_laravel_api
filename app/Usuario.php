<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;


class Usuario extends Authenticable
{

    use Notifiable, HasApiTokens;


    protected $fillable = ['nome', 'sobrenome', 'email', 'password','provider', 'provider_id','avatar', 'cargo_id','tipo_usuario_id','telefone'];


    protected $hidden = array('password', 'remember_token');

    public function instagram(){
        return $this->hasOne(Instagram::class, 'usuario_id', 'id');
    }

    public function cargos(){
        return $this->hasOne('App\Cargo', 'cargo_id', 'id');
    }

    public function tipo_usuarios(){
        return $this->hasOne('App\TipoUsuario', 'tipo_usuario_id', 'id');
    }

    // public function empresas()
    // {
    //     return $this->belongsToMany('App\Empresa', 'usuario_empresa');
    // }

    public function empresas()
    {
        return $this->belongsToMany('App\Empresa', 'usuario_empresa');
    }



}



