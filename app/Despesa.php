<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Despesa extends Model
{
    protected $fillable = ['valor','usuario_id','empresa_id','periodo_id','ano_id'];

    public function empresas()
    {
        return $this->hasOne('App\Empresa', 'id', 'empresa_id');
    }

    public function periodos()
    {
        return $this->hasOne('App\Periodo', 'id', 'periodo_id');
    }

    public function anos()
    {
        return $this->hasOne('App\Ano', 'id', 'ano_id');
    }
    
}
